package saivian.txlabz.com.saivian;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import saivian.txlabz.com.saivian.fragments.CameraFragment;
import saivian.txlabz.com.saivian.fragments.CashBackMainFragment;
import saivian.txlabz.com.saivian.fragments.FavoriteStoresFragment;
import saivian.txlabz.com.saivian.fragments.MainFragment;
import saivian.txlabz.com.saivian.fragments.ReceiptsFragment;
import saivian.txlabz.com.saivian.fragments.StoreMainFragment;
import saivian.txlabz.com.saivian.fragments.Top10StoresFragment;
import saivian.txlabz.com.saivian.fragments.UserProfileFragment;
import saivian.txlabz.com.saivian.models.UserDetail;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.Delegate;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;
import saivian.txlabz.com.saivian.views.Flip3dAnimation;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Delegate{

    ImageView menuIcon;
    DrawerLayout drawer;
    ImageView navMenuIcon;
    ImageView navUserImage;
    TextView navUserName;
    TextView navUserRefId;
    LinearLayout bottomContainer;
    boolean isViewUp = true;
    View camera, receipts, stores;
    String monthCashBack, yearCashBack;
    View activityRootView;
    ImageView bottomSecondImage;
    View bottomContainerParent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainStorageUtility utility = MainStorageUtility.getInstance(this.getApplicationContext());
        utility.setDelegate(this);

        if(!utility.isDataLoaded())
            GeneralUtil.showLoadingDialog(this);

        menuIcon = (ImageView) findViewById(R.id.menuIcon);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);

        navMenuIcon     = (ImageView) headerView.findViewById(R.id.nav_menu_icon);
        navUserImage    = (ImageView) headerView.findViewById(R.id.userImage);
        navUserName     = (TextView) headerView.findViewById(R.id.nav_name);
        navUserRefId    = (TextView) headerView.findViewById(R.id.nav_ref_id);
        bottomContainer = (LinearLayout)findViewById(R.id.bottom_container);
        bottomSecondImage = (ImageView)findViewById(R.id.bottom_second_image);
        bottomContainerParent = findViewById(R.id.bottom_container_parent);
        UserDetail userDetail = StorageUtility.getLoginModel(this);
        if(userDetail != null) {
            changeUserImage(userDetail.getProfileImage());
            navUserName.setText(userDetail.getFirstName() + " " + userDetail.getLastName());
            navUserRefId.setText("Ref : " + userDetail.getReferralId());
        }
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            monthCashBack = bundle.getString(AppConstants.MONTH_CASH_BACK);
            yearCashBack = bundle.getString(AppConstants.YEAR_CASH_BACK);
        }
        View navHome = findViewById(R.id.nav_home);
        View cashBackLocal = findViewById(R.id.nav_cashbacklocal);
        View navUser = findViewById(R.id.nav_user);
        View navInstantSaving = findViewById(R.id.nav_instantsaving);
        View navStore = findViewById(R.id.nav_store);
        View navShare = findViewById(R.id.nav_share);
        View navReceipts = findViewById(R.id.nav_receipt);
        View favorites = findViewById(R.id.nav_favorite);
        View logout = findViewById(R.id.nav_logout);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        receipts = findViewById(R.id.img_receipts);
        camera = findViewById(R.id.img_camera);
        stores = findViewById(R.id.img_stores);
        View parentContainer = findViewById(R.id.mainframe);
        activityRootView = findViewById(R.id.parentContainer);


        receipts.setOnClickListener(this);
        camera.setOnClickListener(this);
        stores.setOnClickListener(this);
        bottomSecondImage.setOnClickListener(this);
        menuIcon.setOnClickListener(this);
        navMenuIcon.setOnClickListener(this);
        navHome.setOnClickListener(this);
        navUser.setOnClickListener(this);
        cashBackLocal.setOnClickListener(this);
        navInstantSaving.setOnClickListener(this);
        navStore.setOnClickListener(this);
        navShare.setOnClickListener(this);
        navReceipts.setOnClickListener(this);
        favorites.setOnClickListener(this);
        logout.setOnClickListener(this);
        initMainFragment();
    }
    public View getRootView(){
        return activityRootView;
    }
    public void changeUserImage(String url){
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.avatar_placeholder)
                .showImageForEmptyUri(R.drawable.avatar_placeholder)
                .showImageOnFail(R.drawable.avatar_placeholder).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();
        imageLoader.displayImage(url,navUserImage,options);
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            super.dispatchTouchEvent(ev);

            if (isViewUp && bottomContainer.getVisibility() == View.VISIBLE
                    && !inCameraArea(ev) && !inReceiptArea(ev) && !inStoresArea(ev)) {
                sliderViewDown();
                isViewUp = false;

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }
    public boolean inCameraArea(MotionEvent ev) {
        float eventX = ev.getX();
        float eventY = ev.getY();
        return (eventX > camera.getX() && eventX < (camera.getX() + camera.getWidth())) ;
    }

    public boolean inReceiptArea(MotionEvent ev) {
        float eventX = ev.getX();
        float eventY = ev.getY();
        return (eventX > receipts.getX() && eventX < (receipts.getX() + receipts.getWidth())) ;
    }

    public boolean inStoresArea(MotionEvent ev) {
        float eventX = ev.getX();
        float eventY = ev.getY();
        return ((eventX > stores.getX() && eventX <
                (receipts.getX() + receipts.getWidth()
                + camera.getX() + camera.getWidth()
                + stores.getX() + stores.getWidth()))
            && eventY > (activityRootView.getHeight() - stores.getHeight()) + stores.getY() && eventY < ((activityRootView.getHeight() - stores.getHeight()) + stores.getY() + stores.getHeight()
        )
        );
    }

    private void initMainFragment() {
        MainFragment myf = new MainFragment();
        if(monthCashBack != null && yearCashBack != null){
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.MONTH_CASH_BACK,monthCashBack);
            bundle.putString(AppConstants.YEAR_CASH_BACK,yearCashBack);
            myf.setArguments(bundle);
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.mainframe, myf);
        transaction.commit();

    }
    public void reloadFragment(String name, boolean shouldPlayAnim){
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(name);
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if(shouldPlayAnim)
            ft.setCustomAnimations(R.anim.flip_in,R.anim.flip_out,R.anim.flip_in,R.anim.flip_out);

        ft.detach(fragment);
        ft.attach(fragment);
        ft.commitAllowingStateLoss();
    }
    public void loadFragment(final Fragment fragment) {
        drawer.closeDrawer(GravityCompat.START);
        if(fragment.isVisible())
            return;
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
        transaction.replace(R.id.mainframe, fragment,fragment.getClass().getName());
        transaction.addToBackStack(fragment.getClass().getName());

        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    public void loadHomeFragment() {
        drawer.closeDrawer(GravityCompat.START);
        getSupportFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
        initMainFragment();
    }
    @Override
    public void onClick(View v) {
        GeneralUtil.hideKeyboard(this);
        switch (v.getId()) {
            case R.id.menuIcon:
                if(!drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.openDrawer(GravityCompat.START);
                } else {
                    drawer.closeDrawer(GravityCompat.START);
                }
                break;
            case R.id.nav_home:
                loadHomeFragment();
                break;
            case R.id.nav_menu_icon:
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.nav_user:
                UserProfileFragment fragment3 = new UserProfileFragment();
                Bundle bundle3 = new Bundle();
                bundle3.putBoolean(AppConstants.CAN_EDIT,false);
                fragment3.setArguments(bundle3);
                loadFragment(fragment3);
//                loadFragment(new UserProfileFragment());
                break;

            case R.id.nav_cashbacklocal:
                CashBackMainFragment fragment = new CashBackMainFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.INDEX,0);
                fragment.setArguments(bundle);
                loadFragment(fragment);
                break;

            case R.id.nav_instantsaving:
                CashBackMainFragment fragment1 = new CashBackMainFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putInt(AppConstants.INDEX,1);
                fragment1.setArguments(bundle1);
                loadFragment(fragment1);
                break;

            case R.id.nav_store:
                loadFragment(new Top10StoresFragment());
//                loadFragment(new StoreMainFragment());
                break;

            case R.id.nav_share:
                UserProfileFragment fragment2 = new UserProfileFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putBoolean(AppConstants.CAN_EDIT,false);
                fragment2.setArguments(bundle2);
                loadFragment(fragment2);
                break;

            case R.id.nav_receipt:
                loadFragment(new ReceiptsFragment());
                break;
            case R.id.nav_favorite:
                loadFragment(new FavoriteStoresFragment());
                break;
            case R.id.img_camera:
                if(!isViewUp) {
                    slideViewUp();
                    isViewUp = true;
                }
                else {
                    getCameraPermission();
                }
                break;
            case R.id.bottom_second_image:
                if(!isViewUp) {
                    slideViewUp();
                    isViewUp = true;
                }
                break;
            case R.id.img_receipts:
                loadFragment(new ReceiptsFragment());
                break;

            case R.id.img_stores:
                loadFragment(new Top10StoresFragment());
//                loadFragment(new StoreMainFragment());
                break;
            case R.id.nav_logout:
                MainStorageUtility utility = MainStorageUtility.getInstance(this);
                utility.clearAll();
                StorageUtility.clearAllPreferences(this);
                Intent intent = new Intent(this,LoginActivity.class);
                intent.putExtra(AppConstants.SHOULD_RUN_SPLASH,"false");
                startActivity(intent);
                finish();
                break;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppConstants.GET_CAMERA_PERMISSION_RC:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            loadFragment(new CameraFragment());
                        }
                    },100);

                } else {

                }
                return;
            case AppConstants.REQUEST_CODE:
                List<Fragment> listOfFragments = getSupportFragmentManager().getFragments();

                if (listOfFragments.size() >= 1) {
                    for (Fragment fragment : listOfFragments) {
                        if (fragment != null)
                            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    }
                }
                break;
        }
    }
    private void getCameraPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( this,
                    new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    AppConstants.GET_CAMERA_PERMISSION_RC);

        }else{
            loadFragment(new CameraFragment());
        }
    }
    private void slideViewUp(){
        Animation animMove2 = AnimationUtils.loadAnimation(this, R.anim.move2);
        bottomContainer.startAnimation(animMove2);
        animMove2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                bottomSecondImage.setVisibility(View.GONE);
                bottomContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void sliderViewDown(){
        Animation animMove2 = AnimationUtils.loadAnimation(this, R.anim.move);
        bottomContainer.startAnimation(animMove2);
        animMove2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                bottomContainer.clearAnimation();
                bottomSecondImage.setVisibility(View.VISIBLE);
                bottomContainer.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    public void hideBottomBar(){
        isViewUp = false;
        bottomContainer.clearAnimation();
        bottomContainerParent.setVisibility(View.GONE);
    }
    public void showBottomBar(){

//        bottomContainer.clearAnimation();
        bottomContainerParent.setVisibility(View.VISIBLE);
        if(bottomSecondImage.getVisibility() == View.VISIBLE)
            isViewUp = false;
        else
            isViewUp = true;
    }

    public void applyAnimationOnParentView(boolean playInReverse){
        View parentContainer = findViewById(R.id.parentContainer);
        final Flip3dAnimation rotation = new Flip3dAnimation(parentContainer,parentContainer);
        if(playInReverse)
            rotation.reverse();
        parentContainer.startAnimation(rotation);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        List<Fragment> listOfFragments = getSupportFragmentManager().getFragments();

        if (listOfFragments.size() >= 1) {
            for (Fragment fragment : listOfFragments) {
                if (fragment != null)
                    fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void callback(String result) {
        GeneralUtil.dismissLoadingDialog();
        if(result.equals(AppConstants.FAILURE))
            GeneralUtil.showAlert(this,this.getString(R.string.call_fail_error));
        else{
            notifyFragments();
        }
    }

    private void notifyFragments() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.mainframe);
        if(currentFragment instanceof MainFragment && currentFragment.isVisible()){
            ((MainFragment)currentFragment).notifyDataAdded();
        }
    }
}
