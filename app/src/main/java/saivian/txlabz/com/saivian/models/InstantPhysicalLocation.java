package saivian.txlabz.com.saivian.models;

/**
 * Created by Fatima Siddique on 6/17/2016.
 */
public class InstantPhysicalLocation {

    String locationName;
    String webAddress;
    String description;
    boolean onlineExclusive;
    String locationKey;
    String postalCode;
    String country;
    String streetAddress;
    String extendedStreetAddress;
    String cityLocality;
    String stateRegion;
    InstantGeoLocation instantGeoLocation;
    String phoneNumber;

    public InstantPhysicalLocation(String locationName, String webAddress, String description, boolean onlineExclusive, String locationKey, String postalCode, String country, String streetAddress, String extendedStreetAddress, String cityLocality, String stateRegion, InstantGeoLocation instantGeoLocation,String phoneNumber) {
        this.locationName = locationName;
        this.webAddress = webAddress;
        this.description = description;
        this.onlineExclusive = onlineExclusive;
        this.locationKey = locationKey;
        this.postalCode = postalCode;
        this.country = country;
        this.streetAddress = streetAddress;
        this.extendedStreetAddress = extendedStreetAddress;
        this.cityLocality = cityLocality;
        this.stateRegion = stateRegion;
        this.instantGeoLocation = instantGeoLocation;
        this.phoneNumber = phoneNumber;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getOnlineExclusive() {
        return onlineExclusive;
    }

    public void setOnlineExclusive(boolean onlineExclusive) {
        this.onlineExclusive = onlineExclusive;
    }

    public String getLocationKey() {
        return locationKey;
    }

    public void setLocationKey(String locationKey) {
        this.locationKey = locationKey;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getExtendedStreetAddress() {
        return extendedStreetAddress;
    }

    public void setExtendedStreetAddress(String extendedStreetAddress) {
        this.extendedStreetAddress = extendedStreetAddress;
    }

    public String getCityLocality() {
        return cityLocality;
    }

    public void setCityLocality(String cityLocality) {
        this.cityLocality = cityLocality;
    }

    public String getStateRegion() {
        return stateRegion;
    }

    public void setStateRegion(String stateRegion) {
        this.stateRegion = stateRegion;
    }

    public InstantGeoLocation getInstantGeoLocation() {
        return instantGeoLocation;
    }

    public void setInstantGeoLocation(InstantGeoLocation instantGeoLocation) {
        this.instantGeoLocation = instantGeoLocation;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
