package saivian.txlabz.com.saivian.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import saivian.txlabz.com.saivian.models.UserDetail;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;
import saivian.txlabz.com.saivian.views.ShareDialog;


/**
 * Created by irfanelahi on 13/03/2016.
 */
public class MainFragment extends Fragment implements View.OnClickListener{

    Context context;
//    CountDownTimer countDownTimer;
    TextView earnedAmount,monthDateExp;
    Button monthToDate, yearToDate;
    long timerValue = 0;
    ImageView share;
    ImageView mainImage;
    TextView username;
    long monthCashBack, yearCashBack;
    View mainImageContainer;
    boolean isInitialAnimPlayed = false;
    Timer timer;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        isInitialAnimPlayed = false;
        View cashBackContainer = view.findViewById(R.id.cashbacklocal_container);
        View instantSavingContainer = view.findViewById(R.id.instantsaving_container);
        mainImage = (ImageView)view.findViewById(R.id.userImage);
        mainImageContainer = view.findViewById(R.id.userImageContainer);
        earnedAmount = (TextView) view.findViewById(R.id.earnedAmount);
        monthDateExp = (TextView)view.findViewById(R.id.txt_monthDate_detail);
        monthToDate = (Button)view.findViewById(R.id.monthToDate);
        yearToDate = (Button)view.findViewById(R.id.yearToDate);
        share = (ImageView)view.findViewById(R.id.share);
        username = (TextView)view.findViewById(R.id.txt_username);

        share.setVisibility(View.INVISIBLE);
        mainImageContainer.setVisibility(View.INVISIBLE);
        username.setVisibility(View.INVISIBLE);

        cashBackContainer.setOnClickListener(this);
        instantSavingContainer.setOnClickListener(this);
        share.setOnClickListener(this);

        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.avatar_placeholder)
                .showImageForEmptyUri(R.drawable.avatar_placeholder)
                .showImageOnFail(R.drawable.avatar_placeholder).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.ARGB_8888).build();

        mainImage = (ImageView)view.findViewById(R.id.userImage);
        UserDetail userDetail = StorageUtility.getLoginModel(context);
        if(userDetail != null) {
            imageLoader.displayImage(userDetail.getProfileImage(), mainImage, options);
            username.setText(userDetail.getFirstName() + " " + userDetail.getLastName());
        }
        monthToDate.setOnClickListener(this);
        yearToDate.setOnClickListener(this);
        monthToDate.setSelected(true);
        yearToDate.setSelected(false);
        share.setVisibility(View.INVISIBLE);
        monthDateExp.setText("Month to Date Cash Back\nThrough "+getCurrentDate());
        notifyDataAdded();
    }
    @Override
    public void onResume(){
        super.onResume();
//
    }

    private void getCashBackValuesFromServer() {
        GeneralUtil.showLoadingDialog(context);
        Ion.with(this)
                .load(AppConstants.GET,AppConstants.GET_CASH_BACK_AMOUNT+"?customer_id="+ StorageUtility.getLoginModel(context).getId())
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();
                Log.i(AppConstants.TAG,"result is : " + result);
                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            monthCashBack = Long.valueOf(Integer.parseInt(Json1.getString(AppConstants.MONTH_CASH_BACK)));
                            yearCashBack = Long.valueOf(Integer.parseInt(Json1.getString(AppConstants.YEAR_CASH_BACK)));
                            timerValue = monthCashBack;
                            animOnUserImage();
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "message: " + e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }

    private void animOnUserImage(){

        Animation animation = AnimationUtils.loadAnimation(context,R.anim.zoom_in);
        mainImageContainer.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mainImageContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                startCountDownTimer(timerValue);
                username.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cashbacklocal_container:
                CashBackMainFragment fragment = new CashBackMainFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.INDEX,0);
                fragment.setArguments(bundle);
                ((MainActivity)context).loadFragment(new CashBackMainFragment());
                break;

            case R.id.instantsaving_container:
                CashBackMainFragment fragment1 = new CashBackMainFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putInt(AppConstants.INDEX,1);
                fragment1.setArguments(bundle1);
                ((MainActivity)context).loadFragment(fragment1);
                break;

            case R.id.monthToDate:
                value = 0;
                timerValue = monthCashBack;
                share.setVisibility(View.INVISIBLE);
                monthDateExp.setVisibility(View.INVISIBLE);
                monthDateExp.setText("Month to Date Cash Back\nThrough "+getCurrentDate());
                monthToDate.setSelected(true);
                yearToDate.setSelected(false);
                animOnUserImage();
                break;

            case R.id.yearToDate:
                value = 0;
                share.setVisibility(View.INVISIBLE);
                monthDateExp.setVisibility(View.INVISIBLE);
                monthDateExp.setText("Year to Date Cash Back\nThrough "+getCurrentDate());
                monthToDate.setSelected(false);
                yearToDate.setSelected(true);
                timerValue = yearCashBack;
                animOnUserImage();
                break;

            case R.id.share:
                Intent intent = new Intent(context,ShareDialog.class);
                startActivity(intent);
//                ShareDialog shareDialog = new ShareDialog(context);
//                shareDialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
//                shareDialog.show();
                break;
        }
    }
    private String getCurrentDate(){
        DateFormat df = new SimpleDateFormat("MMM d, yyyy");
        String now = df.format(new Date());
        return now;
    }
    int value = 0;
    private void startCountDownTimer(final long timerValue) {
        if(timer != null)
            return;
        value = 0;
        monthDateExp.setVisibility(View.INVISIBLE);
        share.setVisibility(View.INVISIBLE);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(earnedAmount != null) {
                    ((MainActivity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            earnedAmount.setText("$ " + value);
                            if(value < timerValue)
                                value++;
                            else{
                                if(timer != null) {
                                    timer.cancel();
                                    applyBlinkAnim();
                                    monthDateExp.setVisibility(View.VISIBLE);
                                    timer = null;
                                }
                            }
                        }
                    });


                }
            }
        },0,7);
//        if(countDownTimer != null){
//            return;
//        }
//        value = 0;
//        monthDateExp.setVisibility(View.INVISIBLE);
//        share.setVisibility(View.INVISIBLE);
//        countDownTimer = new CountDownTimer(timer*1000, 1L) {
//            public void onTick(long millis) {
//                if(earnedAmount != null) {
//                    earnedAmount.setText("$ " + value);
//                    if(value < timer)
//                        value++;
//                    else{
//                        if(countDownTimer != null) {
//                            countDownTimer.cancel();
//                            applyBlinkAnim();
//                            monthDateExp.setVisibility(View.VISIBLE);
//                            countDownTimer = null;
//                        }
//                    }
//                }
//            }
//
//            public void onFinish() {
//                Log.i("check", "In on Finish,,,");
//                applyBlinkAnim();
//                monthDateExp.setVisibility(View.VISIBLE);
//                countDownTimer = null;
//            }
//
//        };
//        countDownTimer.start();
    }

    private void applyBlinkAnim(){
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(100);
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(5);
        earnedAmount.startAnimation(anim);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                applyZoomAnim();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void applyZoomAnim(){
        share.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(context,R.anim.zoom_in);
        share.startAnimation(animation);
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        if(timer != null)
            timer.cancel();
        timer = null;
        timerValue = 0;
    }

    public void notifyDataAdded() {
        if(!isInitialAnimPlayed) {
            MainStorageUtility utility = MainStorageUtility.getInstance(context.getApplicationContext());
            monthCashBack = utility.getMonthCashBack();
            yearCashBack = utility.getYearCashBack();

            if(monthCashBack > -1L && yearCashBack > -1L) {
                if(timer != null)
                    timer.cancel();
                timer = null;
                timerValue = monthCashBack;
                animOnUserImage();
                isInitialAnimPlayed = true;
            }
        }
    }

}
