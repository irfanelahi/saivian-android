package saivian.txlabz.com.saivian.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import saivian.txlabz.com.saivian.models.UserDetail;

/**
 * Created by irfanelahi on 17/03/2016.
 */
public class StorageUtility {

    public static void saveLoginObject(UserDetail model, Context context) {
        SharedPreferences mPreference;
        SharedPreferences.Editor prefsEditor;
        mPreference = PreferenceManager.getDefaultSharedPreferences(context);
        prefsEditor = mPreference.edit();

        prefsEditor.putString(AppConstants.ID, model.getId());
        prefsEditor.putString(AppConstants.REFERRAL_ID, model.getReferralId());
        prefsEditor.putString(AppConstants.FIRST_NAME, model.getFirstName());
        prefsEditor.putString(AppConstants.LAST_NAME, model.getLastName());
        prefsEditor.putString(AppConstants.EMAIL, model.getEmail());
        prefsEditor.putString(AppConstants.PASSWORD, model.getPassword());
        prefsEditor.putString(AppConstants.PROFILE_IMAGE, model.getProfileImage());
        prefsEditor.putString(AppConstants.GENDER, model.getGender());
        prefsEditor.putString(AppConstants.DOB, model.getDob());
        prefsEditor.putString(AppConstants.CITY, model.getCity());
        prefsEditor.putString(AppConstants.STATE, model.getState());
        prefsEditor.putString(AppConstants.ZIP, model.getZip());
        prefsEditor.putString(AppConstants.COUNTRY, model.getCountry());
        prefsEditor.putString(AppConstants.IS_VERIFIED, model.getIsVerified());
        prefsEditor.putString(AppConstants.IS_ACTIVE, model.getIsActive());
        prefsEditor.putString(AppConstants.CREATED_DATE, model.getCreatedDate());
        prefsEditor.putString(AppConstants.PHONE, model.getPhone());
        prefsEditor.putString(AppConstants.SAIVIAN_ID, model.getSaivianId());
        prefsEditor.commit();
    }

    public static UserDetail getLoginModel(Context context) {

        SharedPreferences mPreference;
        mPreference = PreferenceManager.getDefaultSharedPreferences(context);

        String id           = mPreference.getString(AppConstants.ID, null);
        String referralId      = mPreference.getString(AppConstants.REFERRAL_ID, null);
        String firstName         = mPreference.getString(AppConstants.FIRST_NAME, null);
        String lastName  = mPreference.getString(AppConstants.LAST_NAME, null);
        String email        = mPreference.getString(AppConstants.EMAIL, null);
        String password = mPreference.getString(AppConstants.PASSWORD, null);
        String profileImage           = mPreference.getString(AppConstants.PROFILE_IMAGE, null);
        String gender      = mPreference.getString(AppConstants.GENDER, null);
        String dob         = mPreference.getString(AppConstants.DOB, null);
        String city  = mPreference.getString(AppConstants.CITY, null);
        String state        = mPreference.getString(AppConstants.STATE, null);
        String zip = mPreference.getString(AppConstants.ZIP, null);
        String country           = mPreference.getString(AppConstants.COUNTRY, null);
        String isVerified      = mPreference.getString(AppConstants.IS_VERIFIED, null);
        String isActive         = mPreference.getString(AppConstants.IS_ACTIVE, null);
        String createdDate  = mPreference.getString(AppConstants.CREATED_DATE, null);
        String phone = mPreference.getString(AppConstants.PHONE,null);
        String saivianId = mPreference.getString(AppConstants.SAIVIAN_ID,null);

        UserDetail details = null;
        if(country != null && firstName != null && lastName != null && email != null && password != null) {
            details = new UserDetail(id, referralId,firstName,lastName,email,password,profileImage,gender,dob,city, state,zip,country, isVerified,isActive,createdDate,phone, saivianId);
        }
        return details;
    }
    public static void saveDataInPreferences(String key, String value, Context c) {

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(c).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getDataFromPreferences(String key, String defaultValue, Context c) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(c);
        String value = preferences.getString(key, defaultValue);

        return value;
    }

    public static void clearAllPreferences(Context context) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.clear();
        editor.commit();
    }
}
