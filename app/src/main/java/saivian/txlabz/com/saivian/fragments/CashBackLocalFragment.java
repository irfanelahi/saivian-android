package saivian.txlabz.com.saivian.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.adapters.CashBlockLocalItemAdapter;
import saivian.txlabz.com.saivian.models.CashBackCategory;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;

/**
 * Created by Fatima Siddique on 5/4/2016.
 */
public class CashBackLocalFragment extends Fragment implements View.OnClickListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ArrayList<CashBackCategory> cashBackCategories;
    Context context;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cash_back_local, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
//        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        MainStorageUtility utility = MainStorageUtility.getInstance(context.getApplicationContext());
        cashBackCategories = utility.getCashBackCategoryArrayList();
        setupViewPager();
//        getCashBackCategories();
    }
    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        for(int i = 0;i<cashBackCategories.size();i++){
            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstants.CASH_BACK_CATEGORY,cashBackCategories.get(i));
            CashBlockLocalItemFragment cashBlockLocalItem = new CashBlockLocalItemFragment();
            cashBlockLocalItem.setArguments(bundle);
            adapter.addFragment(cashBlockLocalItem, cashBackCategories.get(i).getCategoryName());
        }
        viewPager.setOffscreenPageLimit(cashBackCategories.size());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public void onClick(View view) {

    }
    private void getCashBackCategories(){
        if(cashBackCategories != null)
            cashBackCategories.clear();
        cashBackCategories = new ArrayList<>();
        GeneralUtil.showLoadingDialog(context);
        Ion.with(this)
                .load(AppConstants.GET,AppConstants.GET_MERCHANT_CATEGORIES)
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();
                Log.i(AppConstants.TAG,"result is : "+result);
                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            JSONArray cashBackDataArray = Json1.getJSONArray(AppConstants.CATEGORIES);
                            for(int i = 0;i<cashBackDataArray.length();i++){
                                JSONObject categoryObj = cashBackDataArray.getJSONObject(i);
                                String id = categoryObj.getString(AppConstants.ID);
                                String categoryName = categoryObj.getString(AppConstants.CATEGORY_NAME);
                                String description = categoryObj.getString(AppConstants.DESCRIPTION);
                                String isActive = categoryObj.getString(AppConstants.IS_ACTIVE);

                                cashBackCategories.add(new CashBackCategory(id, categoryName, description, isActive));

                            }
                            setupViewPager();
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error: "+e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }
}
