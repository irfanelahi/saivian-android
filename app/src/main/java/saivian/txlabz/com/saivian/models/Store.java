package saivian.txlabz.com.saivian.models;

import saivian.txlabz.com.saivian.utils.AppConstants;

/**
 * Created by Fatima Siddique on 5/5/2016.
 */
public class Store {
    String storeId;
    String storeName;
    String webUrl;
    String phoneNum;
    String profileImage;
    String featuredImage;
    String address;
    String city;
    String state;
    String zip;
    String latitude;
    String longitude;
    AppConstants.TOP10_STATE top10State;

    public Store(String storeId, String storeName, String webUrl, String phoneNum, String profileImage, String featuredImage,String address, String city, String state, String zip, String latitude, String longitude) {
        this.storeId = storeId;
        this.storeName = storeName;
        this.webUrl = webUrl;
        this.phoneNum = phoneNum;
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.latitude = latitude;
        this.longitude = longitude;
        this.profileImage = profileImage;
        this.featuredImage = featuredImage;
        top10State = AppConstants.TOP10_STATE.NORMAL;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public AppConstants.TOP10_STATE getTop10State() {
        return top10State;
    }

    public void setTop10State(AppConstants.TOP10_STATE top10State) {
        this.top10State = top10State;
    }
}
