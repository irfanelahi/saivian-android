package saivian.txlabz.com.saivian.models;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 5/17/2016.
 */
public class CashBack {
    String id;
    String categoryName;
    String description;
    String image;
    String isActive;
    ArrayList<CashBackStore>storeArrayList;

    public CashBack(String id, String categoryName, String description, String image, String isActive, ArrayList<CashBackStore> storeArrayList) {
        this.id = id;
        this.categoryName = categoryName;
        this.description = description;
        this.image = image;
        this.isActive = isActive;
        this.storeArrayList = storeArrayList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public ArrayList<CashBackStore> getStoreArrayList() {
        return storeArrayList;
    }

    public void setStoreArrayList(ArrayList<CashBackStore> storeArrayList) {
        this.storeArrayList = storeArrayList;
    }
}
