package saivian.txlabz.com.saivian.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.fragments.FavoriteStoresFragment;
import saivian.txlabz.com.saivian.fragments.Top10StoresFragment;
import saivian.txlabz.com.saivian.models.CashBackStore;
import saivian.txlabz.com.saivian.models.OpeningHour;
import saivian.txlabz.com.saivian.models.Store;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.views.ResizeAnimation;
import saivian.txlabz.com.saivian.views.SupportMap;


/**
 * Created by Fatima Siddique on 3/20/2016.
 */
public class Top10StoreItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<CashBackStore> data = Collections.emptyList();
    List<Store> allStoresList = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    int TYPE_ITEM = 0;
    int TYPE_BOTTOM = 1;
    Top10StoresFragment fragment;
    private GoogleMap mGoogleMap;
    ImageLoader imageLoader;
    private final HashSet<MapView> mMaps = new HashSet<MapView>();
    DisplayImageOptions options;
    public Top10StoreItemAdapter(Context context, List<CashBackStore> data, List<Store>allStoresList, Top10StoresFragment fragment) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.allStoresList = allStoresList;
        this.fragment = fragment;
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.progress)
                .showImageForEmptyUri(R.drawable.progress)
                .showImageOnFail(R.drawable.progress).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_BOTTOM){
            View view = inflater.inflate(R.layout.layout_top10_item_bottom, parent, false);
            BottomViewHolder holder = new BottomViewHolder(view);
            return holder;
        }else {
            View view = inflater.inflate(R.layout.layout_top10_store_item, parent, false);
            ItemViewHolder holder = new ItemViewHolder(view);
            return holder;
        }
    }
    @Override
    public int getItemViewType(int position) {
        if (position == data.size()-1)
            return TYPE_BOTTOM;
        return TYPE_ITEM;
    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof ItemViewHolder) {
            final CashBackStore store = data.get(position);
            final ItemViewHolder itemViewHolder = (ItemViewHolder)holder;

            itemViewHolder.mainListContainer.removeAllViews();
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = null;
            final AppConstants.CASH_BACK_STATE state = data.get(position).getState();
            int resource = 0;
            if (state == AppConstants.CASH_BACK_STATE.ITEM_SELECTED || state == AppConstants.CASH_BACK_STATE.NORMAL) {
                resource = R.layout.layout_pressed_image;
            }
            else if (state == AppConstants.CASH_BACK_STATE.MAP_SELECTED) {
                resource = R.layout.layout_pressed_map;

            }
            else if (state == AppConstants.CASH_BACK_STATE.HOURS_SELECTED) {
                resource = R.layout.layout_opening_hours;
            }
            v = vi.inflate(resource, null);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            if (state == AppConstants.CASH_BACK_STATE.ITEM_SELECTED || state == AppConstants.CASH_BACK_STATE.NORMAL) {
                ImageView featuredImage = (ImageView) v.findViewById(R.id.list_item);
                imageLoader.displayImage(store.getProfileImage(),featuredImage,options);
            }

            if(!alreadyInitialed(itemViewHolder.mapView)) {
                itemViewHolder.initializeMapView();
                mMaps.add(itemViewHolder.mapView);
            }

            if(store.getLocation() != null && store.getLocation().getLongitude() != null && store.getLocation().getLatitude() != null) {
                if(!store.getLocation().getLongitude().isEmpty() && !store.getLocation().getLatitude().isEmpty()) {
                    double lat = Double.parseDouble(store.getLocation().getLatitude());
                    double lng = Double.parseDouble(store.getLocation().getLongitude());
                    LatLng item = new LatLng(lat, lng);
                    itemViewHolder.mapView.setTag(item);
                    if (itemViewHolder.googleMap != null) {
                        setMapLocation(itemViewHolder.googleMap, item);
                    }
                }
            }
            populateView(v,state,position,itemViewHolder);

            v.setLayoutParams(layoutParams);
            itemViewHolder.mainListContainer.addView(v);

            if(store.getIsFavorite().equals("1"))
                itemViewHolder.favoriteIcon.setImageResource(R.drawable.fave_icon_active);
            else
                itemViewHolder.favoriteIcon.setImageResource(R.drawable.fave_icon);
            itemViewHolder.storeName.setText(data.get(position).getStoreName());
            itemViewHolder.storeNameLayout2.setText(data.get(position).getStoreName());
            itemViewHolder.phone.setText(store.getPhoneNumber());
            itemViewHolder.website.setText(store.getWebUrl());
            if(store.getLocation() != null) {
                itemViewHolder.address1.setText(store.getLocation().getAddress());
                itemViewHolder.address2.setText(store.getLocation().getCity() + " " + store.getLocation().getState());
            }
            imageLoader.displayImage(store.getProfileImage(),itemViewHolder.imageForAnim,options);

            if(store.getTop10State() == AppConstants.TOP10_STATE.NORMAL) {
                itemViewHolder.layout1.setVisibility(View.VISIBLE);
                itemViewHolder.layout2.setVisibility(View.GONE);
            }else if(store.getTop10State() == AppConstants.TOP10_STATE.ITEM_COLLAPSE){
                collapseListView(itemViewHolder,position);
            } else {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) itemViewHolder.layout2.getLayoutParams();
                lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                itemViewHolder.layout2.setLayoutParams(lp);
                itemViewHolder.layout1.setVisibility(View.GONE);
                itemViewHolder.layout2.setVisibility(View.VISIBLE);
            }
            itemViewHolder.layout1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(shouldExpand()) {
                        fragment.scrollToPosition(position);
                        expandListView(itemViewHolder, position);
                    }
                }
            });

            itemViewHolder.mapIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(data.get(position).getState() != AppConstants.CASH_BACK_STATE.MAP_SELECTED) {
                        if (!store.getLocation().getLatitude().equals("") || !(store.getLocation().getLongitude().equals(""))) {
                            ((MainActivity) context).applyAnimationOnParentView(false);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    data.get(position).setState(AppConstants.CASH_BACK_STATE.MAP_SELECTED);
                                    notifyItemChanged(position);
                                }
                            }, 60);
                        } else {
                            GeneralUtil.showAlert(context, "Location not setup for this Store");
                        }
                    }
                    return;
                }
            });
            itemViewHolder.openHoursIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (data.get(position).getState() != AppConstants.CASH_BACK_STATE.HOURS_SELECTED) {
                        ((MainActivity) context).applyAnimationOnParentView(false);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                data.get(position).setState(AppConstants.CASH_BACK_STATE.HOURS_SELECTED);
                                notifyItemChanged(position);
                            }
                        }, 60);
                    }
                }
            });
            itemViewHolder.phoneIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!data.get(position).getPhoneNumber().equals(""))
                        fragment.makeCall(data.get(position).getPhoneNumber());
                    else
                        GeneralUtil.showAlert(context,"No phone number setup for this store");
                }
            });
            itemViewHolder.webUrlIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    GeneralUtil.openBrowserForInstall(context,data.get(position).getWebUrl());
                }
            });
            itemViewHolder.storeNameLayout2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(state == AppConstants.CASH_BACK_STATE.MAP_SELECTED | state == AppConstants.CASH_BACK_STATE.HOURS_SELECTED) {
                        ((MainActivity) context).applyAnimationOnParentView(true);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                data.get(position).setState(AppConstants.CASH_BACK_STATE.ITEM_SELECTED);
                                notifyItemChanged(position);
                            }
                        }, 60);
                    } else {
                        data.get(position).setState(AppConstants.CASH_BACK_STATE.NORMAL);
                        notifyItemChanged(position);
                    }

                }
            });
            itemViewHolder.favoriteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            fragment.addFavorite(data.get(position),position);
                            notifyItemChanged(position);

                        }
                    });
                }
            });

        }else{
            final BottomViewHolder itemViewHolder = (BottomViewHolder) holder;
//            createStoreNameSpinner(itemViewHolder.inputEditText);

            itemViewHolder.storeState.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.scrollToBottom();
                }
            });
            itemViewHolder.zip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.scrollToBottom();
                }
            });
            itemViewHolder.storePhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.scrollToBottom();
                }
            });
            itemViewHolder.submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isDataValid(itemViewHolder)){
//                        int selectedPos = itemViewHolder.storeName.getSelectedItemPosition();
                        fragment.addStore(itemViewHolder.inputEditText.getText().toString(),itemViewHolder.storeAddress.getText().toString(),
                                itemViewHolder.storeState.getText().toString(),itemViewHolder.zip.getText().toString(),itemViewHolder.storePhone.getText().toString(),itemViewHolder.storeCity.getText().toString());
                        itemViewHolder.inputEditText.setText("");
                        itemViewHolder.storeAddress.setText("");
                        itemViewHolder.storePhone.setText("");
                        itemViewHolder.storeState.setText("");
                        itemViewHolder.storeCity.setText("");
                        itemViewHolder.zip.setText("");
                    }
                }
            });
        }
    }
    private void expandListView(final ItemViewHolder itemViewHolder, final int position){
        data.get(position).setTop10State(AppConstants.TOP10_STATE.ITEM_EXPAND);
        imageLoader.displayImage(data.get(position).getProfileImage(),itemViewHolder.imageForAnim,options);
        itemViewHolder.imageForAnim.setDrawingCacheEnabled(true);
        itemViewHolder.imageForAnim.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        itemViewHolder.imageForAnim.layout(0, 0, itemViewHolder.imageForAnim.getMeasuredWidth(), itemViewHolder.imageForAnim.getMeasuredHeight());

        itemViewHolder.imageForAnim.buildDrawingCache(true);
        Bitmap bm = Bitmap.createBitmap(itemViewHolder.imageForAnim.getDrawingCache());
        itemViewHolder.imageForAnim.setDrawingCacheEnabled(false); // clear drawing cache
        int height = (bm.getHeight()-context.getResources().getInteger(R.integer.bitmap_height));
        if(height > 0)
            bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),  height);
        else{
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(),R.drawable.progress);
            bm = Bitmap.createBitmap(icon, 0, 0, bm.getWidth(),  height);
        }
        itemViewHolder.imageForAnim.setImageBitmap(bm);
        itemViewHolder.imageForAnim.setVisibility(View.VISIBLE);
        final Animation animSlide = AnimationUtils.loadAnimation(context, R.anim.slide_in_right);
        itemViewHolder.imageForAnim.startAnimation(animSlide);

        animSlide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                itemViewHolder.layout1.setVisibility(View.GONE);
                itemViewHolder.imageForAnim.setVisibility(View.GONE);
                animSlide.cancel();
                animSlide.reset();
                itemViewHolder.imageForAnim.clearAnimation();
                final ResizeAnimation resizeAnimation = new ResizeAnimation(itemViewHolder.layout2,GeneralUtil.dpToPx((context.getResources().getInteger(R.integer.store_10_height))), 1);
                resizeAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        itemViewHolder.layout2.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        resizeAnimation.cancel();
                        resizeAnimation.reset();
                        resizeAnimation.setFillAfter(false);
                        itemViewHolder.layout2.clearAnimation();
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) itemViewHolder.layout2.getLayoutParams();
                        lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        itemViewHolder.layout2.setLayoutParams(lp);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                resizeAnimation.setDuration(200);
                resizeAnimation.setFillAfter(true);
                itemViewHolder.layout2.startAnimation(resizeAnimation);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
    private void collapseListView(final ItemViewHolder itemViewHolder, final int position){
        itemViewHolder.imageForAnim.setDrawingCacheEnabled(true);
        itemViewHolder.imageForAnim.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        itemViewHolder.imageForAnim.layout(0, 0, itemViewHolder.imageForAnim.getMeasuredWidth(), itemViewHolder.imageForAnim.getMeasuredHeight());


        itemViewHolder.imageForAnim.buildDrawingCache(true);
        Bitmap bm = Bitmap.createBitmap(itemViewHolder.imageForAnim.getDrawingCache());
        itemViewHolder.imageForAnim.setDrawingCacheEnabled(false);
        int height = bm.getHeight()-(bm.getHeight()-context.getResources().getInteger(R.integer.bitmap_height));
        Log.i(AppConstants.TAG,"Height is : " + height);
        if(height <= bm.getHeight())
            bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), height);

        final ResizeAnimation resizeAnimation = new ResizeAnimation(itemViewHolder.layout2,height, GeneralUtil.dpToPx((context.getResources().getInteger(R.integer.store_10_height))));
        final Bitmap finalBm = bm;
        resizeAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                resizeAnimation.cancel();
                resizeAnimation.reset();
                resizeAnimation.setFillAfter(false);
                itemViewHolder.layout2.clearAnimation();
                itemViewHolder.imageForAnim.setImageBitmap(finalBm);
                itemViewHolder.imageForAnim.setVisibility(View.VISIBLE);
                itemViewHolder.layout2.setVisibility(View.GONE);
//                imageLoader.displayImage(data.get(position).getFeaturedImage(),itemViewHolder.imageForAnim,options);

                final Animation animSlide = AnimationUtils.loadAnimation(context, R.anim.slide_out_right);
                itemViewHolder.imageForAnim.startAnimation(animSlide);

                animSlide.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        itemViewHolder.imageForAnim.setVisibility(View.GONE);
                        animSlide.cancel();
                        animSlide.reset();
                        itemViewHolder.imageForAnim.clearAnimation();
                        itemViewHolder.imageForAnim.setVisibility(View.GONE);
                        itemViewHolder.layout1.setVisibility(View.VISIBLE);
                        data.get(position).setTop10State(AppConstants.TOP10_STATE.NORMAL);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        resizeAnimation.setDuration(200);
        resizeAnimation.setFillAfter(true);
        itemViewHolder.layout2.startAnimation(resizeAnimation);

    }
    private boolean shouldExpand(){
        for(int i = 0; i < data.size();i++){
            if(data.get(i).getTop10State() == AppConstants.TOP10_STATE.ITEM_EXPAND){
                data.get(i).setTop10State(AppConstants.TOP10_STATE.ITEM_COLLAPSE);
                notifyItemChanged(i);
                return false;
            }
        }
        return true;
    }
    private boolean isDataValid(BottomViewHolder itemViewHolder) {
        boolean result = true;
        if(itemViewHolder.storeAddress.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter Store Address");
        }else if(itemViewHolder.storePhone.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter Store Phone Number");
        }else if(itemViewHolder.storeState.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter Store State");
        }else if(itemViewHolder.storeCity.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter Store City");
        }
        else if(itemViewHolder.zip.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter Store Zip");
        }
        else if(!itemViewHolder.storeState.getText().toString().equals("")){
            Pattern ps = Pattern.compile("^[a-zA-Z ]+$");
            Matcher ms = ps.matcher(itemViewHolder.storeState.getText().toString());
            boolean bs = ms.matches();
            if (bs == false) {
                result = false;
                GeneralUtil.showAlert(context,"Enter Valid State Name");
            }
        }
        return result;
    }
    private void populateView(View v, final AppConstants.CASH_BACK_STATE state, int position, ItemViewHolder viewHolder) {
        final CashBackStore store = data.get(position);
        if (state == AppConstants.CASH_BACK_STATE.ITEM_SELECTED || state == AppConstants.CASH_BACK_STATE.ITEM_SELECTED){
//            ImageView featuredImage = (ImageView) v.findViewById(R.id.list_item);
//            imageLoader.displayImage(store.getProfileImage(),featuredImage,options);
        }
        else if (state == AppConstants.CASH_BACK_STATE.MAP_SELECTED) {
            viewHolder.mapView.setVisibility(View.VISIBLE);
//            final SupportMap mMapFragment = new SupportMap();
//
//            SupportMap.MapViewCreatedListener mapViewCreatedListener = new SupportMap.MapViewCreatedListener() {
//                @Override
//                public void onMapCreated() {
//                    mGoogleMap = mMapFragment.getMap();
//                    if (mGoogleMap != null) {
//                        setupMap(mGoogleMap,store.getLocation().getLatitude(),store.getLocation().getLongitude());
//                    }
//
//                }
//            };
//            mMapFragment.itsMapViewCreatedListener = mapViewCreatedListener;
//
//            FragmentManager fm = fragment.getChildFragmentManager();
//            SupportMapFragment supportMapFragment = mMapFragment;
//            fm.beginTransaction().replace(R.id.layout_map, supportMapFragment).commit();

        }
        else if (state == AppConstants.CASH_BACK_STATE.HOURS_SELECTED){
            TextView sundayTiming = (TextView)v.findViewById(R.id.lbl_sunday_timing);
            TextView mondayTiming = (TextView)v.findViewById(R.id.lbl_monday_timing);
            TextView tuesdayTiming = (TextView)v.findViewById(R.id.lbl_tuesday_timing);
            TextView wednesdayTiming = (TextView)v.findViewById(R.id.lbl_wednesday_timing);
            TextView thursdayTiming = (TextView)v.findViewById(R.id.lbl_thursday_timing);
            TextView fridayTiming = (TextView)v.findViewById(R.id.lbl_friday_timing);
            TextView saturdayTiming = (TextView)v.findViewById(R.id.lbl_saturday_timing);

            OpeningHour openingHour = store.getOpeningHour();
            sundayTiming.setText(openingHour.getSunday());
            mondayTiming.setText(openingHour.getMonday());
            tuesdayTiming.setText(openingHour.getTuesday());
            wednesdayTiming.setText(openingHour.getWednesday());
            thursdayTiming.setText(openingHour.getThursday());
            fridayTiming.setText(openingHour.getFriday());
            saturdayTiming.setText(openingHour.getSaturday());
        }
    }
    protected void setupMap(GoogleMap map, String lat, String lng) {

        mGoogleMap = map;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        Log.d(AppConstants.TAG, "Map Ready");

        if (mGoogleMap != null) {
            updateMarker(lat, lng);
        }

    }
    private void updateMarker(String lat, String lng) {
        Double mLat = Double.parseDouble(lat);
        Double mLng = Double.parseDouble(lng);
        LatLng mLatLng = new LatLng(mLat,mLng);

        MarkerOptions mMarkerOptions = new MarkerOptions();

        if (mGoogleMap != null) {

            mGoogleMap.addMarker(mMarkerOptions
                    .position(mLatLng)
                    .draggable(false)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.map_marker)));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng,14));
        }

    }
    @Override
    public int getItemCount() {
        return data.size();
    }
    private static void setMapLocation(GoogleMap map, LatLng data) {
        // Add a marker for this item and set the camera
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(data, 14f));
        map.addMarker(new MarkerOptions().position(data));

        // Set the map type back to normal.
        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

    }
    private boolean alreadyInitialed(MapView mapView) {

        for (MapView map : mMaps) {
            if(map == mapView)
                return true;
        }
        return false;
    }
    class ItemViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        TextView storeName;
        View layout1, layout2;
        ImageView imageForAnim;
//        ImageView featuredImage;
        TextView storeNameLayout2, address1, address2, website, phone;
        ImageView favoriteIcon, mapIcon, phoneIcon, openHoursIcon, webUrlIcon;
        LinearLayout mainListContainer;
        MapView mapView;
        GoogleMap googleMap;
        public ItemViewHolder(View itemView) {
            super(itemView);
            storeName = (TextView) itemView.findViewById(R.id.txt_store_name);
            layout1 = itemView.findViewById(R.id.layout_1);
            layout2 = itemView.findViewById(R.id.layout_2);
            imageForAnim = (ImageView)itemView.findViewById(R.id.anim_image);
//            featuredImage = (ImageView)itemView.findViewById(R.id.featured_image);
            storeNameLayout2 = (TextView)itemView.findViewById(R.id.txt_store_name_l2);
            address1 = (TextView) itemView.findViewById(R.id.txt_address_1);
            address2 = (TextView) itemView.findViewById(R.id.txt_address_2);
            website = (TextView) itemView.findViewById(R.id.txt_website);
            phone = (TextView) itemView.findViewById(R.id.txt_phone);
            favoriteIcon = (ImageView)itemView.findViewById(R.id.icon_fav);
            mapIcon = (ImageView)itemView.findViewById(R.id.img_map);
            phoneIcon = (ImageView)itemView.findViewById(R.id.img_phone);
            openHoursIcon = (ImageView)itemView.findViewById(R.id.img_openHours);
            webUrlIcon = (ImageView) itemView.findViewById(R.id.img_website);
            mainListContainer = (LinearLayout)itemView.findViewById(R.id.main_list_container);
            mapView         = (MapView) itemView.findViewById(R.id.mapView);
        }
        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(context);
            this.googleMap = googleMap;
            LatLng data = (LatLng) mapView.getTag();
            if (data != null) {
                setMapLocation(googleMap, data);
            }
        }

        /**
         * Initialises the MapView by calling its lifecycle methods.
         */
        public void initializeMapView() {
            if (mapView != null) {
                // Initialise the MapView
                mapView.onCreate(null);
                // Set the map ready callback to receive the GoogleMap object
                mapView.getMapAsync(this);
            }
        }
    }

    class BottomViewHolder extends RecyclerView.ViewHolder {
//        Spinner storeName;
        EditText storePhone;
        EditText storeAddress;
        EditText storeState;
        EditText storeCity;
        TextView zip;
        Button cancel;
        Button submit;
        EditText inputEditText;

        public BottomViewHolder(View itemView) {
            super(itemView);
//            storeName = (Spinner) itemView.findViewById(R.id.spinner_store_name);
            storePhone = (EditText) itemView.findViewById(R.id.et_store_phone);
            storeAddress = (EditText) itemView.findViewById(R.id.et_store_address);
            storeState = (EditText) itemView.findViewById(R.id.et_store_state);
            storeCity = (EditText) itemView.findViewById(R.id.et_store_city);
            zip = (TextView)itemView.findViewById(R.id.et_store_zip);
            cancel = (Button)itemView.findViewById(R.id.btn_cancel);
            submit = (Button)itemView.findViewById(R.id.btn_submit);
            inputEditText = (EditText) itemView.findViewById(R.id.spinner_name_of_store);

        }
    }
}