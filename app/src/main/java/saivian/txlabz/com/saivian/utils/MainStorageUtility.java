package saivian.txlabz.com.saivian.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import saivian.txlabz.com.saivian.models.CashBack;
import saivian.txlabz.com.saivian.models.CashBackCategory;
import saivian.txlabz.com.saivian.models.CashBackStore;
import saivian.txlabz.com.saivian.models.Location;
import saivian.txlabz.com.saivian.models.OpeningHour;
import saivian.txlabz.com.saivian.models.PaymentInfo;
import saivian.txlabz.com.saivian.models.Receipt;
import saivian.txlabz.com.saivian.models.Store;
import saivian.txlabz.com.saivian.models.UserDetail;

/**
 * Created by Fatima Siddique on 5/30/2016.
 */
public class MainStorageUtility {

    static MainStorageUtility utility;
    Delegate delegate;
    ArrayList<Store> fetchAllStoreArrayList;
    ArrayList<CashBackCategory>cashBackCategoryArrayList;
    ArrayList<CashBack>cashBackArrayList;
    ArrayList<CashBackStore>top10StoresArrayList;
    ArrayList<CashBackStore>favoriteStoresArrayList;
    ArrayList<Receipt>receiptArrayList;
    PaymentInfo paymentInfo;
    boolean isDataLoaded = false;

    Context context;
    Long monthCashBack = -1L, yearCashBack = -1L;

    private MainStorageUtility(Context context){
        this.context = context;
        if(cashBackArrayList == null) {
            fetchAllStoreArrayList = new ArrayList<>();
            cashBackCategoryArrayList = new ArrayList<>();
            cashBackArrayList = new ArrayList<>();
            favoriteStoresArrayList = new ArrayList<>();
            top10StoresArrayList = new ArrayList<>();
            receiptArrayList = new ArrayList<>();
        }
    }

    public static MainStorageUtility getInstance(Context context){
        if(utility == null)
            utility = new MainStorageUtility(context);
        return utility;
    }
    public void setDelegate(Delegate delegate){
        this.delegate = delegate;
    }

    public void  getDataInBackground(){
        isDataLoaded = false;
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                Ion.with(context.getApplicationContext())
                        .load(AppConstants.GET,AppConstants.FETCH_CASH_BACK_LOCAL+"?customer_id="+ StorageUtility.getLoginModel(context.getApplicationContext()).getId())
                        .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                        .setTimeout(20*1000)
                        .asString().setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        GeneralUtil.dismissLoadingDialog();
                        if (e == null) {
                            processJSON(result);
                        }
                        else {
                            isDataLoaded = true;
                            delegate.callback(AppConstants.FAILURE);
                            Log.i(AppConstants.TAG, "message: " + e.getMessage());
                        }
                    }
                });
                return null;
            }

        }.execute(null, null, null);

    }
    public void processJSON(String result){
        Log.i(AppConstants.TAG,"result is : " + result);

        try {

            JSONObject Json1 = new JSONObject(result);
            int status = Json1.getInt(AppConstants.STATUS);
            if (status == AppConstants.STATUS_SUCCESS) {
                String month = Json1.getString(AppConstants.MONTH_CASH_BACK);
                String year = Json1.getString(AppConstants.YEAR_CASH_BACK);

                if(isInteger(month))
                    monthCashBack = Long.valueOf(Integer.parseInt(month));
                else
                    monthCashBack = 0L;
                if(isInteger(year))
                    yearCashBack = Long.valueOf(Integer.parseInt(year));
                else
                    yearCashBack = 0L;
                JSONObject customerJObj = Json1.getJSONObject(AppConstants.CUSTOMER_DATA);
                if(customerJObj != null) {
                    String customerId = customerJObj.getString(AppConstants.ID);
                    String referralId = customerJObj.getString(AppConstants.REFERRAL_ID);
                    String firstName = customerJObj.getString(AppConstants.FIRST_NAME);
                    String lastName = customerJObj.getString(AppConstants.LAST_NAME);
                    String email = customerJObj.getString(AppConstants.EMAIL);
                    String password = customerJObj.getString(AppConstants.PASSWORD);
                    String profileImage = customerJObj.getString(AppConstants.PROFILE_IMAGE);
                    String gender = customerJObj.getString(AppConstants.GENDER);
                    String dob = customerJObj.getString(AppConstants.DOB);
                    String city = customerJObj.getString(AppConstants.CITY);
                    String state = customerJObj.getString(AppConstants.STATE);
                    String zip = customerJObj.getString(AppConstants.ZIP);
                    String country = customerJObj.getString(AppConstants.COUNTRY);
                    String isVerified = customerJObj.getString(AppConstants.IS_VERIFIED);
                    String isActive = customerJObj.getString(AppConstants.IS_ACTIVE);
                    String createdDate = customerJObj.getString(AppConstants.CREATED_DATE);
                    String phone = customerJObj.getString(AppConstants.PHONE);
                    String saivianId = customerJObj.getString(AppConstants.SAIVIAN_ID);
                    if(firstName != null && !firstName.equals(""))
                        firstName = firstName.substring(0,1).toUpperCase() + firstName.substring(1);
                    if(lastName != null && !lastName.equals(""))
                        lastName = lastName.substring(0,1).toUpperCase() + lastName.substring(1);

                    UserDetail userDetail = new UserDetail(customerId, referralId, firstName, lastName, email, password, profileImage, gender, dob, city, state, zip, country, isVerified, isActive, createdDate,phone, saivianId);
                    StorageUtility.saveLoginObject(userDetail, context);
                }
                JSONArray cashBackDataArray = Json1.getJSONArray(AppConstants.CASH_BACK_DATA);
                for (int i = 0;i<cashBackDataArray.length();i++){
                    JSONObject categoryObj = cashBackDataArray.getJSONObject(i);
                    String id = categoryObj.getString(AppConstants.ID);
                    String categoryName = categoryObj.getString(AppConstants.CATEGORY_NAME);
                    String descriptionCategory = categoryObj.getString(AppConstants.DESCRIPTION);
                    String image = categoryObj.getString(AppConstants.IMAGE);
                    String isActive = categoryObj.getString(AppConstants.IS_ACTIVE);
                    JSONArray storesArray = categoryObj.getJSONArray(AppConstants.STORES_LIST);
                    ArrayList<CashBackStore> cashBackStoreArrayList = new ArrayList<CashBackStore>();
                    for(int j = 0; j < storesArray.length(); j++){
                        JSONObject storeObj = storesArray.getJSONObject(j);
                        String storeId = storeObj.getString(AppConstants.STORE_ID);
                        String firstName = storeObj.getString(AppConstants.FIRST_NAME);
                        String lastName = storeObj.getString(AppConstants.LAST_NAME);
                        String storeName = storeObj.getString(AppConstants.STORE_NAME);
                        String description = storeObj.getString(AppConstants.DESCRIPTION);
                        String email = storeObj.getString(AppConstants.EMAIL);
                        String phoneNumber = storeObj.getString(AppConstants.PHONE);
                        String webUrl = storeObj.getString(AppConstants.WEB_URL);
                        String isFavorite = storeObj.getString(AppConstants.IS_FAVOURITE);
                        String profileImage = storeObj.getString(AppConstants.PROFILE_IMAGE);
                        String featuredImage = storeObj.getString(AppConstants.FEATURED_IMAGE);

                        if(storeName != null && !storeName.equals(""))
                            storeName= storeName.substring(0,1).toUpperCase() + storeName.substring(1);
                        JSONObject openingHourJSON = storeObj.getJSONObject(AppConstants.OPENING_HOURS);

                        String monday = openingHourJSON.getString(AppConstants.MONDAY);
                        String tuesday = openingHourJSON.getString(AppConstants.TUESDAY);
                        String wednesday = openingHourJSON.getString(AppConstants.WEDNESDAY);
                        String thursday = openingHourJSON.getString(AppConstants.THURSDAY);
                        String friday = openingHourJSON.getString(AppConstants.FRIDAY);
                        String saturday = openingHourJSON.getString(AppConstants.SATURDAY);
                        String sunday = openingHourJSON.getString(AppConstants.SUNDAY);

                        JSONObject locationJSON = storeObj.getJSONObject(AppConstants.LOCATION);
                        String address = locationJSON.getString(AppConstants.ADDRESS);
                        String city = locationJSON.getString(AppConstants.CITY);
                        String state = locationJSON.getString(AppConstants.STATE);
                        String zip = locationJSON.getString(AppConstants.ZIP);
                        String country = locationJSON.getString(AppConstants.COUNTRY);
                        String latitude = locationJSON.getString(AppConstants.LATITUDE);
                        String longitude = locationJSON.getString(AppConstants.LONGITUDE);

                        OpeningHour openingHour = new OpeningHour(monday, tuesday, wednesday, thursday, friday, saturday, sunday);
                        Location location = new Location(address,city, state, zip, country, latitude,longitude);

                        cashBackStoreArrayList.add(new CashBackStore(storeId,firstName,lastName, storeName,description,email,phoneNumber, webUrl,isFavorite,profileImage, featuredImage, openingHour, location));
                    }
                    cashBackCategoryArrayList.add(new CashBackCategory(id,categoryName,image,isActive));
                    cashBackArrayList.add(new CashBack(id,categoryName,descriptionCategory,image,isActive,cashBackStoreArrayList));
                }
                JSONArray favoritesStoresArray = Json1.getJSONArray(AppConstants.FAVORITE_STORES);
                for (int i = 0; i<favoritesStoresArray.length();i++){
                    JSONObject storeObj = favoritesStoresArray.getJSONObject(i);
                    String storeId = storeObj.getString(AppConstants.STORE_ID);
                    String firstName = storeObj.getString(AppConstants.FIRST_NAME);
                    String lastName = storeObj.getString(AppConstants.LAST_NAME);
                    String storeName = storeObj.getString(AppConstants.STORE_NAME);
                    String description = storeObj.getString(AppConstants.DESCRIPTION);
                    String email = storeObj.getString(AppConstants.EMAIL);
                    String phoneNumber = storeObj.getString(AppConstants.PHONE);
                    String webUrl = storeObj.getString(AppConstants.WEB_URL);
                    String isFavorite = storeObj.getString(AppConstants.IS_FAVOURITE);
                    String profileImage = storeObj.getString(AppConstants.PROFILE_IMAGE);
                    String featuredImage = storeObj.getString(AppConstants.FEATURED_IMAGE);

                    if(storeName != null && !storeName.equals(""))
                        storeName = storeName.substring(0,1).toUpperCase() + storeName.substring(1);

                    if(description != null && !description.equals(""))
                        description = description.substring(0,1).toUpperCase() + description.substring(1);

                    JSONObject openingHourJSON = storeObj.getJSONObject(AppConstants.OPENING_HOURS);

                    String monday = openingHourJSON.getString(AppConstants.MONDAY);
                    String tuesday = openingHourJSON.getString(AppConstants.TUESDAY);
                    String wednesday = openingHourJSON.getString(AppConstants.WEDNESDAY);
                    String thursday = openingHourJSON.getString(AppConstants.THURSDAY);
                    String friday = openingHourJSON.getString(AppConstants.FRIDAY);
                    String saturday = openingHourJSON.getString(AppConstants.SATURDAY);
                    String sunday = openingHourJSON.getString(AppConstants.SUNDAY);

                    JSONObject locationJSON = storeObj.getJSONObject(AppConstants.LOCATION);
                    String address = locationJSON.getString(AppConstants.ADDRESS);
                    String city = locationJSON.getString(AppConstants.CITY);
                    String state = locationJSON.getString(AppConstants.STATE);
                    String zip = locationJSON.getString(AppConstants.ZIP);
                    String country = locationJSON.getString(AppConstants.COUNTRY);
                    String latitude = locationJSON.getString(AppConstants.LATITUDE);
                    String longitude = locationJSON.getString(AppConstants.LONGITUDE);

                    OpeningHour openingHour = new OpeningHour(monday, tuesday, wednesday, thursday, friday, saturday, sunday);
                    Location location = new Location(address,city, state, zip, country, latitude,longitude);

                    favoriteStoresArrayList.add(new CashBackStore(storeId,firstName,lastName, storeName,description,email,phoneNumber, webUrl,isFavorite,profileImage, featuredImage, openingHour, location));
                }
                JSONArray top10StoresArray = Json1.getJSONArray(AppConstants.TOP_10_STORES_LIST);
                for (int i = 0; i<top10StoresArray.length();i++){
                    JSONObject storeObj = top10StoresArray.getJSONObject(i);
                    String storeId = storeObj.getString(AppConstants.STORE_ID);
                    String firstName = storeObj.getString(AppConstants.FIRST_NAME);
                    String lastName = storeObj.getString(AppConstants.LAST_NAME);
                    String storeName = storeObj.getString(AppConstants.STORE_NAME);
                    String description = storeObj.getString(AppConstants.DESCRIPTION);
                    String email = storeObj.getString(AppConstants.EMAIL);
                    String phoneNumber = storeObj.getString(AppConstants.PHONE);
                    String webUrl = storeObj.getString(AppConstants.WEB_URL);
                    String isFavorite = storeObj.getString(AppConstants.IS_FAVOURITE);
                    String profileImage = storeObj.getString(AppConstants.PROFILE_IMAGE);
                    String featuredImage = storeObj.getString(AppConstants.FEATURED_IMAGE);

                    if(storeName != null && !storeName.equals(""))
                        storeName= storeName.substring(0,1).toUpperCase() + storeName.substring(1);

                    JSONObject openingHourJSON = storeObj.getJSONObject(AppConstants.OPENING_HOURS);

                    String monday = openingHourJSON.getString(AppConstants.MONDAY);
                    String tuesday = openingHourJSON.getString(AppConstants.TUESDAY);
                    String wednesday = openingHourJSON.getString(AppConstants.WEDNESDAY);
                    String thursday = openingHourJSON.getString(AppConstants.THURSDAY);
                    String friday = openingHourJSON.getString(AppConstants.FRIDAY);
                    String saturday = openingHourJSON.getString(AppConstants.SATURDAY);
                    String sunday = openingHourJSON.getString(AppConstants.SUNDAY);

                    JSONObject locationJSON = storeObj.getJSONObject(AppConstants.LOCATION);
                    String address = locationJSON.getString(AppConstants.ADDRESS);
                    String city = locationJSON.getString(AppConstants.CITY);
                    String state = locationJSON.getString(AppConstants.STATE);
                    String zip = locationJSON.getString(AppConstants.ZIP);
                    String country = locationJSON.getString(AppConstants.COUNTRY);
                    String latitude = locationJSON.getString(AppConstants.LATITUDE);
                    String longitude = locationJSON.getString(AppConstants.LONGITUDE);

                    OpeningHour openingHour = new OpeningHour(monday, tuesday, wednesday, thursday, friday, saturday, sunday);
                    Location location = new Location(address,city, state, zip, country, latitude,longitude);

                    top10StoresArrayList.add(new CashBackStore(storeId,firstName,lastName, storeName,description,email,phoneNumber, webUrl,isFavorite,profileImage, featuredImage, openingHour, location));
                }
                top10StoresArrayList.add(new CashBackStore("","","","","","","","","","","",null,null));
                JSONArray receiptsArray = Json1.getJSONArray(AppConstants.RECEIPTS_LIST);
                for(int i = 0; i<receiptsArray.length(); i++){
                    JSONObject receiptsObject = receiptsArray.getJSONObject(i);
                    String id = receiptsObject.getString(AppConstants.RECEIPT_ID);
                    String receiptType = receiptsObject.getString(AppConstants.RECEIPT_TYPE);
                    String storeName = receiptsObject.getString(AppConstants.STORE_NAME);
                    String purchasedDate = receiptsObject.getString(AppConstants.PURCHASED_DATE);
                    String subTotal = receiptsObject.getString(AppConstants.SUBTOTAL_AMOUNT);
                    String receiptImage = "";//receiptsObject.getString(AppConstants.RECEIPT_IMAGE);
                    String uploadedDate = receiptsObject.getString(AppConstants.UPLOADED_DATE);

                    if(storeName != null && !storeName.equals(""))
                        storeName= storeName.substring(0,1).toUpperCase() + storeName.substring(1);

                    SimpleDateFormat inputFormat = null;
                    Date date = null;
                    try{
                        inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        date = inputFormat.parse(purchasedDate);
                    }catch (Exception ex){
                        inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                        date = inputFormat.parse(purchasedDate);
                    }
                    SimpleDateFormat outputFormat = new SimpleDateFormat("MMMM");
                    String outputString = outputFormat.format(date) + " "+ String.valueOf(date.getYear()+1900);
                    receiptArrayList.add(new Receipt(id,receiptType,storeName,purchasedDate,subTotal,receiptImage,uploadedDate,outputString));
                }
                JSONArray storesArray = Json1.getJSONArray(AppConstants.ALl_STORES_LIST);
                for(int i = 0; i < storesArray.length(); i++){
                    JSONObject storeObj = storesArray.getJSONObject(i);
                    String storeId = storeObj.getString(AppConstants.STORE_ID);
                    String storeName = storeObj.getString(AppConstants.STORE_NAME);
                    String webUrl = storeObj.getString(AppConstants.WEB_URL);
                    String phoneNum = storeObj.getString(AppConstants.PHONE);
                    String profileImage = storeObj.getString(AppConstants.PROFILE_IMAGE);
                    String featuredImage = storeObj.getString(AppConstants.FEATURED_IMAGE);
                    String address = storeObj.getString(AppConstants.ADDRESS);
                    String city = storeObj.getString(AppConstants.CITY);
                    String state = storeObj.getString(AppConstants.STATE);
                    String zip = storeObj.getString(AppConstants.ZIP);
                    String latitude = storeObj.getString(AppConstants.LATITUDE);
                    String longitude = storeObj.getString(AppConstants.LONGITUDE);

                    if(storeName != null && !storeName.equals(""))
                        storeName= storeName.substring(0,1).toUpperCase() + storeName.substring(1);

                    fetchAllStoreArrayList.add(new Store(storeId,storeName,webUrl,phoneNum,profileImage,featuredImage,address,city,state,zip,latitude,longitude));
                }
                JSONObject paymentInfoObj = Json1.getJSONObject(AppConstants.PAYMENT_INFO);
                String cardHolderName = paymentInfoObj.getString(AppConstants.CARD_HOLDER_NAME);
                String cardType = paymentInfoObj.getString(AppConstants.CARD_TYPE);
                String cardNumber = paymentInfoObj.getString(AppConstants.CARD_NUMBER);
                String cvc = paymentInfoObj.getString(AppConstants.CVC);
                String expiryMonth = paymentInfoObj.getString(AppConstants.EXPIRY_MONTH);
                String expiryYear = paymentInfoObj.getString(AppConstants.EXPIRY_YEAR);
                if(cardHolderName != null && !cardHolderName.equals(""))
                    cardHolderName = cardHolderName.substring(0,1).toUpperCase() + cardHolderName.substring(1);
                paymentInfo = new PaymentInfo(cardHolderName,cardType,cardNumber,cvc,expiryMonth,expiryYear);
                delegate.callback(AppConstants.SUCCESS);
                isDataLoaded = true;
            } else {
                isDataLoaded = true;
                delegate.callback(AppConstants.FAILURE);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            isDataLoaded = true;
            delegate.callback(AppConstants.FAILURE);
        }

    }
    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }
    public ArrayList<CashBack> getCashBackArrayList() {
        return cashBackArrayList;
    }

    public ArrayList<CashBackStore> getTop10StoresArrayList() {
        return top10StoresArrayList;
    }

    public ArrayList<CashBackStore> getFavoriteStoresArrayList() {
        return favoriteStoresArrayList;
    }

    public ArrayList<Receipt> getReceiptArrayList() {
        return receiptArrayList;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public Long getMonthCashBack() {
        return monthCashBack;
    }

    public Long getYearCashBack() {
        return yearCashBack;
    }
    public boolean isDataLoaded(){
        return isDataLoaded;
    }

    public ArrayList<CashBackCategory> getCashBackCategoryArrayList() {
        return cashBackCategoryArrayList;
    }
    public ArrayList<CashBackStore> getStoreByCategoryId(String categoryId){
        for (CashBack cashBack: cashBackArrayList){
            if(cashBack.getId().equals(categoryId))
                return cashBack.getStoreArrayList();
        }
        return null;
    }

    public ArrayList<Store> getFetchAllStoreArrayList() {
        return fetchAllStoreArrayList;
    }

    public void addStoreToFavorite(CashBackStore store){
        favoriteStoresArrayList.add(store);
        updateArrays(store);
    }
    private void updateArrays(CashBackStore store){
        for (int i = 0; i<top10StoresArrayList.size();i++){
            CashBackStore store1 = top10StoresArrayList.get(i);
            if(store1.getStoreId().equals(store.getStoreId())){
                store1.setIsFavorite(store1.getIsFavorite());
            }
        }
        for (int i = 0; i<favoriteStoresArrayList.size();i++){
            CashBackStore store1 = favoriteStoresArrayList.get(i);
            if(store1.getStoreId().equals(store.getStoreId())){
                store1.setIsFavorite(store1.getIsFavorite());
            }
        }
    }
    public void removeStoreFromFavorite(CashBackStore store){
        int index = 0;
        for(CashBackStore store1:favoriteStoresArrayList){
            if(store1.getStoreId().equals(store.getStoreId())) {
                favoriteStoresArrayList.remove(index);
                break;
            }
            index++;
        }
        updateArrays(store);
    }

    public void clearAll(){
        if(cashBackArrayList != null)
            cashBackArrayList.clear();
        if(cashBackCategoryArrayList != null)
            cashBackCategoryArrayList.clear();
        if(fetchAllStoreArrayList != null)
            fetchAllStoreArrayList.clear();
        if(favoriteStoresArrayList != null)
            favoriteStoresArrayList.clear();
        if(top10StoresArrayList != null)
            top10StoresArrayList.clear();
        if(receiptArrayList != null)
            receiptArrayList.clear();
    }
}
