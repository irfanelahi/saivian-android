package saivian.txlabz.com.saivian.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONObject;

import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.StorageUtility;

/**
 * Created by Fatima Siddique on 5/5/2016.
 */
public class MarketResearchFragment extends Fragment implements View.OnClickListener{
    Context context;
    Spinner householdCount, householdIncome;
    EditText zip;
    RadioGroup rgResearchBrand;

    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_market_research, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        householdCount = (Spinner) view.findViewById(R.id.spinner_household_count);
        householdIncome = (Spinner) view.findViewById(R.id.spinner_household_income);
        zip = (EditText)view.findViewById(R.id.et_zip);
        rgResearchBrand = (RadioGroup) view.findViewById(R.id.rg_research_brand);
        Button submit = (Button)view.findViewById(R.id.btn_online_signature);
        submit.setOnClickListener(this);
        setSpinners();
    }
    private void setSpinners(){
        if(context == null)
            return;
        ArrayAdapter countAdapter = ArrayAdapter.createFromResource(context, R.array.houseHoldCount, R.layout.spinner_dropdown_item);
        countAdapter.setDropDownViewResource(R.layout.custom_spinner_item);

        householdCount.setAdapter(countAdapter);
        householdCount.setSelection(0, false);
        householdCount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        ArrayAdapter incomeAdapter = ArrayAdapter.createFromResource(context, R.array.houseHoldIncome, R.layout.spinner_dropdown_item);
        incomeAdapter.setDropDownViewResource(R.layout.custom_spinner_item);

        householdIncome.setAdapter(incomeAdapter);
        householdIncome.setSelection(0, false);
        householdIncome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private String getSelectedRadioButtonValue(){
        int id= rgResearchBrand.getCheckedRadioButtonId();
        View radioButton = rgResearchBrand.findViewById(id);
        int radioId = rgResearchBrand.indexOfChild(radioButton);
        RadioButton btn = (RadioButton) rgResearchBrand.getChildAt(radioId);
        String selection = (String) btn.getText();

        return selection;
    }
    private void submitSubmitMarketResearch(){
        GeneralUtil.showLoadingDialog(context);
        Ion.with(this)
                .load(AppConstants.POST,AppConstants.SUBMIT_MARKET_RESEARCH)
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .setBodyParameter(AppConstants.CUSTOMER_ID, StorageUtility.getLoginModel(context).getId())
                .setBodyParameter(AppConstants.ANSWER+"1", zip.getText().toString())
                .setBodyParameter(AppConstants.ANSWER+"2", householdCount.getSelectedItem().toString())
                .setBodyParameter(AppConstants.ANSWER+"3", householdIncome.getSelectedItem().toString())
                .setBodyParameter(AppConstants.ANSWER+"4", getSelectedRadioButtonValue())
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            zip.setText("");
                            householdIncome.setSelection(0);
                            householdCount.setSelection(0);
                            rgResearchBrand.clearCheck();
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_online_signature:
                if(isDataValid())
                   submitSubmitMarketResearch();
                break;
        }
    }

    private boolean isDataValid() {
        boolean result = true;
        if(zip.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter zip");
        }else if(rgResearchBrand.getCheckedRadioButtonId()==-1){
            result = false;
            GeneralUtil.showAlert(context,"Select name brand or store brand product");
        }
        return result;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();

        zip = null;
        rgResearchBrand = null;
        householdCount = null;
        householdIncome = null;
        context = null;
    }
    @Override
    public void onResume(){
        super.onResume();
        context = getActivity();
    }
//    @Override
//    public void onDestroy(){
//        super.onDestroy();
////        if(activityRootView != null) {
////            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
////                activityRootView.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
////            } else {
////                activityRootView.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
////            }
////            activityRootView = null;
////        }
////        activityRootView = null;
//        context = null;
//    }
}
