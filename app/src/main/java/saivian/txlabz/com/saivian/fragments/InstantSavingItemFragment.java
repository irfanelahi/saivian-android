package saivian.txlabz.com.saivian.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.adapters.InstantSavingItemAdapter;
import saivian.txlabz.com.saivian.models.InstantOffer;

/**
 * Created by Fatima Siddique on 5/4/2016.
 */
public class InstantSavingItemFragment extends Fragment {
    Context context;
    ArrayList<InstantOffer> offerArrayList;

    public static InstantSavingItemFragment getInstance(ArrayList<InstantOffer> arrayList){
        InstantSavingItemFragment fragment = new InstantSavingItemFragment();
        fragment.offerArrayList = arrayList;
        return  fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_block_local_item, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        InstantSavingItemAdapter adapter = new InstantSavingItemAdapter(context,offerArrayList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(linearLayoutManager);
    }


}
