package saivian.txlabz.com.saivian.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class ExecuteTask extends AsyncTask<Void, Void, Void>{
    private Context act;
    Delegate delegate;

    public ExecuteTask(Context act, Delegate delegate) {
        Log.i("check","In CancelEncounterTask");
        this.act = act;
        this.delegate = delegate;
    }
    @Override
    protected void onPreExecute() {

    }

    @Override
    protected Void doInBackground(Void... params) {

        Ion.with(act)
                .load(AppConstants.GET,AppConstants.GET_CASH_BACK_AMOUNT+"?customer_id="+ StorageUtility.getLoginModel(act).getId())
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();
                Log.i(AppConstants.TAG,"result is : " + result);
                if (e == null) {
                    try {
                        delegate.callback(result);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(act, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "message: " + e.getMessage());
                    GeneralUtil.handleException(act, e);
                }

            }
        });
        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        onPostExecute(v);
    }

}
