package saivian.txlabz.com.saivian;

import android.app.Application;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import io.fabric.sdk.android.Fabric;
import java.io.File;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ApplicationController extends Application {

	public static int SCREEN_WIDTH = 0;
	public static int SCREEN_HEIGHT = 0;


	@Override
	public void onCreate()
	{
		super.onCreate();
		Fabric.with(this, new Crashlytics());
		FacebookSdk.sdkInitialize(getApplicationContext());
		CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
						.setDefaultFontPath("fonts/OpenSans-Regular.ttf")
						.setFontAttrId(R.attr.fontPath)
						.build()
		);


		WindowManager windowManager = (WindowManager) getApplicationContext()
				.getSystemService(Context.WINDOW_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			Point size = new Point();
			windowManager.getDefaultDisplay().getSize(size);
			SCREEN_WIDTH = size.x;
			SCREEN_HEIGHT = size.y;
		} else {
			Display d = windowManager.getDefaultDisplay();
			SCREEN_WIDTH = d.getWidth();
			SCREEN_HEIGHT = d.getHeight();
		}

		File cacheDir = StorageUtils.getCacheDirectory(getApplicationContext());

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
				.memoryCacheExtraOptions(SCREEN_WIDTH, SCREEN_HEIGHT ) // default = device screen dimensions
				.diskCacheExtraOptions(SCREEN_WIDTH * 2, SCREEN_HEIGHT * 2, null)
				.threadPoolSize(3) // default
				.threadPriority(Thread.NORM_PRIORITY - 2) // default
				.tasksProcessingOrder(QueueProcessingType.FIFO) // default
				.denyCacheImageMultipleSizesInMemory()
				.memoryCache(new LruMemoryCache(2 * 1024 * 1024))
				.memoryCacheSize(2 * 1024 * 1024)
				.memoryCacheSizePercentage(13) // default
	//			.diskCache(new UnlimitedDiscCache(cacheDir)) // default
				.diskCacheSize(100 * 1024 * 1024)
				.diskCacheFileCount(1000)
				.diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
				.imageDownloader(new BaseImageDownloader(getApplicationContext())) // default
				.defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
				.writeDebugLogs()
				.build();
        ImageLoader.getInstance().init(config);
	}


	@Override
	public void onTerminate()
	{
		super.onTerminate();
	}

    @Override
    public void onLowMemory() {
        ImageLoader.getInstance().clearMemoryCache();
        super.onLowMemory();
    }
}
