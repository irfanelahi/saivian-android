package saivian.txlabz.com.saivian.models;

import java.io.Serializable;

/**
 * Created by Fatima Siddique on 5/17/2016.
 */
public class CashBackCategory implements Serializable{
    String id;
    String categoryName;
    String image;
    String isActive;

    public CashBackCategory(String id, String categoryName, String image, String isActive) {
        this.id = id;
        this.categoryName = categoryName;
        this.image = image;
        this.isActive = isActive;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}
