package saivian.txlabz.com.saivian.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.adapters.StoreArrayAdapter;
import saivian.txlabz.com.saivian.adapters.StoreNameSpinnerAdapter;
import saivian.txlabz.com.saivian.models.Receipt;
import saivian.txlabz.com.saivian.models.Store;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;

/**
 * Created by Fatima Siddique on 5/9/2016.
 */
public class UploadTravelFragment extends Fragment implements View.OnClickListener{
    ArrayList <Store> storeListItems;
    Context context;
    ImageView imageToUpload;
    //    Spinner nameOfStores;
    private int year = 0;
    private int month = 0;
    private int day = 0;
    TextView purchasedDate;
    EditText subTotalAmount;
    String pathFile = null;
    EditText et3rdParty;
    String storeId;
//    View activityRootView;
    boolean isFragmentPaused = false;
    AutoCompleteTextView inputEditText;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        isFragmentPaused = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_upload_travel, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        Bundle bundle = getArguments();
        if(bundle != null){
            pathFile = bundle.getString(AppConstants.RECEIPT_IMAGE);
        }
//        activityRootView = ((MainActivity)context).getRootView();
//        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(listener);

        et3rdParty = (EditText)view.findViewById(R.id.et_3rd_party);
        subTotalAmount = (EditText)view.findViewById(R.id.et_subtotal_amount);
        purchasedDate = (TextView)view.findViewById(R.id.spinner_purchased_date);
//        nameOfStores = (Spinner)view.findViewById(R.id.spinner_name_of_store);
        imageToUpload = (ImageView)view.findViewById(R.id.img_for_upload);
        View purchasedDateContainer = view.findViewById(R.id.departure_container);

        MainStorageUtility utility = MainStorageUtility.getInstance(context);
        storeListItems = utility.getFetchAllStoreArrayList();

        inputEditText = (AutoCompleteTextView) view.findViewById(R.id.spinner_name_of_store);
        if(context == null)
            return;
        if(storeListItems != null) {
            StoreArrayAdapter mDepartmentArrayAdapter = new StoreArrayAdapter(context, R.layout.simple_text_view, storeListItems);
            inputEditText.setAdapter(mDepartmentArrayAdapter);
        }
        purchasedDateContainer.setOnClickListener(this);
        inputEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Object item = adapterView.getItemAtPosition(i);
                if (item instanceof Store){
                    Store student=(Store) item;
                    storeId = student.getStoreId();
                }else{
                    storeId = null;
                }

            }
        });

        inputEditText.setThreshold(1);
        Button uploadBtn = (Button)view.findViewById(R.id.btn_upload);
        uploadBtn.setOnClickListener(this);

//

        ImageView retake = (ImageView)view.findViewById(R.id.img_retake);
        ImageView upload = (ImageView)view.findViewById(R.id.img_upload);
        retake.setOnClickListener(this);
//        retake.setEnabled(false);
        upload.setSelected(true);
        loadImageInBG();

//        createStoreNameSpinner();
//        fetchStores();
    }
    public void loadImageInBG() {
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(pathFile, options);
                ByteArrayOutputStream out = new ByteArrayOutputStream();

                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

                Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

                return  decoded;
            }

            @Override
            protected void onPostExecute(Bitmap msg) {
                super.onPostExecute(msg);
                imageToUpload.setImageBitmap(msg);
            }

        }.execute(null, null, null);

    }
    private void createDOBDialog() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd = new DatePickerDialog(context, myDateSetListener, year, month, day);
//        dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpd.getDatePicker().setMaxDate(new Date().getTime());
        dpd.show();
    }
    DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker datePicker, int i, int j, int k) {

            year = i;
            month = j + 1;
            day = k;
            purchasedDate.setText(String.format("%02d",year)+"-"+String.format("%02d",month)+"-"+String.format("%02d",day));
        }
    };
    @Override
    public void onResume(){
        super.onResume();
        if(context != null)
            ((MainActivity)context).hideBottomBar();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.departure_container:
                createDOBDialog();
                break;
            case R.id.btn_upload:
                if(isDataValid()){
                    uploadReceipt();
                }
                break;

            case R.id.img_retake:
                ((MainActivity)context).loadFragment(new CameraFragment());
                break;

            case R.id.btn_cancel:
                break;
        }
    }

    private boolean isDataValid() {
        boolean result = true;
        if(storeId == null || (storeId != null && storeId.equals("")) || !checkValidStoreName()){
            result = false;
            GeneralUtil.showAlert(context,"Enter Valid Store Name");
        } else if(purchasedDate.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Select Purchased Date");
        }else if(subTotalAmount.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter subtotal amount");
        }else if(subTotalAmount.getText().toString().equals("0")){
            result = false;
            GeneralUtil.showAlert(context,"Subtotal amount must be greater than 0");
        }
        return result;
    }
    private boolean checkValidStoreName(){
        String input = inputEditText.getText().toString();
        if(input != null && !input.equals("")) {
            for (int i = 0; i < storeListItems.size(); i++) {
                Store store = storeListItems.get(i);
                if (input.equalsIgnoreCase(store.getStoreName())) {
                    storeId = store.getStoreId();
                    return true;
                }
            }
        }
        return false;
    }
//    private void createStoreNameSpinner(){
//        final StoreNameSpinnerAdapter adapter = new StoreNameSpinnerAdapter(context, storeListItems);
//
//        nameOfStores.setAdapter(adapter);
//        nameOfStores.setSelection(0, false);
//        nameOfStores.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//                String storeId = adapter.getItem(arg2).getStoreId();
//                Log.i(AppConstants.TAG,"Store id is : " + storeId);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//            }
//        });
//    }

    private void fetchStores(){
        if(storeListItems != null)
            storeListItems.clear();
        storeListItems = new ArrayList<>();
        GeneralUtil.showLoadingDialog(context);
        Ion.with(this)
                .load(AppConstants.GET,AppConstants.FETCH_ALL_STORES)
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            JSONArray storesArray = Json1.getJSONArray(AppConstants.STORES_LIST);
                            for(int i = 0; i < storesArray.length(); i++){
                                JSONObject storeObj = storesArray.getJSONObject(i);
                                String storeId = storeObj.getString(AppConstants.STORE_ID);
                                String storeName = storeObj.getString(AppConstants.STORE_NAME);
                                String webUrl = storeObj.getString(AppConstants.WEB_URL);
                                String phoneNum = storeObj.getString(AppConstants.PHONE);
                                String profileImage = storeObj.getString(AppConstants.PROFILE_IMAGE);
                                String featuredImage = storeObj.getString(AppConstants.FEATURED_IMAGE);
                                String address = storeObj.getString(AppConstants.ADDRESS);
                                String city = storeObj.getString(AppConstants.CITY);
                                String state = storeObj.getString(AppConstants.STATE);
                                String zip = storeObj.getString(AppConstants.ZIP);
                                String latitude = storeObj.getString(AppConstants.LATITUDE);
                                String longitude = storeObj.getString(AppConstants.LONGITUDE);


                                storeListItems.add(new Store(storeId,storeName,webUrl,phoneNum,profileImage,featuredImage,address,city,state,zip,latitude,longitude));
                            }
//                            createStoreNameSpinner();
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }

    public void uploadReceipt() {
        GeneralUtil.showLoadingDialog(context);
//        int selectedPos = 0;//nameOfStores.getSelectedItemPosition();
//        String storeID = storeListItems.get(selectedPos).getStoreId();
        Ion.with(this)
                .load(AppConstants.POST, AppConstants.UPLOAD_RECEIPT)
                .setHeader(AppConstants.HEADER_KEY, AppConstants.HEADER_VALUE)
                .setMultipartParameter(AppConstants.CUSTOMER_ID, StorageUtility.getLoginModel(context).getId())
                .setMultipartParameter(AppConstants.RECEIPT_TYPE, AppConstants.TRAVEL)
                .setMultipartParameter(AppConstants.STORE_ID, storeId)
                .setMultipartParameter(AppConstants.THIRD_PARTY,et3rdParty.getText().toString())
                .setMultipartParameter(AppConstants.SUBTOTAL_AMOUNT, subTotalAmount.getText().toString())
                .setMultipartParameter(AppConstants.PURCHASED_DATE, purchasedDate.getText().toString())
                .setMultipartFile(AppConstants.RECEIPT_IMAGE, new File(pathFile))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                            JSONArray receiptsArray = Json1.getJSONArray(AppConstants.RECEIPT);
                            JSONObject receiptsObject = receiptsArray.getJSONObject(0);
                            String id = receiptsObject.getString(AppConstants.RECEIPT_ID);
                            String receiptType = receiptsObject.getString(AppConstants.RECEIPT_TYPE);
                            String storeName = receiptsObject.getString(AppConstants.STORE_NAME);
                            String purchasedDate = receiptsObject.getString(AppConstants.PURCHASED_DATE);
                            String subTotal = receiptsObject.getString(AppConstants.SUBTOTAL_AMOUNT);
                            String receiptImage = "";//receiptsObject.getString(AppConstants.RECEIPT_IMAGE);
                            String uploadedDate = receiptsObject.getString(AppConstants.UPLOADED_DATE);
                            //2016-05-09 03:38:29
                            SimpleDateFormat inputFormat = null;
                            Date date = null;
                            try{
                                inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                date = inputFormat.parse(purchasedDate);
                            }catch (Exception ex){
                                inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                                date = inputFormat.parse(purchasedDate);
                            }
                            SimpleDateFormat outputFormat = new SimpleDateFormat("MMMM");
                            String outputString = outputFormat.format(date) + " "+ String.valueOf(date.getYear()+1900);
                            MainStorageUtility utility = MainStorageUtility.getInstance(context);
                            ArrayList<Receipt>receiptArrayList = utility.getReceiptArrayList();
                            receiptArrayList.add(new Receipt(id,receiptType,storeName,purchasedDate,subTotal,receiptImage,uploadedDate,outputString));

                            ((MainActivity)context).loadFragment(new MainFragment());
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }
//    ViewTreeObserver.OnGlobalLayoutListener listener = new ViewTreeObserver.OnGlobalLayoutListener() {
//        @Override
//        public void onGlobalLayout() {
//            if(isFragmentPaused)
//                return;
//            Rect r = new Rect();
//            if(activityRootView != null)
//                activityRootView.getWindowVisibleDisplayFrame(r);
//            else{
//                activityRootView = ((MainActivity)context).getRootView();
//            }
//            int screenHeight = activityRootView.getRootView().getHeight();
//
//            int keypadHeight = screenHeight - r.bottom;
//
//            if (keypadHeight > screenHeight * 0.15) {
//                ((MainActivity)context).hideBottomBar();
//            }
//            else {
//                ((MainActivity)context).showBottomBar();
//            }
//        }
//    };
    @Override
    public void onDestroyView(){
        super.onDestroyView();
        isFragmentPaused = true;
//        if(activityRootView != null) {
//            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
//                activityRootView.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
//            } else {
//                activityRootView.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
//            }
//            activityRootView = null;
//        }
//        activityRootView = null;
//        listener = null;
        context = null;
    }
}
