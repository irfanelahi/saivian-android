package saivian.txlabz.com.saivian.models;

import saivian.txlabz.com.saivian.utils.AppConstants;

/**
 * Created by Fatima Siddique on 5/10/2016.
 */
public class CashBackLocalItem {
    int resource;
    AppConstants.CASH_BACK_STATE state;

    public CashBackLocalItem(int resource) {
        this.resource = resource;
        this.state = AppConstants.CASH_BACK_STATE.NORMAL;
    }

    public int getResource() {
        return resource;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public AppConstants.CASH_BACK_STATE getState() {
        return state;
    }

    public void setState(AppConstants.CASH_BACK_STATE state) {
        this.state = state;
    }
}
