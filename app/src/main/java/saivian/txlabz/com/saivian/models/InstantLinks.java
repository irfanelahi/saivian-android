package saivian.txlabz.com.saivian.models;

/**
 * Created by Fatima Siddique on 6/17/2016.
 */
public class InstantLinks {

    String showOffer;
    String showStore;
    String showLocation;
    String findOffersAtThisStore;
    String findOffersAtThisLocation;
    String findThisOfferAtMoreLocations;
    String findLocationsWithThisOffer;
    String findLocationsForThisStore;

    public InstantLinks(String showOffer, String showStore, String showLocation, String findOffersAtThisStore, String findOffersAtThisLocation, String findThisOfferAtMoreLocations, String findLocationsWithThisOffer, String findLocationsForThisStore) {
        this.showOffer = showOffer;
        this.showStore = showStore;
        this.showLocation = showLocation;
        this.findOffersAtThisStore = findOffersAtThisStore;
        this.findOffersAtThisLocation = findOffersAtThisLocation;
        this.findThisOfferAtMoreLocations = findThisOfferAtMoreLocations;
        this.findLocationsWithThisOffer = findLocationsWithThisOffer;
        this.findLocationsForThisStore = findLocationsForThisStore;
    }

    public String getShowOffer() {
        return showOffer;
    }

    public void setShowOffer(String showOffer) {
        this.showOffer = showOffer;
    }

    public String getShowStore() {
        return showStore;
    }

    public void setShowStore(String showStore) {
        this.showStore = showStore;
    }

    public String getShowLocation() {
        return showLocation;
    }

    public void setShowLocation(String showLocation) {
        this.showLocation = showLocation;
    }

    public String getFindOffersAtThisStore() {
        return findOffersAtThisStore;
    }

    public void setFindOffersAtThisStore(String findOffersAtThisStore) {
        this.findOffersAtThisStore = findOffersAtThisStore;
    }

    public String getFindOffersAtThisLocation() {
        return findOffersAtThisLocation;
    }

    public void setFindOffersAtThisLocation(String findOffersAtThisLocation) {
        this.findOffersAtThisLocation = findOffersAtThisLocation;
    }

    public String getFindThisOfferAtMoreLocations() {
        return findThisOfferAtMoreLocations;
    }

    public void setFindThisOfferAtMoreLocations(String findThisOfferAtMoreLocations) {
        this.findThisOfferAtMoreLocations = findThisOfferAtMoreLocations;
    }

    public String getFindLocationsWithThisOffer() {
        return findLocationsWithThisOffer;
    }

    public void setFindLocationsWithThisOffer(String findLocationsWithThisOffer) {
        this.findLocationsWithThisOffer = findLocationsWithThisOffer;
    }

    public String getFindLocationsForThisStore() {
        return findLocationsForThisStore;
    }

    public void setFindLocationsForThisStore(String findLocationsForThisStore) {
        this.findLocationsForThisStore = findLocationsForThisStore;
    }
}
