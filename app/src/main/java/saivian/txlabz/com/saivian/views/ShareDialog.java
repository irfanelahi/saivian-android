package saivian.txlabz.com.saivian.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Telephony;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;

import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.StorageUtility;


public class ShareDialog extends Activity implements View.OnClickListener{

	Context context;
    CallbackManager callbackManager;
    com.facebook.share.widget.ShareDialog shareDialog;
    View shareContainer;
    String urlToShare = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        setContentView(R.layout.layout_share);
        context = ShareDialog.this;
        urlToShare = StorageUtility.getDataFromPreferences(AppConstants.SHARE_URL,"",context);
        if(urlToShare.isEmpty())
            urlToShare = "https://www.saivian.net";


        callbackManager = CallbackManager.Factory.create();
        shareDialog = new com.facebook.share.widget.ShareDialog(this);
        View emptyView1 = findViewById(R.id.emptyView1);
        View emptyView2 = findViewById(R.id.emptyView2);

        View facebook = findViewById(R.id.img_fb);
        View twitter  = findViewById(R.id.img_twitter);
        View instagram = findViewById(R.id.img_instagram);
        View mail = findViewById(R.id.img_mail);
        View sms = findViewById(R.id.img_sms);

        shareContainer = findViewById(R.id.shareContainer);
        final Animation zoomOut = AnimationUtils.loadAnimation(context, R.anim.scale_zoom_out);
        facebook.setOnClickListener(this);
        twitter.setOnClickListener(this);
        instagram.setOnClickListener(this);
        mail.setOnClickListener(this);
        sms.setOnClickListener(this);
        shareContainer.startAnimation(zoomOut);

        emptyView1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return exitAnim();
            }
        });
        emptyView2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return exitAnim();
            }
        });
    }
    private boolean exitAnim(){
        shareContainer.clearAnimation();
        final Animation zoomIn = AnimationUtils.loadAnimation(context, R.anim.scale_zoom_in);
        shareContainer.startAnimation(zoomIn);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        },700);
        return true;
    }
    private void showFBShareDialog(){

        if (com.facebook.share.widget.ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("Saivian")
                    .setContentDescription("Shop. Save. Share. @ Saivian: "+urlToShare)
                    .setContentUrl(Uri.parse("http://saivian.net/"))
                    .build();

            shareDialog.show(linkContent);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_fb:
                showFBShareDialog();
                break;
            case R.id.img_twitter:
                shareOnTwitter();
                break;
            case R.id.img_instagram:
                shareOnInstagram();
                break;
            case R.id.img_mail:
                shareOnEmail();
                break;
            case R.id.img_sms:
                shareOnContacts();
                break;
        }
    }
    public void shareOnTwitter()
    {

        String text = "Shop. Save. Share. @ Saivian: "+urlToShare ;

        try {
            String url = "twitter://post?message=";

            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url + Uri.encode(text)));
            startActivity(i);
        } catch (android.content.ActivityNotFoundException e) {
//            String url = "http://mobile.twitter.com/compose/tweet?status=";
            String url = "https://twitter.com/intent/tweet?text=";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url + Uri.encode(text)));
            startActivity(i);
        }
    }
    public void shareOnEmail()
    {
        try{
            String URI="mailto:?subject=" + StorageUtility.getLoginModel(this).getFirstName() +" has Invited to Saivian"
                    + "&body=" + "Shop. Save. Share. @ Saivian: "+urlToShare;
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setType("message/rfc822");
            Uri data = Uri.parse(URI);
            intent.setData(data);
            context.startActivity(intent);
        }
        catch(Exception e){
            Toast.makeText(context, "No account is setup", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    public void shareOnInstagram(){
        String text = "Hey, join the revolutionary app to Shop, Save and Share! " + urlToShare;
        try
        {
            try
            {
                Uri file = Uri.parse("android.resource://saivian.txlabz.com.saivian/"+R.drawable.saivian_logo);
                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM,file);
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Shop. Save. Share. @ Saivian: "+urlToShare);
                shareIntent.setPackage("com.instagram.android");
                startActivity(shareIntent);
            }
            catch(Exception e) {
                Toast.makeText(this,"You need to download the Instagram app to share Saivian.",Toast.LENGTH_SHORT).show();
            }

        }
        catch(Exception e) {
            Toast.makeText(this,"You need to download the Instagram app to share Saivian.",Toast.LENGTH_SHORT).show();
        }
    }
    private void shareOnContacts(){
        String text = "Shop. Save. Share. @ Saivian: "+urlToShare;
        try{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
            {
                String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this); // Need to change the build to API 19

                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, text);

                if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
                // any app that support this intent.
                {
                    sendIntent.setPackage(defaultSmsPackageName);
                }
                startActivity(sendIntent);

            }
            else {
                Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("sms_body",text);
                startActivity(smsIntent);
            }
        }
        catch(Exception e){
            e.printStackTrace();
            Toast.makeText(this, "Application not available",Toast.LENGTH_SHORT).show();
        }
    }
}
