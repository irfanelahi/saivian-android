package saivian.txlabz.com.saivian.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.models.UserDetail;
import saivian.txlabz.com.saivian.utils.StorageUtility;


/**
 * Created by irfanelahi on 13/03/2016.
 */
public class UserProfileFragment extends Fragment implements View.OnClickListener{
    Context context;
    ImageView profileImage;
    TextView firstNameValue, lastNameValue, saivianIdValue, passwordValue,profilePicValue, emailValue, phoneValue;
    EditText etFirstNameValue, etLastNameValue, etSaivianIdValue, etPasswordValue,etProfilePicValue, etEmailValue, etPhoneValue;
    Dialog dialog;
    ImageLoader imageLoader;
    View activityRootView;
    DisplayImageOptions options;
    File imageFile;
    String firstNameStr, lastNameStr, phoneStr;
    boolean isFragmentPaused = false;
    Button editButton;
    ScrollView scrollView;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        isFragmentPaused = false;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        sw = dm.widthPixels;
        sh = dm.heightPixels;
        Log.i(AppConstants.TAG,"Screen Width = " + sw);
        Log.i(AppConstants.TAG,"Screen Height = " + sh);
        scrollView = (ScrollView)view.findViewById(R.id.scrollView);
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.avatar_placeholder)
                .showImageForEmptyUri(R.drawable.avatar_placeholder)
                .showImageOnFail(R.drawable.avatar_placeholder).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();
        profileImage = (ImageView)view.findViewById(R.id.userImage);
        imageLoader.displayImage(StorageUtility.getLoginModel(context).getProfileImage(),profileImage,options);

        firstNameValue = (TextView)view.findViewById(R.id.firstNameValue);
        lastNameValue = (TextView)view.findViewById(R.id.lastNameValue);
        saivianIdValue = (TextView)view.findViewById(R.id.saiviaIdValue);
        passwordValue = (TextView)view.findViewById(R.id.passwordValue);
        emailValue = (TextView)view.findViewById(R.id.emailValue);
        phoneValue = (TextView)view.findViewById(R.id.phoneValue);


        etFirstNameValue = (EditText) view.findViewById(R.id.firstNameEditText);
        etLastNameValue = (EditText) view.findViewById(R.id.lastNameEditText);
        etSaivianIdValue = (EditText) view.findViewById(R.id.saivianIDEditText);
        etPasswordValue = (EditText) view.findViewById(R.id.passwordEditText);
        etEmailValue = (EditText) view.findViewById(R.id.emailEditText);
        etPhoneValue = (EditText) view.findViewById(R.id.phoneEditText);
        editButton = (Button)view.findViewById(R.id.editButton);
        TextView whatsThis = (TextView)view.findViewById(R.id.whatsThis);
        TextView shareWebsite = (TextView)view.findViewById(R.id.shareWebsite);
        editButton.setOnClickListener(this);
        whatsThis.setOnClickListener(this);
        shareWebsite.setOnClickListener(this);
        profileImage.setOnClickListener(this);
//        getUserProfileFromServer();

        UserDetail userDetail = StorageUtility.getLoginModel(context);
        firstNameValue.setText(userDetail.getFirstName());
        lastNameValue.setText(userDetail.getLastName());
        emailValue.setText(userDetail.getEmail());
        saivianIdValue.setText(userDetail.getSaivianId());
        phoneValue.setText(userDetail.getPhone());

        etFirstNameValue.setText(userDetail.getFirstName());
        etLastNameValue.setText(userDetail.getLastName());
        etPhoneValue.setText(userDetail.getPhone());

        firstNameStr = userDetail.getFirstName();
        lastNameStr = userDetail.getLastName();
        phoneStr = userDetail.getPhone();

        ((MainActivity)context).changeUserImage(userDetail.getProfileImage());


        activityRootView = ((MainActivity)context).getRootView();
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(listener);
        onFocusListeners();

        Bundle bundle = getArguments();
        if(bundle != null){
            boolean canEdit = bundle.getBoolean(AppConstants.CAN_EDIT);
            if(!canEdit){
                editButton.setVisibility(View.GONE);
            }
        }
    }
    ViewTreeObserver.OnGlobalLayoutListener listener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            if(isFragmentPaused)
                return;
            Rect r = new Rect();
            if(activityRootView == null)
                return;
            activityRootView.getWindowVisibleDisplayFrame(r);
            int screenHeight = activityRootView.getRootView().getHeight();

            int keypadHeight = screenHeight - r.bottom;

            Log.d(AppConstants.TAG, "keypadHeight = " + keypadHeight);
            if (keypadHeight > screenHeight * 0.15) {
                ((MainActivity)context).hideBottomBar();
            }
            else {
                ((MainActivity)context).showBottomBar();
            }
        }
    };
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editButton:
                if(firstNameValue.getVisibility() == View.VISIBLE && lastNameValue.getVisibility() == View.VISIBLE) {
                    firstNameValue.setVisibility(View.GONE);
                    etFirstNameValue.setVisibility(View.VISIBLE);
                    lastNameValue.setVisibility(View.GONE);
                    etLastNameValue.setVisibility(View.VISIBLE);
                    phoneValue.setVisibility(View.GONE);
                    etPhoneValue.setVisibility(View.VISIBLE);
                    editButton.setText("SAVE");
                }else{
                    sendUpdatedProfileFromServer();
                }
                break;
            case R.id.shareWebsite:
                showShareWebsite();
                break;
            case R.id.whatsThis:
                showWhatsThisDialog();
                break;
            case R.id.userImage:
//                getReadStoragePermission();
                break;
        }
    }
    int sw, sh, left, bottom;

    private void onFocusListeners(){
        etFirstNameValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scroll();
            }
        });
        etLastNameValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scroll();
            }
        });
//        etPhoneValue.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                scroll();
//            }
//        });
        etPhoneValue.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    final Handler handler;
                    handler = new Handler();

                    final Runnable r = new Runnable() {
                        public void run() {
                            scrollView.smoothScrollTo(0, 500);

                        }
                    };
                    handler.postDelayed(r, 200);
                }
            }
        });
    }
    private void scroll(){
        final Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                scrollView.smoothScrollTo(0, 500);

            }
        };
        handler.postDelayed(r, 200);
    }
    private void getReadStoragePermission(){
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    AppConstants.GET_READ_PERMISSION_RC);

        }else{
            GeneralUtil.startGalleryByIntent(context);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppConstants.GET_READ_PERMISSION_RC: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    GeneralUtil.startGalleryByIntent(context);
                } else {

                }
                return;
            }
        }
    }
    private void getUserProfileFromServer(){
        GeneralUtil.showLoadingDialog(context);
        Ion.with(this)
                .load(AppConstants.GET,AppConstants.CUSTOMER_PROFILE_URL+"?customer_id="+StorageUtility.getLoginModel(context).getId())
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            parseJson(result);
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }

    private void sendUpdatedProfileFromServer(){
        if(!etFirstNameValue.getText().toString().equals(""))
            firstNameStr = etFirstNameValue.getText().toString();
        if(!etLastNameValue.getText().toString().equals(""))
            lastNameStr = etLastNameValue.getText().toString();
        if(!etPhoneValue.getText().toString().equals(""))
            phoneStr = etPhoneValue.getText().toString();

        GeneralUtil.showLoadingDialog(context);
        if(imageFile != null) {
            Ion.with(this)
                    .load(AppConstants.POST, AppConstants.EDIT_PROFILE)
                    .setHeader(AppConstants.HEADER_KEY, AppConstants.HEADER_VALUE)
                    .setMultipartFile(AppConstants.PROFILE_IMAGE, imageFile)
                    .setMultipartParameter(AppConstants.CUSTOMER_ID, StorageUtility.getLoginModel(context).getId())
                    .setMultipartParameter(AppConstants.PHONE, phoneStr)
                    .setMultipartParameter(AppConstants.FIRST_NAME, firstNameStr)
                    .setMultipartParameter(AppConstants.LAST_NAME, lastNameStr)
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    GeneralUtil.dismissLoadingDialog();

                    if (e == null) {
                        try {
                            parseJson(result);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            GeneralUtil.handleException(context, ex);
                        }

                    } else {
                        Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                        GeneralUtil.handleException(context, e);
                    }

                }
            });
        }else{
            Ion.with(this)
                    .load(AppConstants.POST, AppConstants.EDIT_PROFILE)
                    .setHeader(AppConstants.HEADER_KEY, AppConstants.HEADER_VALUE)
                    .setBodyParameter(AppConstants.CUSTOMER_ID, StorageUtility.getLoginModel(context).getId())
                    .setBodyParameter(AppConstants.PHONE, phoneStr)
                    .setBodyParameter(AppConstants.FIRST_NAME, firstNameStr)
                    .setBodyParameter(AppConstants.LAST_NAME, lastNameStr)
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    GeneralUtil.dismissLoadingDialog();

                    if (e == null) {
                        try {
                            parseJson(result);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            GeneralUtil.handleException(context, ex);
                        }

                    } else {
                        Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                        GeneralUtil.handleException(context, e);
                    }

                }
            });
        }
    }
    private void parseJson(String result) throws JSONException {
        JSONObject Json1 = new JSONObject(result);
        int status = Json1.getInt(AppConstants.STATUS);
        if (status == AppConstants.STATUS_SUCCESS) {
            JSONObject customerJObj = Json1.getJSONObject(AppConstants.CUSTOMER_DATA);
            String id = customerJObj.getString(AppConstants.ID);
            String referralId = customerJObj.getString(AppConstants.REFERRAL_ID);
            String firstName = customerJObj.getString(AppConstants.FIRST_NAME);
            String lastName = customerJObj.getString(AppConstants.LAST_NAME);
            String email = customerJObj.getString(AppConstants.EMAIL);
            String password = customerJObj.getString(AppConstants.PASSWORD);
            String profileImage = customerJObj.getString(AppConstants.PROFILE_IMAGE);
            String gender = customerJObj.getString(AppConstants.GENDER);
            String dob = customerJObj.getString(AppConstants.DOB);
            String city = customerJObj.getString(AppConstants.CITY);
            String state = customerJObj.getString(AppConstants.STATE);
            String zip = customerJObj.getString(AppConstants.ZIP);
            String country = customerJObj.getString(AppConstants.COUNTRY);
            String isVerified = customerJObj.getString(AppConstants.IS_VERIFIED);
            String isActive = customerJObj.getString(AppConstants.IS_ACTIVE);
            String createdDate = customerJObj.getString(AppConstants.CREATED_DATE);
            String phone = customerJObj.getString(AppConstants.PHONE);
            String saivianId = customerJObj.getString(AppConstants.SAIVIAN_ID);

            UserDetail userDetail = new UserDetail(id, referralId, firstName, lastName, email, password, profileImage, gender, dob, city, state, zip, country, isVerified, isActive, createdDate,phone, saivianId);
            StorageUtility.saveLoginObject(userDetail, context);

            firstNameValue.setText(firstName);
            lastNameValue.setText(lastName);
            emailValue.setText(email);
            saivianIdValue.setText(id);
            phoneValue.setText(phone);

            etFirstNameValue.setText(firstName);
            etLastNameValue.setText(lastName);
            etPhoneValue.setText(phone);

            firstNameStr = firstName;
            lastNameStr = lastName;
            phoneStr = phone;

            ((MainActivity)context).changeUserImage(profileImage);
        }else{
            GeneralUtil.showAlert(context,Json1.getString(AppConstants.MESSAGE));
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(data == null)
            return;
        if (requestCode == AppConstants.REQUEST_GALLERY)
        {
            Uri selectedImage = data.getData();
            String picturePath = GeneralUtil.getPath(context,selectedImage);
            profileImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            if(picturePath != null)
                imageFile = new File(picturePath);
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        isFragmentPaused = true;
        context = null;
    }
    private void showWhatsThisDialog(){

        if(dialog != null)
            dialog.dismiss();
        dialog = new Dialog(context, R.style.CustomTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setContentView(R.layout.dialog_whats_this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        View dialogContainer = dialog.findViewById(R.id.dialogContainer);
        dialogContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                dialog.dismiss();
                return true;
            }
        });
        dialog.show();

    }
    private void showShareWebsite(){
        if(dialog != null)
            dialog.dismiss();
        dialog = new Dialog(context, R.style.CustomTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_share_website);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        final EditText etUrl = (EditText) dialog.findViewById(R.id.etUrl);
        String url = StorageUtility.getDataFromPreferences(AppConstants.SHARE_URL,"",context);
        if(url != null && !url.isEmpty())
            etUrl.setText(url);

        View dialogContainer = dialog.findViewById(R.id.dialogContainer);
        dialogContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(!etUrl.getText().toString().equals("") && !GeneralUtil.isValidURL(etUrl.getText().toString())){
                    GeneralUtil.hideKeyboard((Activity) context);
                    GeneralUtil.showAlert(context, getResources().getString(R.string.enter_valid_url));
                    return false;
                }
                else if(!etUrl.getText().toString().equals("") && GeneralUtil.isValidURL(etUrl.getText().toString()))
                    StorageUtility.saveDataInPreferences(AppConstants.SHARE_URL,etUrl.getText().toString(),context);

                dialog.dismiss();
                return true;
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if(!etUrl.getText().toString().equals("") && !GeneralUtil.isValidURL(etUrl.getText().toString())){
                    GeneralUtil.hideKeyboard((Activity) context);
                    GeneralUtil.showAlert(context, getResources().getString(R.string.enter_valid_url));
                    return;
                }
                else if(!etUrl.getText().toString().equals("") && GeneralUtil.isValidURL(etUrl.getText().toString()))
                    StorageUtility.saveDataInPreferences(AppConstants.SHARE_URL,etUrl.getText().toString(),context);

                dialog.dismiss();
            }
        });
        dialog.show();

    }
    @Override
    public void onStop() {
        super.onStop();
        isFragmentPaused = true;
    }
    @Override
    public void onResume() {
        super.onResume();
        isFragmentPaused = false;
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        isFragmentPaused = true;
        if(activityRootView != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                activityRootView.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
            } else {
                activityRootView.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
            }
            activityRootView = null;
        }
        dialog = null;
        activityRootView = null;
        context = null;
    }
}
