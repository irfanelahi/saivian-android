package saivian.txlabz.com.saivian.models;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 6/17/2016.
 */
public class InstantCategory {
    String categoryName;
    String categoryKey;
    String categoryParentKey;
    String categoryParentName;
    String categoryType;
    ArrayList<InstantOffer> offerArrayList;

    public InstantCategory(String categoryName, String categoryKey, String categoryParentKey, String categoryParentName, String categoryType,ArrayList<InstantOffer>offers) {
        this.categoryName = categoryName;
        this.categoryKey = categoryKey;
        this.categoryParentKey = categoryParentKey;
        this.categoryParentName = categoryParentName;
        this.categoryType = categoryType;
        this.offerArrayList = offers;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getCategoryParentKey() {
        return categoryParentKey;
    }

    public void setCategoryParentKey(String categoryParentKey) {
        this.categoryParentKey = categoryParentKey;
    }

    public String getCategoryParentName() {
        return categoryParentName;
    }

    public void setCategoryParentName(String categoryParentName) {
        this.categoryParentName = categoryParentName;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public ArrayList<InstantOffer> getOfferArrayList() {
        return offerArrayList;
    }

    public void setOfferArrayList(ArrayList<InstantOffer> offerArrayList) {
        this.offerArrayList = offerArrayList;
    }
}
