package saivian.txlabz.com.saivian.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.adapters.Top10StoreItemAdapter;
import saivian.txlabz.com.saivian.models.CashBackStore;
import saivian.txlabz.com.saivian.models.Location;
import saivian.txlabz.com.saivian.models.OpeningHour;
import saivian.txlabz.com.saivian.models.Store;
import saivian.txlabz.com.saivian.models.StoreListItem;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;

/**
 * Created by Fatima Siddique on 5/5/2016.
 */
public class Top10StoresFragment extends Fragment implements View.OnClickListener{
    Context context;
    ArrayList <Store> storeListItems;
    ArrayList<CashBackStore> top10StoreArrayList;
    RecyclerView recyclerView;
    Top10StoreItemAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_top_10_stores, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);
        Button addStore = (Button)view.findViewById(R.id.btn_add_store);
        addStore.setOnClickListener(this);
        MainStorageUtility utility = MainStorageUtility.getInstance(context);
        storeListItems = utility.getFetchAllStoreArrayList();
        top10StoreArrayList = utility.getTop10StoresArrayList();
        adapter = new Top10StoreItemAdapter(context,top10StoreArrayList,storeListItems, Top10StoresFragment.this);
        recyclerView.setAdapter(adapter);
    }

    public void addStore(String storeName, String address, String state, String zip, String phoneNum,String city) {
        GeneralUtil.showLoadingDialog(context);
            Ion.with(this)
                    .load(AppConstants.POST, AppConstants.ADD_STORE)
                    .setHeader(AppConstants.HEADER_KEY, AppConstants.HEADER_VALUE)
                    .setMultipartParameter(AppConstants.CUSTOMER_ID, StorageUtility.getLoginModel(context).getId())
                    .setMultipartParameter(AppConstants.STORE_NAME, storeName)
                    .setMultipartParameter(AppConstants.STORE_ADDRESS, address)
                    .setMultipartParameter(AppConstants.PHONE, phoneNum)
                    .setMultipartParameter(AppConstants.STATE, state)
                    .setMultipartParameter(AppConstants.ZIP, zip)
                    .setMultipartParameter(AppConstants.CITY, city)
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String result) {
                    GeneralUtil.dismissLoadingDialog();

                    if (e == null) {
                        try {

                            JSONObject Json1 = new JSONObject(result);
                            int status = Json1.getInt(AppConstants.STATUS);
                            if (status == AppConstants.STATUS_SUCCESS) {
                                JSONObject storeObj = Json1.getJSONObject(AppConstants.STORE_DATA);
                                String storeId = storeObj.getString(AppConstants.STORE_ID);
                                String firstName = storeObj.getString(AppConstants.FIRST_NAME);
                                String lastName = storeObj.getString(AppConstants.LAST_NAME);
                                String storeName = storeObj.getString(AppConstants.STORE_NAME);
                                String description = storeObj.getString(AppConstants.DESCRIPTION);
                                String email = storeObj.getString(AppConstants.EMAIL);
                                String phoneNumber = storeObj.getString(AppConstants.PHONE);
                                String webUrl = storeObj.getString(AppConstants.WEB_URL);
                                String isFavorite = storeObj.getString(AppConstants.IS_FAVOURITE);
                                String profileImage = storeObj.getString(AppConstants.PROFILE_IMAGE);
                                String featuredImage = storeObj.getString(AppConstants.FEATURED_IMAGE);

                                JSONObject openingHourJSON = storeObj.getJSONObject(AppConstants.OPENING_HOURS);

                                String monday = openingHourJSON.getString(AppConstants.MONDAY);
                                String tuesday = openingHourJSON.getString(AppConstants.TUESDAY);
                                String wednesday = openingHourJSON.getString(AppConstants.WEDNESDAY);
                                String thursday = openingHourJSON.getString(AppConstants.THURSDAY);
                                String friday = openingHourJSON.getString(AppConstants.FRIDAY);
                                String saturday = openingHourJSON.getString(AppConstants.SATURDAY);
                                String sunday = openingHourJSON.getString(AppConstants.SUNDAY);

                                JSONObject locationJSON = storeObj.getJSONObject(AppConstants.LOCATION);
                                String address = locationJSON.getString(AppConstants.ADDRESS);
                                String city = locationJSON.getString(AppConstants.CITY);
                                String state = locationJSON.getString(AppConstants.STATE);
                                String zip = locationJSON.getString(AppConstants.ZIP);
                                String country = locationJSON.getString(AppConstants.COUNTRY);
                                String latitude = locationJSON.getString(AppConstants.LATITUDE);
                                String longitude = locationJSON.getString(AppConstants.LONGITUDE);

                                OpeningHour openingHour = new OpeningHour(monday, tuesday, wednesday, thursday, friday, saturday, sunday);
                                Location location = new Location(address,city, state, zip, country, latitude,longitude);

                                int size = top10StoreArrayList.size()-1;
                                top10StoreArrayList.add(size,new CashBackStore(storeId,firstName,lastName, storeName,description,email,phoneNumber, webUrl,isFavorite,profileImage, featuredImage, openingHour, location));
                                adapter.notifyItemInserted(top10StoreArrayList.size()-1);
                            } else {
                                GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                            }

                        } catch (Exception ex) {
                            ex.printStackTrace();
                            GeneralUtil.handleException(context, ex);
                        }

                    } else {
                        Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                        GeneralUtil.handleException(context, e);
                    }

                }
            });
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        adapter = null;
        recyclerView = null;
        linearLayoutManager = null;

    }
    @Override
    public void onDetach(){
        super.onDetach();
        context = null;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_add_store:
                scrollToBottom();
                break;
        }
    }
    public void scrollToBottom(){
        recyclerView.scrollToPosition(top10StoreArrayList.size()-1);
    }

    public void addFavorite(final CashBackStore store, final int position){

        Ion.with(this)
                .load(AppConstants.POST,AppConstants.ADD_FAVORITE_STORE)
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .setBodyParameter(AppConstants.STORE_ID,store.getStoreId())
                .setBodyParameter(AppConstants.CUSTOMER_ID,StorageUtility.getLoginModel(context).getId())
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.i(AppConstants.TAG,"result is : "+result);

                if (e == null) {
                    try {
                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            MainStorageUtility utility = MainStorageUtility.getInstance(context);
                            if(store.getIsFavorite().equals("1")) {
                                store.setIsFavorite("0");
                                utility.removeStoreFromFavorite(store);
                            }
                            else {
                                store.setIsFavorite("1");
                                utility.addStoreToFavorite(store);
                            }
                            adapter.notifyItemChanged(position);
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }
    public void makeCall(String num){
        if(num != null && !num.equals("") && GeneralUtil.isValidPhoneNumber(num)) {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + num));
            startActivity(intent);
         //   ((Activity) context).overridePendingTransition(R.anim.flip_out, R.anim.flip_in);
        }
    }
    public void scrollToPosition(int position){
        linearLayoutManager.scrollToPositionWithOffset(position,0);
    }
}
