package saivian.txlabz.com.saivian.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;

import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.models.PaymentInfo;
import saivian.txlabz.com.saivian.models.Store;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;

/**
 * Created by Fatima Siddique on 5/5/2016.
 */
public class MethodOfPaymentFragment extends Fragment implements View.OnClickListener{
    Context context;
    TextView nameValue, cardTypeValue, cardNumber, cardExpiry, cvc,etCardExpiry;
    EditText etNameValue, etCardNumber, etCvc ;
    Spinner cardType;
    ScrollView scrollView;
    Dialog dialog;
    boolean isEditClicked = false;
    int expiryMonth, expiryYear;

    TextView dummyName, dummyNumber, dummyCvc;

    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_method_of_payment, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        scrollView    = (ScrollView)view.findViewById(R.id.scrollView);
        cardExpiry    = (TextView)view.findViewById(R.id.txt_card_expiry);
        cvc           = (TextView)view.findViewById(R.id.txt_card_cvc);
        nameValue     = (TextView)view.findViewById(R.id.txt_name);
        cardTypeValue = (TextView)view.findViewById(R.id.txt_card_type);
        cardNumber    = (TextView)view.findViewById(R.id.txt_card_number);

        etNameValue   = (EditText)view.findViewById(R.id.et_holder_name);
        etCardNumber  = (EditText)view.findViewById(R.id.et_card_number);
        cardType      = (Spinner) view.findViewById(R.id.spinner_card_type);
        etCvc         = (EditText)view.findViewById(R.id.et_cvc);
        etCardExpiry  = (TextView)view.findViewById(R.id.et_expiry);
        dummyName     = (TextView)view.findViewById(R.id.dummyHolderTV);
        dummyCvc      = (TextView)view.findViewById(R.id.dummyCvcTV);
        dummyNumber   = (TextView)view.findViewById(R.id.dummyNumberTV);
        View spinner  =           view.findViewById(R.id.card_type_container);
        Button edit   = (Button)  view.findViewById(R.id.btn_edit);
        Button submit = (Button)  view.findViewById(R.id.btn_submit);
        Button cancel = (Button)  view.findViewById(R.id.btn_cancel);

        dummyNumber.setOnClickListener(this);
        dummyCvc.setOnClickListener(this);
        dummyName.setOnClickListener(this);
        spinner.setOnClickListener(this);
        etNameValue.setOnClickListener(this);
        etCardNumber.setOnClickListener(this);
        etCvc.setOnClickListener(this);
        etCardExpiry.setOnClickListener(this);

        edit.setOnClickListener(this);
        submit.setOnClickListener(this);
        cancel.setOnClickListener(this);
        etCardExpiry.setOnClickListener(this);

        createCardTypeSpinner();
        MainStorageUtility utility = MainStorageUtility.getInstance(context);
        PaymentInfo paymentInfo = utility.getPaymentInfo();
        if(paymentInfo != null) {
            nameValue.setText(paymentInfo.getCardHolderName());
            cardTypeValue.setText(paymentInfo.getCardType());
            cardNumber.setText(paymentInfo.getCardNumber());
            cvc.setText(paymentInfo.getCvc());
            if(!paymentInfo.getExpiryMonth().equals("") && !paymentInfo.getExpiryYear().equals(""))
                cardExpiry.setText(paymentInfo.getExpiryMonth()+"/"+paymentInfo.getExpiryYear());
        }
    }
    private void createCardTypeSpinner() {
        ArrayAdapter countAdapter = ArrayAdapter.createFromResource(context, R.array.cardType, R.layout.spinner_dropdown_item);
        countAdapter.setDropDownViewResource(R.layout.custom_spinner_item);

        cardType.setAdapter(countAdapter);
        cardType.setSelection(0, false);
        cardType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    private void fetchPaymentInfo(){
        GeneralUtil.showLoadingDialog(context);
        Ion.with(this)
                .load(AppConstants.GET,AppConstants.FETCH_PAYMENT_INFO+"?customer_id="+ StorageUtility.getLoginModel(context).getId())
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            JSONObject paymentInfo = Json1.getJSONObject(AppConstants.PAYMENT_INFO);
                            String id = paymentInfo.getString(AppConstants.ID);
                            String customerId = paymentInfo.getString(AppConstants.CUSTOMER_ID);
                            String cardHolderName = paymentInfo.getString(AppConstants.CARD_HOLDER_NAME);
                            String cardType = paymentInfo.getString(AppConstants.CARD_TYPE);
                            String cardNum = paymentInfo.getString(AppConstants.CARD_NUMBER);

                            nameValue.setText(cardHolderName);
                            cardTypeValue.setText(cardType);
                            cardNumber.setText(cardNum);

                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }

    private void savePaymentInfo(){
        GeneralUtil.showLoadingDialog(context);
        Ion.with(this)
                .load(AppConstants.POST,AppConstants.ADD_PAYMENT_INFO)
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .setBodyParameter(AppConstants.CUSTOMER_ID,StorageUtility.getLoginModel(context).getId())
                .setBodyParameter(AppConstants.CARD_HOLDER_NAME, etNameValue.getText().toString())
                .setBodyParameter(AppConstants.CARD_TYPE, cardType.getSelectedItem().toString())
                .setBodyParameter(AppConstants.CARD_NUMBER, etCardNumber.getText().toString())
                .setBodyParameter(AppConstants.CVC, etCvc.getText().toString())
                .setBodyParameter(AppConstants.EXPIRY_MONTH, String.valueOf(expiryMonth))
                .setBodyParameter(AppConstants.EXPIRY_YEAR, String.valueOf(expiryYear))
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));

                            cardTypeValue.setText(cardType.getSelectedItem().toString());
                            cardNumber.setText(etCardNumber.getText().toString());
                            nameValue.setText(etNameValue.getText().toString());
                            cvc.setText(etCvc.getText().toString());
                            cardExpiry.setText(expiryMonth+"/"+expiryYear);
                            etNameValue.setText("");
                            etCardNumber.setText("");
                            etCvc.setText("");
                            etCardExpiry.setText("");
                            cardType.setSelection(0,false);
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }

    @Override
    public void onClick(View view) {
        GeneralUtil.hideKeyboard((Activity) context);
        switch (view.getId()){
            case R.id.dummyHolderTV:
                editNotAllowed();
                break;
            case R.id.dummyCvcTV:
                editNotAllowed();
                break;
            case R.id.dummyNumberTV:
                editNotAllowed();
                break;
            case R.id.btn_submit:
                if(isDataValid())
                    savePaymentInfo();
                break;
            case R.id.btn_edit:
                isEditClicked = true;

                dummyName.setVisibility(View.GONE);
                dummyCvc.setVisibility(View.GONE);
                dummyNumber.setVisibility(View.GONE);

                etNameValue.setVisibility(View.VISIBLE);
                etCardNumber.setVisibility(View.VISIBLE);
                etCvc.setVisibility(View.VISIBLE);

                final Handler handler;
                handler = new Handler();

                final Runnable r = new Runnable() {
                    public void run() {
                        scrollView.smoothScrollTo(0, 500);

                    }
                };
                handler.postDelayed(r, 200);
                break;
            case R.id.et_expiry:
                if(!isEditClicked) {
                    editNotAllowed();
                    return;
                }
                showCalenderDialog();
                break;
        }
    }
    private void editNotAllowed(){
        GeneralUtil.showAlert(context,context.getResources().getString(R.string.error_edit));
        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }
    private boolean isDataValid() {
        boolean result = true;
        if (etNameValue.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter Card Holder Name");
        }else if (etCardNumber.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter Card Number");
        }else if (etCvc.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter Card CVC");
        }else if (etCardExpiry.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(context,"Enter Card Expiry Date");
        }
        return result;
    }
    private void showCalenderDialog(){
        if(dialog != null)
            dialog.dismiss();
        dialog = new Dialog(context, R.style.CustomThemeDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_calender);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        final NumberPicker monthPicker = (NumberPicker) dialog.findViewById(R.id.month_picker);
        final NumberPicker yearPicker = (NumberPicker) dialog.findViewById(R.id.year_picker);
        Button done = (Button)dialog.findViewById(R.id.btn_done);
        monthPicker.setMaxValue(12);
        monthPicker.setMinValue(1);

        yearPicker.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);


        final int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        final int month = Calendar.getInstance().get(Calendar.MONTH)+1;
        final int day = Calendar.getInstance().get(Calendar.DATE);
        final String[] yearsList = new String[7];

        int index = 0;
        for (int i = thisYear; i < thisYear+7; i++) {
            yearsList[index]=(Integer.toString(i));
            index++;
        }

        yearPicker.setMinValue(0);
        yearPicker.setMaxValue(yearsList.length-1);
        yearPicker.setWrapSelectorWheel(true);

        yearPicker.setValue(0);
        yearPicker.setDisplayedValues(yearsList);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expiryMonth = monthPicker.getValue();
                expiryYear = Integer.parseInt(yearsList[yearPicker.getValue()]);
                if(expiryMonth < month && expiryYear == thisYear){
                    Toast.makeText(context,"Please select Valid Expiry Time",Toast.LENGTH_SHORT).show();
                }else {
                    etCardExpiry.setText(monthPicker.getValue() + "/" + yearsList[yearPicker.getValue()]);
                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        nameValue = null;
        etNameValue = null;
        cardNumber = null;
        cardType = null;
        cardTypeValue = null;
        etCardNumber = null;
        context = null;
    }
}
