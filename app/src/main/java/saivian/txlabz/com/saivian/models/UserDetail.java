package saivian.txlabz.com.saivian.models;

/**
 * Created by Fatima Siddique on 5/16/2016.
 */
public class UserDetail {

    String id;
    String saivianId;
    String referralId;
    String firstName;
    String lastName;
    String email;
    String password;
    String profileImage;
    String gender;
    String dob;
    String city;
    String state;
    String zip;
    String country;
    String isVerified;
    String isActive;
    String createdDate;
    String phone;

    public UserDetail(String id, String referralId, String firstName, String lastName, String email, String password, String profileImage, String gender, String dob, String city, String state, String zip, String country, String isVerified, String isActive, String createdDate, String phone, String saivianId) {
        this.id = id;
        this.referralId = referralId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.profileImage = profileImage;
        this.gender = gender;
        this.dob = dob;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.country = country;
        this.isVerified = isVerified;
        this.isActive = isActive;
        this.createdDate = createdDate;
        this.phone = phone;
        this.saivianId = saivianId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferralId() {
        return referralId;
    }

    public void setReferralId(String referralId) {
        this.referralId = referralId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSaivianId() {
        return saivianId;
    }

    public void setSaivianId(String saivianId) {
        this.saivianId = saivianId;
    }
}
