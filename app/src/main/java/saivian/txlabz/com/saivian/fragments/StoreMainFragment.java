package saivian.txlabz.com.saivian.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import java.util.ArrayList;
import java.util.List;

import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;

/**
 * Created by Fatima Siddique on 5/4/2016.
 */
public class StoreMainFragment extends Fragment implements View.OnClickListener {
    Context context;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    View activityRootView;
    boolean isFragmentPaused = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        isFragmentPaused = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_store_main, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        activityRootView = ((MainActivity)context).getRootView();
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(listener);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new Top10StoresFragment(), "Top 10 Stores");
        adapter.addFragment(new MethodOfPaymentFragment(), "Method of Payment");
        adapter.addFragment(new MarketResearchFragment(), "Market Research");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);
    }
    ViewTreeObserver.OnGlobalLayoutListener listener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            if(isFragmentPaused)
                return;
            Rect r = new Rect();
            if(activityRootView != null)
                activityRootView.getWindowVisibleDisplayFrame(r);
            else{
                activityRootView = ((MainActivity)context).getRootView();
            }
            int screenHeight = activityRootView.getRootView().getHeight();

            int keypadHeight = screenHeight - r.bottom;

            if (keypadHeight > screenHeight * 0.15) {
                ((MainActivity)context).hideBottomBar();
            }
            else {
                ((MainActivity)context).showBottomBar();
            }
        }
    };
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public void onClick(View view) {

    }
    @Override
    public void onResume() {
        super.onResume();
        isFragmentPaused = false;
    }
    @Override
    public void onStop() {
        super.onStop();
        isFragmentPaused = true;
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        isFragmentPaused = true;
        if(activityRootView != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                activityRootView.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
            } else {
                activityRootView.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
            }
            activityRootView = null;
        }
        activityRootView = null;

    }
}
