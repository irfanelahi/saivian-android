package saivian.txlabz.com.saivian.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import saivian.txlabz.com.saivian.models.CashBackStore;
import saivian.txlabz.com.saivian.models.OpeningHour;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.fragments.CashBlockLocalItemFragment;

/**
 * Created by Fatima Siddique on 3/20/2016.
 */
public class CashBlockLocalItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<CashBackStore> data = Collections.emptyList();

    private LayoutInflater inflater;
    private Context context;
    int lastClickedItem = -1;
    String fragmentName;
    CashBlockLocalItemFragment fragment;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    private final HashSet<MapView> mMaps = new HashSet<MapView>();

    private GoogleMap mGoogleMap;
    public CashBlockLocalItemAdapter(Context context, List<CashBackStore> data, String fragmentName, CashBlockLocalItemFragment fragment) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.fragmentName = fragmentName;
        this.fragment = fragment;
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.progress)
                .showImageForEmptyUri(R.drawable.progress)
                .showImageOnFail(R.drawable.progress).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();

    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AppConstants.CASH_BACK_STATE normalState = AppConstants.CASH_BACK_STATE.NORMAL;
        AppConstants.CASH_BACK_STATE selectedState = AppConstants.CASH_BACK_STATE.ITEM_SELECTED;
        if (viewType == normalState.getIntValue()) {
            View view = inflater.inflate(R.layout.layout_cash_block_list_item, parent, false);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        } else {
            View view = inflater.inflate(R.layout.layout_cash_back_item_pressed, parent, false);
            ViewHolderSelected holder = new ViewHolderSelected(view);
            return holder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        AppConstants.CASH_BACK_STATE state = data.get(position).getState();
        return state.getIntValue();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        int dp = (int) (context.getResources().getDimension(R.dimen.marginTop_cashback) / context.getResources().getDisplayMetrics().density);
        final CashBackStore store = data.get(position);
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
//            if (position == 0) {
//                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                layoutParams.setMargins(0, GeneralUtil.dpToPx(dp),0,0);
//                viewHolder.parentPanel.setLayoutParams(layoutParams);
//            }
            viewHolder.imageView.clearAnimation();
            imageLoader.displayImage(data.get(position).getProfileImage(),viewHolder.imageView,options);
            viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fragment.scrollToPosition(position);
                    if (lastClickedItem > -1) {
                        data.get(lastClickedItem).setState(AppConstants.CASH_BACK_STATE.NORMAL);
                        notifyItemChanged(lastClickedItem);
                    }
                    data.get(position).setState(AppConstants.CASH_BACK_STATE.ITEM_SELECTED);
                    notifyItemChanged(position);

                    lastClickedItem = position;

                }
            });
        }
        if (holder instanceof ViewHolderSelected) {
            final ViewHolderSelected viewHolder = (ViewHolderSelected) holder;
//            if (position == 0) {
//                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
//                        RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                layoutParams.setMargins(0, GeneralUtil.dpToPx(dp), 0, 0);
//                viewHolder.parentPanel.setLayoutParams(layoutParams);
//            }
            if(!alreadyInitialed(viewHolder.mapView)) {
                viewHolder.initializeMapView();
                mMaps.add(viewHolder.mapView);
            }

            if(store.getLocation() != null && store.getLocation().getLongitude() != null && store.getLocation().getLatitude() != null) {
                if(!store.getLocation().getLongitude().isEmpty() && !store.getLocation().getLatitude().isEmpty()) {
                    double lat = Double.parseDouble(store.getLocation().getLatitude());
                    double lng = Double.parseDouble(store.getLocation().getLongitude());
                    LatLng item = new LatLng(lat, lng);
                    viewHolder.mapView.setTag(item);
                    if (viewHolder.googleMap != null) {
                        setMapLocation(viewHolder.googleMap, item);
                    }
                }
            }

            viewHolder.itemContainer.removeAllViews();
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = null;
            final AppConstants.CASH_BACK_STATE state = data.get(position).getState();
            int resource = 0;
            if (state == AppConstants.CASH_BACK_STATE.ITEM_SELECTED)
                resource = R.layout.layout_pressed_image;
            else if (state == AppConstants.CASH_BACK_STATE.MAP_SELECTED) {
                resource = R.layout.layout_pressed_map;

            }
            else if (state == AppConstants.CASH_BACK_STATE.HOURS_SELECTED) {
                resource = R.layout.layout_opening_hours;
            }
            v = vi.inflate(resource, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            populateView(v,state,position,viewHolder);

            v.setLayoutParams(layoutParams);
            viewHolder.itemContainer.addView(v);
//            viewHolder.txtAddress1.setText(store.getLocation().getAddress());
            String address2 = "";

            if(!store.getLocation().getCity().equals("") && !store.getLocation().getCountry().equals(""))
                address2 = store.getLocation().getCity() + ", "+ store.getLocation().getCountry();
            else if(!store.getLocation().getCity().equals("") && store.getLocation().getCountry().equals(""))
                address2 = store.getLocation().getCity();
            else if(store.getLocation().getCity().equals("") && !store.getLocation().getCountry().equals(""))
                address2 = store.getLocation().getCountry();
            String finalAddress = store.getLocation().getAddress() + " " +address2;
            if(!finalAddress.trim().equals("")) {
                viewHolder.mapContainer.setVisibility(View.VISIBLE);
                viewHolder.txtAddress1.setText(finalAddress);
            }
            else
                viewHolder.mapContainer.setVisibility(View.GONE);

            viewHolder.hotelName.setText(store.getStoreName());
            String finalPhone = store.getPhoneNumber();
            if(!finalPhone.equals("")) {
                viewHolder.phoneContainer.setVisibility(View.VISIBLE);
                viewHolder.txtPhone.setText("Phone: " + finalPhone);
            }
            else {
                viewHolder.phoneContainer.setVisibility(View.GONE);
            }
            String webUrl = store.getWebUrl();
            if(!webUrl.equals("")) {
                viewHolder.websiteContainer.setVisibility(View.VISIBLE);
                viewHolder.txtWebUrl.setText("Website: " + webUrl);
            }
            else
                viewHolder.websiteContainer.setVisibility(View.GONE);

            viewHolder.txtDescription.setText(store.getDescription().trim());
            if(store.getIsFavorite().equals("1"))
                viewHolder.favorite.setImageResource(R.drawable.fave_icon_active);
            else
                viewHolder.favorite.setImageResource(R.drawable.fave_icon);

            viewHolder.mapContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(data.get(position).getState() != AppConstants.CASH_BACK_STATE.MAP_SELECTED) {
                        if (!store.getLocation().getLatitude().equals("") || !(store.getLocation().getLongitude().equals(""))) {
                            ((MainActivity) context).applyAnimationOnParentView(false);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    data.get(position).setState(AppConstants.CASH_BACK_STATE.MAP_SELECTED);
                                    notifyItemChanged(position);
                                }
                            }, 60);
                        } else {
                            GeneralUtil.showAlert(context, "Location not setup for this Store");
                        }
                    }
                    return;
                }
            });
            viewHolder.openHoursContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (data.get(position).getState() != AppConstants.CASH_BACK_STATE.HOURS_SELECTED) {
                        ((MainActivity) context).applyAnimationOnParentView(false);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                data.get(position).setState(AppConstants.CASH_BACK_STATE.HOURS_SELECTED);
                                notifyItemChanged(position);
                            }
                        }, 60);
                    }
                }
            });
            viewHolder.phoneContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!data.get(position).getPhoneNumber().equals(""))
                        fragment.makeCall(data.get(position).getPhoneNumber());
                    else
                        GeneralUtil.showAlert(context,"No phone number setup for this store");
                }
            });
            viewHolder.websiteContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    GeneralUtil.openBrowserForInstall(context,data.get(position).getWebUrl());
                }
            });
            viewHolder.hotelName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(state == AppConstants.CASH_BACK_STATE.MAP_SELECTED | state == AppConstants.CASH_BACK_STATE.HOURS_SELECTED) {
                        ((MainActivity) context).applyAnimationOnParentView(true);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                data.get(position).setState(AppConstants.CASH_BACK_STATE.ITEM_SELECTED);
                                notifyItemChanged(position);
                            }
                        }, 60);
                    } else {
                        (viewHolder).parentPanel.clearAnimation();
                        data.get(position).setState(AppConstants.CASH_BACK_STATE.NORMAL);
                        notifyItemChanged(position);
                    }

                }
            });
            viewHolder.favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            fragment.addFavorite(data.get(position),position);
                            notifyItemChanged(position);

                        }
                    });
                }
            });
        }
    }

    private boolean alreadyInitialed(MapView mapView) {

        for (MapView map : mMaps) {
            if(map == mapView)
                return true;
        }
        return false;
    }


    private void populateView(View v, final AppConstants.CASH_BACK_STATE state, final int position, final ViewHolderSelected viewHolder) {
        final CashBackStore store = data.get(position);
        if (state == AppConstants.CASH_BACK_STATE.ITEM_SELECTED){
            ImageView featuredImage = (ImageView) v.findViewById(R.id.list_item);
            imageLoader.displayImage(store.getProfileImage(),featuredImage,options);
            featuredImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    (viewHolder).parentPanel.clearAnimation();
                    data.get(position).setState(AppConstants.CASH_BACK_STATE.NORMAL);
                    notifyItemChanged(position);
                }
            });
        }
        else if (state == AppConstants.CASH_BACK_STATE.MAP_SELECTED) {
            viewHolder.mapView.setVisibility(View.VISIBLE);
//            MapView mapView = (MapView) v.findViewById(R.id.lite_listrow_map);

//            final SupportMap mMapFragment = new SupportMap();
//
//            SupportMap.MapViewCreatedListener mapViewCreatedListener = new SupportMap.MapViewCreatedListener() {
//                @Override
//                public void onMapCreated() {
//                    mGoogleMap = mMapFragment.getMap();
//                    if (mGoogleMap != null) {
//                        setupMap(mGoogleMap,store.getLocation().getLatitude(),store.getLocation().getLongitude());
//                    }
//
//                }
//            };
//            mMapFragment.itsMapViewCreatedListener = mapViewCreatedListener;
//
//            FragmentManager fm = fragment.getChildFragmentManager();
//            SupportMapFragment supportMapFragment = mMapFragment;
//            fm.beginTransaction().replace(R.id.layout_map, supportMapFragment).commit();

        }
        else if (state == AppConstants.CASH_BACK_STATE.HOURS_SELECTED){
            TextView sundayTiming = (TextView)v.findViewById(R.id.lbl_sunday_timing);
            TextView mondayTiming = (TextView)v.findViewById(R.id.lbl_monday_timing);
            TextView tuesdayTiming = (TextView)v.findViewById(R.id.lbl_tuesday_timing);
            TextView wednesdayTiming = (TextView)v.findViewById(R.id.lbl_wednesday_timing);
            TextView thursdayTiming = (TextView)v.findViewById(R.id.lbl_thursday_timing);
            TextView fridayTiming = (TextView)v.findViewById(R.id.lbl_friday_timing);
            TextView saturdayTiming = (TextView)v.findViewById(R.id.lbl_saturday_timing);

            OpeningHour openingHour = store.getOpeningHour();
            sundayTiming.setText(openingHour.getSunday());
            mondayTiming.setText(openingHour.getMonday());
            tuesdayTiming.setText(openingHour.getTuesday());
            wednesdayTiming.setText(openingHour.getWednesday());
            thursdayTiming.setText(openingHour.getThursday());
            fridayTiming.setText(openingHour.getFriday());
            saturdayTiming.setText(openingHour.getSaturday());
        }
    }
    protected void setupMap(GoogleMap map, String lat, String lng) {

        mGoogleMap = map;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        Log.d(AppConstants.TAG, "Map Ready");

        if (mGoogleMap != null) {
            updateMarker(lat, lng);
        }

    }
    private void updateMarker(String lat, String lng) {
        Double mLat = Double.parseDouble(lat);
        Double mLng = Double.parseDouble(lng);
        LatLng mLatLng = new LatLng(mLat,mLng);

        MarkerOptions mMarkerOptions = new MarkerOptions();

        if (mGoogleMap != null) {

            mGoogleMap.addMarker(mMarkerOptions
                    .position(mLatLng)
                    .draggable(false)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.map_marker)));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng,14));
        }

    }
    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        FrameLayout parentPanel;
        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.list_item);
            parentPanel = (FrameLayout) itemView.findViewById(R.id.parentPanel);
        }
    }
    class ViewHolderSelected extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        ImageView map, openHours, phone, featuredImage;
        TextView hotelName;
        LinearLayout itemContainer;
        RelativeLayout parentPanel;
        TextView txtWebUrl, txtPhone, txtAddress1, txtAddress2, txtDescription;
        ImageView favorite;
        View mapContainer, phoneContainer, websiteContainer, openHoursContainer;
        MapView mapView;
        GoogleMap googleMap;
        public ViewHolderSelected(View itemView) {
            super(itemView);
            map             = (ImageView)itemView.findViewById(R.id.img_map);
            openHours       = (ImageView)itemView.findViewById(R.id.img_openHours);
            phone           = (ImageView)itemView.findViewById(R.id.img_phone);
            hotelName       = (TextView)itemView.findViewById(R.id.txt_restaurant_name);
            itemContainer   = (LinearLayout)itemView.findViewById(R.id.main_list_container);
            parentPanel     = (RelativeLayout) itemView.findViewById(R.id.parentPanel);
            txtWebUrl       = (TextView)itemView.findViewById(R.id.txt_website);
            txtPhone        = (TextView)itemView.findViewById(R.id.txt_phone);
            txtAddress1     = (TextView)itemView.findViewById(R.id.txt_address_1);
            txtAddress2     = (TextView)itemView.findViewById(R.id.txt_address_2);
            txtDescription  = (TextView)itemView.findViewById(R.id.txt_restaurant_description);
            favorite        = (ImageView)itemView.findViewById(R.id.icon_fav);

            mapContainer    = itemView.findViewById(R.id.mapContainer);
            phoneContainer  = itemView.findViewById(R.id.phoneContainer);
            websiteContainer= itemView.findViewById(R.id.websiteContainer);
            openHoursContainer = itemView.findViewById(R.id.hoursContainer);
            mapView         = (MapView) itemView.findViewById(R.id.mapView);

        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(context);
            this.googleMap = googleMap;
            LatLng data = (LatLng) mapView.getTag();
            if (data != null) {
                setMapLocation(googleMap, data);
            }
        }

        /**
         * Initialises the MapView by calling its lifecycle methods.
         */
        public void initializeMapView() {
            if (mapView != null) {
                // Initialise the MapView
                mapView.onCreate(null);
                // Set the map ready callback to receive the GoogleMap object
                mapView.getMapAsync(this);
            }
        }

    }
    private static void setMapLocation(GoogleMap map, LatLng data) {
        // Add a marker for this item and set the camera
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(data, 14f));
        map.addMarker(new MarkerOptions().position(data));

        // Set the map type back to normal.
        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

    }

}