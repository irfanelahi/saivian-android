package saivian.txlabz.com.saivian;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

import saivian.txlabz.com.saivian.models.UserDetail;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;
import saivian.txlabz.com.saivian.views.ActivitySwitcher;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener{
    EditText etFirstName, etLastName, etEmailAddress, etReferral, etPassword;
    TextView txtDob;
    private int year = 0;
    private int month = 0;
    private int day = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        overridePendingTransition(R.anim.flip_in,R.anim.flip_out);
        setContentView(R.layout.activity_signup);

        init();
    }

    private void init() {
        etFirstName = (EditText)findViewById(R.id.firstNameEditText);
        etLastName = (EditText)findViewById(R.id.lastNameEditText);
        etEmailAddress = (EditText)findViewById(R.id.emailEditText);
        txtDob = (TextView)findViewById(R.id.dobTextView);
        etReferral = (EditText) findViewById(R.id.referralEditText);
        etPassword = (EditText)findViewById(R.id.passwordEditText);
        Button signUp = (Button)findViewById(R.id.btn_signUp);
        signUp.setOnClickListener(this);
        txtDob.setOnClickListener(this);
    }
    private void createDOBDialog() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd = new DatePickerDialog(this, myDateSetListener, year, month, day);
//        dpd.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpd.getDatePicker().setMaxDate(new Date().getTime());
        dpd.show();
    }
    DatePickerDialog.OnDateSetListener myDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker datePicker, int i, int j, int k) {

            year = i;
            month = j + 1;
            day = k;
            txtDob.setText(String.format("%02d",year)+"-"+String.format("%02d",month)+"-"+String.format("%02d",day));
        }
    };
    @Override
    public void onClick(View view) {
        GeneralUtil.hideKeyboard(this);
        switch (view.getId()){
            case R.id.btn_signUp:
                if(isDataValid()){
                    sendSignUpRequestToServer();
                }
                break;
            case R.id.dobTextView:
                createDOBDialog();
                break;
        }
    }

    private boolean isDataValid() {
        boolean result = true;
        if(!GeneralUtil.isNetworkAvailable(this)) {
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.internet_not_connected));
        }else if(etFirstName.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.error_firstName));
        }else if(etLastName.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.error_lastName));
        }
        else if(etEmailAddress.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.error_email));
        }else if(!GeneralUtil.isEmailValid(etEmailAddress.getText().toString())){
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.error_email));
        }else if(txtDob.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.error_dob));
        }else if(etReferral.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.error_referral));
        }else if (etPassword.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.error_password));
        }
        return result;
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(AppConstants.SHOULD_RUN_SPLASH,"false");
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        final Intent finalIntent = intent;
        ActivitySwitcher.animationOut(findViewById(R.id.container), getWindowManager(), new ActivitySwitcher.AnimationFinishedListener() {
            @Override
            public void onAnimationFinished() {
                startActivity(finalIntent);
                finish();
            }
        });
    }
    protected void sendSignUpRequestToServer(){
        GeneralUtil.showLoadingDialog(this);
        Ion.with(this)
                .load(AppConstants.POST,AppConstants.SIGN_UP_URL)
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .setTimeout(20*1000)
                .setBodyParameter(AppConstants.EMAIL, etEmailAddress.getText().toString())
                .setBodyParameter(AppConstants.FIRST_NAME, etFirstName.getText().toString())
                .setBodyParameter(AppConstants.LAST_NAME, etLastName.getText().toString())
                .setBodyParameter(AppConstants.PASSWORD, etPassword.getText().toString())
                .setBodyParameter(AppConstants.REFERRAL_ID, etReferral.getText().toString())
                .setBodyParameter(AppConstants.DOB, txtDob.getText().toString())
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            String sessionToken = Json1.getString(AppConstants.SESSION_TOKEN);
//                            JSONObject customerJObj = Json1.getJSONObject(AppConstants.CUSTOMER_DATA);
//                            String id = customerJObj.getString(AppConstants.ID);
//                            String referralId = customerJObj.getString(AppConstants.REFERRAL_ID);
//                            String firstName = customerJObj.getString(AppConstants.FIRST_NAME);
//                            String lastName = customerJObj.getString(AppConstants.LAST_NAME);
//                            String email = customerJObj.getString(AppConstants.EMAIL);
//                            String password = customerJObj.getString(AppConstants.PASSWORD);
//                            String profileImage = customerJObj.getString(AppConstants.PROFILE_IMAGE);
//                            String gender = customerJObj.getString(AppConstants.GENDER);
//                            String dob = customerJObj.getString(AppConstants.DOB);
//                            String city = customerJObj.getString(AppConstants.CITY);
//                            String state = customerJObj.getString(AppConstants.STATE);
//                            String zip = customerJObj.getString(AppConstants.ZIP);
//                            String country = customerJObj.getString(AppConstants.COUNTRY);
//                            String isVerified = customerJObj.getString(AppConstants.IS_VERIFIED);
//                            String isActive = customerJObj.getString(AppConstants.IS_ACTIVE);
//                            String createdDate = customerJObj.getString(AppConstants.CREATED_DATE);
//
//                            UserDetail userDetail = new UserDetail(id, referralId,firstName,lastName,email,password,profileImage,gender,dob,city, state,zip,country, isVerified,isActive,createdDate);
//                            StorageUtility.saveLoginObject(userDetail,SignupActivity.this);
                            StorageUtility.saveDataInPreferences(AppConstants.SESSION_TOKEN,sessionToken,SignupActivity.this);
                            StorageUtility.saveDataInPreferences(AppConstants.IS_USER_LOGGED_IN,"true",SignupActivity.this);
                            MainStorageUtility utility = MainStorageUtility.getInstance(SignupActivity.this);
                            utility.processJSON(result);
                            Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else  {
                            GeneralUtil.showAlert(SignupActivity.this, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(SignupActivity.this, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(SignupActivity.this,e);
                }

            }
        });
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        GeneralUtil.unbindDrawables(findViewById(R.id.container));
        System.gc();
        finish();
    }
}
