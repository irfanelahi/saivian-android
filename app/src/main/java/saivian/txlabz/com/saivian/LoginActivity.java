package saivian.txlabz.com.saivian;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONObject;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import saivian.txlabz.com.saivian.models.UserDetail;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.Delegate;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;
import saivian.txlabz.com.saivian.views.ActivitySwitcher;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends Activity implements View.OnClickListener, Delegate {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    EditText etEmail, etPassword;
    View container;
    ScrollView loginScroll;
    GifImageView gifImageView;
    ImageView mainLogo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        init();
        MainStorageUtility utility = MainStorageUtility.getInstance(this);
        utility.setDelegate(this);

        gifImageView.setVisibility(View.GONE);
        loginScroll.setVisibility(View.GONE);
        Bundle bundle = getIntent().getExtras();
        String showRunSplash = "true";
        if(bundle != null){
            showRunSplash = bundle.getString(AppConstants.SHOULD_RUN_SPLASH);
            Log.i(AppConstants.TAG,"show run splash .... " + showRunSplash);

        }

        if(StorageUtility.getDataFromPreferences(AppConstants.IS_USER_LOGGED_IN,"false",this).equals("true"))
            utility.getDataInBackground();

        if(showRunSplash == null || (showRunSplash != null && showRunSplash.equals("true"))){
            runSplashFlow();
        }
        else {
            gifImageView.setVisibility(View.GONE);
            loginScroll.setVisibility(View.VISIBLE);
            animationOnLoginView();
        }
    }

    private void runSplashFlow() {
        gifImageView.setVisibility(View.VISIBLE);
        gifImageView.setImageResource( R.drawable.splash);
        GifDrawable gifDrawable = (GifDrawable) gifImageView.getDrawable();
        gifDrawable.addAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationCompleted(int loopNumber) {
                Intent intent = null;
                if(StorageUtility.getDataFromPreferences(AppConstants.IS_USER_LOGGED_IN,"false",LoginActivity.this).equals("true")) {
                    intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else
                    animationOnLoginView();

            }
        });
    }
    private void animationOnLoginView(){
        gifImageView.setVisibility(View.GONE);
        loginScroll.setVisibility(View.VISIBLE);

        Animation translateAnim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.translate_logo);
        mainLogo.startAnimation(translateAnim);
        translateAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation1) {
                final View loginContainer = findViewById(R.id.loginContainer);
                Animation animation = AnimationUtils.loadAnimation(LoginActivity.this,R.anim.scale_zoom_in_view);
                loginContainer.startAnimation(animation);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        loginContainer.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void init() {
        mainLogo = (ImageView)findViewById(R.id.mainLogo);
        gifImageView = (GifImageView)findViewById(R.id.gifImageView);
        loginScroll = (ScrollView)findViewById(R.id.loginScrollContainer);
        container = findViewById(R.id.container);
        etEmail = (EditText)findViewById(R.id.emailEditText);
        etPassword = (EditText)findViewById(R.id.passwordEditText);
        TextView forgotPassword = (TextView)findViewById(R.id.forgotPassword);

        Button signInButton = (Button) findViewById(R.id.signinButton);
        TextView signUp = (TextView) findViewById(R.id.txt_sign_up);




        signInButton.setOnClickListener(this);
        signUp.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        GeneralUtil.hideKeyboard(this);
        Intent intent = null;
        switch (v.getId()) {
            case R.id.signinButton:
                if(isDataValid())
                    sendLoginRequestToServer();
                break;
            case R.id.txt_sign_up:
                // we only animateOut this activity here.
                // The new activity will animateIn from its onResume() - be sure to implement it.
                intent = new Intent(getApplicationContext(), SignupActivity.class);
                // disable default animation for new intent
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                final Intent finalIntent = intent;
                ActivitySwitcher.animationOut(container, getWindowManager(), new ActivitySwitcher.AnimationFinishedListener() {
                    @Override
                    public void onAnimationFinished() {
                        startActivity(finalIntent);
                        finish();
                    }
                });
                break;
            case R.id.forgotPassword:
                intent = new Intent(this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;
        }
    }
    private boolean isDataValid() {
        boolean result = true;
        if(!GeneralUtil.isNetworkAvailable(this)) {
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.internet_not_connected));
        } else if(etEmail.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.error_email));
        }/* else if(!GeneralUtil.isEmailValid(etEmail.getText().toString())){
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.error_email));
        }*/ else if (etPassword.getText().toString().equals("")){
            result = false;
            GeneralUtil.showAlert(this, getString(R.string.error_password));
        }
        return result;
    }
    protected void sendLoginRequestToServer(){
        GeneralUtil.showLoadingDialog(this);
        Ion.with(this)
                .load(AppConstants.SIGN_IN_URL)
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .setTimeout(20*1000)
                .setBodyParameter(AppConstants.EMAIL, etEmail.getText().toString())
                .setBodyParameter(AppConstants.PASSWORD, etPassword.getText().toString())
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {

                            StorageUtility.saveDataInPreferences(AppConstants.IS_USER_LOGGED_IN, "true", LoginActivity.this);
                            MainStorageUtility utility = MainStorageUtility.getInstance(LoginActivity.this);
                            utility.processJSON(result);
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            GeneralUtil.showAlert(LoginActivity.this, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(LoginActivity.this, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(LoginActivity.this, e);
                }

            }
        });
    }
    @Override
    public void onBackPressed(){
        System.exit(0);
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        container.clearAnimation();
        GeneralUtil.unbindDrawables(findViewById(R.id.container));
        System.gc();
        finish();
    }

    @Override
    public void callback(String result) {

    }
}
