package saivian.txlabz.com.saivian.models;

import saivian.txlabz.com.saivian.utils.AppConstants;

/**
 * Created by Fatima Siddique on 5/17/2016.
 */
public class CashBackStore {
    String storeId;
    String firstName;
    String lastName;
    String storeName;
    String description;
    String email;
    String phoneNumber;
    String webUrl;
    String isFavorite;
    String profileImage;
    String featuredImage;
    OpeningHour openingHour;
    Location location;
    AppConstants.CASH_BACK_STATE state;
    AppConstants.TOP10_STATE top10State;
    public CashBackStore(String storeId, String firstName, String lastName, String storeName, String description, String email, String phoneNumber, String webUrl, String isFav, String profileImage, String featuredImage, OpeningHour openingHour, Location location) {
        this.storeId = storeId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.storeName = storeName;
        this.description = description;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.webUrl = webUrl;
        this.isFavorite = isFav;
        this.profileImage = profileImage;
        this.featuredImage = featuredImage;
        this.openingHour = openingHour;
        this.location = location;
        this.state = AppConstants.CASH_BACK_STATE.NORMAL;
        this.top10State = AppConstants.TOP10_STATE.NORMAL;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public void setFeaturedImage(String featuredImage) {
        this.featuredImage = featuredImage;
    }

    public OpeningHour getOpeningHour() {
        return openingHour;
    }

    public void setOpeningHour(OpeningHour openingHour) {
        this.openingHour = openingHour;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public AppConstants.CASH_BACK_STATE getState() {
        return state;
    }

    public void setState(AppConstants.CASH_BACK_STATE state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public AppConstants.TOP10_STATE getTop10State() {
        return top10State;
    }

    public void setTop10State(AppConstants.TOP10_STATE top10State) {
        this.top10State = top10State;
    }
}
