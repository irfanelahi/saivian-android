package saivian.txlabz.com.saivian.models;

/**
 * Created by Fatima Siddique on 5/18/2016.
 */
public class StoreListItem {
    String storeId;
    String storeName;
    String phoneNumber;
    String profileImage;

    public StoreListItem(String storeId, String storeName, String phoneNumber, String profileImage) {
        this.storeId = storeId;
        this.storeName = storeName;
        this.phoneNumber = phoneNumber;
        this.profileImage = profileImage;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
