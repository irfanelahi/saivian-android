package saivian.txlabz.com.saivian.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.views.CameraPreview;

/**
 * Created by Fatima Siddique on 5/8/2016.
 */
public class CameraFragment extends Fragment implements View.OnClickListener{

    private Camera mCamera;
    private CameraPreview mCameraPreview;
    Bitmap bitmap1;
    Context context;
    ImageView selectOptionImage;
    boolean isTravelSelected = false;
    boolean isShoppingSelected = false;
    ImageView travelImage, shoppingImage;

    ImageView screenGuide;
    View screenGuideContainer;
    View retakeContainer;
    String filePath = null;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_camera, container, false);
        ((MainActivity)context).hideBottomBar();
        init(view);
        return view;
    }

    private void init(View view) {

        mCamera = getCameraInstance();
        mCameraPreview = new CameraPreview(context, mCamera);
        final FrameLayout preview = (FrameLayout) view.findViewById(R.id.camera_preview);
        preview.addView(mCameraPreview);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               preview.setVisibility(View.VISIBLE);
            }
        },550);
        screenGuide = (ImageView)view.findViewById(R.id.img_screen_guide);
        screenGuideContainer = view.findViewById(R.id.guideContainer);
        retakeContainer = view.findViewById(R.id.retake_container);

        ImageView cameraImage = (ImageView)view.findViewById(R.id.img_camera);
        travelImage = (ImageView)view.findViewById(R.id.img_travel);
        shoppingImage = (ImageView)view.findViewById(R.id.img_shopping);
        selectOptionImage = (ImageView)view.findViewById(R.id.img_select_option);

        ImageView retake = (ImageView)view.findViewById(R.id.img_retake);
        ImageView upload = (ImageView)view.findViewById(R.id.img_upload);

        retake.setOnClickListener(this);
        upload.setOnClickListener(this);
        cameraImage.setOnClickListener(this);
        travelImage.setOnClickListener(this);
        shoppingImage.setOnClickListener(this);
        selectOptionImage.setVisibility(View.GONE);

    }


    /**
     * Helper method to access the camera returns null if it cannot get the
     * camera or does not exist
     *
     * @return
     */
    private Camera getCameraInstance() {
        Camera camera = null;
        try {
            camera = Camera.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return camera;
    }
    private Bitmap getBitmap(File file) {

        Uri uri = Uri.fromFile(file);
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = context.getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = context.getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }


    Camera.PictureCallback mPicture1 = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile();
            if (pictureFile == null) {
                return;
            }
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            filePath = pictureFile.getAbsolutePath();
//            bitmap1 = getBitmap(pictureFile);
            screenGuideContainer.setVisibility(View.GONE);
            retakeContainer.setVisibility(View.VISIBLE);
        }
    };
    private void getWriteStoragePermission(){
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                    AppConstants.GET_WRITE_PERMISSION_RC);

        }else{
            Log.i(AppConstants.TAG,"");
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppConstants.GET_WRITE_PERMISSION_RC: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
        }
    }
    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"Saivian");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Saivian", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_camera:
                if(!isTravelSelected && !isShoppingSelected){
                    selectOptionImage.setVisibility(View.VISIBLE);
                }else{
                    try {
                        mCamera.takePicture(null, null, mPicture1);
                    }catch (Exception e){e.printStackTrace();}
                }
                break;
            case R.id.img_travel:
                selectOptionImage.setVisibility(View.GONE);
                isTravelSelected = true;
                isShoppingSelected = false;
                shoppingImage.setSelected(false);
                travelImage.setSelected(true);
                break;

            case R.id.img_shopping:
                selectOptionImage.setVisibility(View.GONE);
                isTravelSelected = false;
                isShoppingSelected = true;
                travelImage.setSelected(false);
                shoppingImage.setSelected(true);
                break;

            case R.id.img_retake:
                ((MainActivity)context).reloadFragment(this.getClass().getName(),false);
                break;

            case R.id.img_upload:
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.RECEIPT_IMAGE,filePath);
                if(isShoppingSelected) {
                    UploadShoppingFragment fragment = new UploadShoppingFragment();
                    fragment.setArguments(bundle);
                    ((MainActivity) context).loadFragment(fragment);
                }
                else if(isTravelSelected) {
                    UploadTravelFragment fragment = new UploadTravelFragment();
                    fragment.setArguments(bundle);
                    ((MainActivity) context).loadFragment(fragment);
                }
                break;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        ((MainActivity)context).hideBottomBar();
    }
    @Override
    public void onStop(){
        super.onStop();
        ((MainActivity)context).showBottomBar();
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        selectOptionImage = null;
        screenGuideContainer = null;
        context = null;
    }
}
