package saivian.txlabz.com.saivian;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pl.droidsonroids.gif.AnimationListener;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.Delegate;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;

public class SplashActivity extends AppCompatActivity implements Delegate{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_main);
        MainStorageUtility utility = MainStorageUtility.getInstance(this.getApplicationContext());
        utility.setDelegate(this);

        GifImageView gifImageView = (GifImageView)findViewById(R.id.gifImageView);
        gifImageView.setImageResource( R.drawable.splash);
        GifDrawable gifDrawable = (GifDrawable) gifImageView.getDrawable();
        gifDrawable.addAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationCompleted(int loopNumber) {
                Intent intent = null;
                if(StorageUtility.getDataFromPreferences(AppConstants.IS_USER_LOGGED_IN,"false",SplashActivity.this).equals("true"))
                    intent = new Intent(SplashActivity.this, MainActivity.class);
                else
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        if(StorageUtility.getDataFromPreferences(AppConstants.IS_USER_LOGGED_IN,"false",SplashActivity.this).equals("true"))
            utility.getDataInBackground();
    }

    @Override
    public void callback(String result) {

    }
}
