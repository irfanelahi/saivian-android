package saivian.txlabz.com.saivian.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import saivian.txlabz.com.saivian.models.CashBackCategory;
import saivian.txlabz.com.saivian.models.CashBackStore;
import saivian.txlabz.com.saivian.models.Location;
import saivian.txlabz.com.saivian.models.OpeningHour;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.adapters.CashBlockLocalItemAdapter;
import saivian.txlabz.com.saivian.models.CashBackLocalItem;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;

/**
 * Created by Fatima Siddique on 5/4/2016.
 */
public class CashBlockLocalItemFragment extends Fragment {
    Context context;
    ArrayList<CashBackStore>cashBackStoreArrayList;
    RecyclerView recyclerView;
    CashBlockLocalItemAdapter adapter;
    LinearLayoutManager linearLayoutManager;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_block_local_item, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        Bundle bundle = getArguments();
        CashBackCategory category = null;
        if(bundle != null){
            category = (CashBackCategory) bundle.getSerializable(AppConstants.CASH_BACK_CATEGORY);
        }
        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);

        getCallPermissions();
        MainStorageUtility utility = MainStorageUtility.getInstance(context.getApplicationContext());
        cashBackStoreArrayList = utility.getStoreByCategoryId(category.getId());
        adapter = new CashBlockLocalItemAdapter(context,cashBackStoreArrayList,this.getClass().getName(),CashBlockLocalItemFragment.this);
        recyclerView.setAdapter(adapter);
//        getCashBackDetailsFromServer(category.getId());
    }

    public void getCallPermissions(){
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.CALL_PHONE},
                    AppConstants.GET_CALL_PERMISSION_RC);

        }else{
//            makeCall();
        }

    }
    public void makeCall(String num){
        if(num != null && !num.equals("") && GeneralUtil.isValidPhoneNumber(num)) {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + num));
            startActivity(intent);
//            ((Activity) context).overridePendingTransition(R.anim.flip_out, R.anim.flip_in);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppConstants.GET_CALL_PERMISSION_RC: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    makeCall();
                } else {

                }
                return;
            }
        }
    }
    private void getCashBackDetailsFromServer(String categoryId){
        GeneralUtil.showLoadingDialog(context);
        if(cashBackStoreArrayList != null)
            cashBackStoreArrayList.clear();
        cashBackStoreArrayList = new ArrayList<CashBackStore>();
        Ion.with(this)
                .load(AppConstants.GET,AppConstants.FETCH_CASH_BACK_BY_CATEGORY+"?category_id="+ categoryId+"&customer_id="+StorageUtility.getLoginModel(context).getId())
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();
                Log.i(AppConstants.TAG,"result is : "+result);

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {

                                JSONArray storeDataArray = Json1.getJSONArray(AppConstants.STORES_LIST);


                                for(int j = 0; j < storeDataArray.length();j++){
                                    JSONObject storeObj = storeDataArray.getJSONObject(j);
                                    String storeId = storeObj.getString(AppConstants.STORE_ID);
                                    String firstName = storeObj.getString(AppConstants.FIRST_NAME);
                                    String lastName = storeObj.getString(AppConstants.LAST_NAME);
                                    String storeName = storeObj.getString(AppConstants.STORE_NAME);
                                    String description = storeObj.getString(AppConstants.DESCRIPTION);
                                    String email = storeObj.getString(AppConstants.EMAIL);
                                    String phoneNumber = storeObj.getString(AppConstants.PHONE);
                                    String webUrl = storeObj.getString(AppConstants.WEB_URL);
                                    String isFavorite = storeObj.getString(AppConstants.IS_FAVOURITE);
                                    String profileImage = storeObj.getString(AppConstants.PROFILE_IMAGE);
                                    String featuredImage = storeObj.getString(AppConstants.FEATURED_IMAGE);

                                    JSONObject openingHourJSON = storeObj.getJSONObject(AppConstants.OPENING_HOURS);

                                    String monday = openingHourJSON.getString(AppConstants.MONDAY);
                                    String tuesday = openingHourJSON.getString(AppConstants.TUESDAY);
                                    String wednesday = openingHourJSON.getString(AppConstants.WEDNESDAY);
                                    String thursday = openingHourJSON.getString(AppConstants.THURSDAY);
                                    String friday = openingHourJSON.getString(AppConstants.FRIDAY);
                                    String saturday = openingHourJSON.getString(AppConstants.SATURDAY);
                                    String sunday = openingHourJSON.getString(AppConstants.SUNDAY);

                                    JSONObject locationJSON = storeObj.getJSONObject(AppConstants.LOCATION);
                                    String address = locationJSON.getString(AppConstants.ADDRESS);
                                    String city = locationJSON.getString(AppConstants.CITY);
                                    String state = locationJSON.getString(AppConstants.STATE);
                                    String zip = locationJSON.getString(AppConstants.ZIP);
                                    String country = locationJSON.getString(AppConstants.COUNTRY);
                                    String latitude = locationJSON.getString(AppConstants.LATITUDE);
                                    String longitude = locationJSON.getString(AppConstants.LONGITUDE);

                                    OpeningHour openingHour = new OpeningHour(monday, tuesday, wednesday, thursday, friday, saturday, sunday);
                                    Location location = new Location(address,city, state, zip, country, latitude,longitude);

                                    cashBackStoreArrayList.add(new CashBackStore(storeId,firstName,lastName, storeName,description,email,phoneNumber, webUrl,isFavorite,profileImage, featuredImage, openingHour, location));
                                }
                            adapter = new CashBlockLocalItemAdapter(context,cashBackStoreArrayList,this.getClass().getName(),CashBlockLocalItemFragment.this);
                            recyclerView.setAdapter(adapter);
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }

    public void addFavorite(final CashBackStore store, final int position){

        Ion.with(this)
                .load(AppConstants.POST,AppConstants.ADD_FAVORITE_STORE)
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .setBodyParameter(AppConstants.STORE_ID,store.getStoreId())
                .setBodyParameter(AppConstants.CUSTOMER_ID,StorageUtility.getLoginModel(context).getId())
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.i(AppConstants.TAG,"result is : "+result);

                if (e == null) {
                    try {
                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            MainStorageUtility utility = MainStorageUtility.getInstance(context);
                            if(store.getIsFavorite().equals("1")) {
                                store.setIsFavorite("0");
                                utility.removeStoreFromFavorite(store);
                            }
                            else {
                                store.setIsFavorite("1");
                                utility.addStoreToFavorite(store);
                            }
                            adapter.notifyItemChanged(position);
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }
    public void scrollToPosition(int position){
        linearLayoutManager.scrollToPositionWithOffset(position,5);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if(cashBackStoreArrayList != null)
//            cashBackStoreArrayList.clear();
        cashBackStoreArrayList = null;
        linearLayoutManager = null;
        recyclerView = null;
        adapter = null;
        context = null;
    }
}
