package saivian.txlabz.com.saivian.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collections;
import java.util.List;

import saivian.txlabz.com.saivian.models.InstantOffer;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.R;

/**
 * Created by Fatima Siddique on 3/20/2016.
 */
public class InstantSavingItemAdapter extends RecyclerView.Adapter<InstantSavingItemAdapter.ViewHolder> {
    List<InstantOffer> data = Collections.emptyList();

    private LayoutInflater inflater;
    private Context context;
    ImageLoader imageLoader;
    DisplayImageOptions options;


    public InstantSavingItemAdapter(Context context, List<InstantOffer> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        imageLoader = ImageLoader.getInstance();
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.progress)
                .showImageForEmptyUri(R.drawable.progress)
                .showImageOnFail(R.drawable.progress).cacheInMemory(true)
                .cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();

    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.layout_instant_saving_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        imageLoader.displayImage(data.get(position).getLogoUrl(),holder.imageView,options);
        String desc = data.get(position).getOfferStore().getDescription();
        if(desc != null && !desc.equals("") && !desc.equals("null"))
            holder.description.setText(desc);
        else
            holder.description.setText("No Description Provided");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView description;
        RelativeLayout parentPanel;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.img_instant_saving);
            description = (TextView)itemView.findViewById(R.id.instantsaving_desc);
            parentPanel = (RelativeLayout)itemView.findViewById(R.id.parentPanel);
        }
    }
}