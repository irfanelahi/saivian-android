package saivian.txlabz.com.saivian.models;

/**
 * Created by Fatima Siddique on 6/17/2016.
 */
public class InstantGeoLocation {
    String latitude;
    String longitude;

    public InstantGeoLocation(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
