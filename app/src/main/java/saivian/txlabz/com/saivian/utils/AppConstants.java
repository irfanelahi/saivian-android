package saivian.txlabz.com.saivian.utils;

/**
 * Created by Fatima Siddique on 5/4/2016.
 */
public class AppConstants {

    public static final String BASE_URL                       = "http://www.saivian-squad.com/";
    public static final String INSTANT_SAVING_BASE_URL        = "https://offer-demo.adcrws.com";
    public static final String SIGN_UP_URL                    = BASE_URL + "customer/signup";
    public static final String SIGN_IN_URL                    = BASE_URL + "customer/login";
    public static final String CUSTOMER_PROFILE_URL           = BASE_URL + "customer/customerProfile";
    public static final String RESET_PASSWORD                 = BASE_URL + "customer/forgotPassword";
    public static final String FETCH_RECEIPTS                 = BASE_URL + "receipts/fetch";
    public static final String GET_CASH_BACK_AMOUNT           = BASE_URL + "customer/cashbackAmount";
    public static final String EDIT_PROFILE                   = BASE_URL + "customer/editProfile";
    public static final String FETCH_CASH_BACK_LOCAL          = BASE_URL + "merchant/fetchCashBackLocal";
    public static final String GET_MERCHANT_CATEGORIES        = BASE_URL + "merchant/categories";
    public static final String FETCH_CASH_BACK_BY_CATEGORY    = BASE_URL + "merchant/fetchCashBackByCategory";
    public static final String FETCH_ALL_STORES               = BASE_URL + "stores/fetchAll";
    public static final String FETCH_USER_TOP_10_STORES       = BASE_URL + "stores/storesList";
    public static final String ADD_STORE                      = BASE_URL + "stores/add";
    public static final String FETCH_PAYMENT_INFO             = BASE_URL + "customer/fetchPaymentInfo";
    public static final String ADD_PAYMENT_INFO               = BASE_URL + "customer/addPaymentInfo";
    public static final String SUBMIT_MARKET_RESEARCH         = BASE_URL + "/customer/submitMarketResearch";
    public static final String UPLOAD_RECEIPT                 = BASE_URL + "receipts/upload";
    public static final String ADD_FAVORITE_STORE             = BASE_URL + "stores/addFavouriteStore";

    public static final String GET_INSTANT_OFFERS             = INSTANT_SAVING_BASE_URL + "/v1/offers";

    public static final String TAG = "Savian";
    public static final String INDEX = "index";
    public static final String OFFERS = "offers" ;
    public static final int REQUEST_CODE = 240;
    public static final String SHARE_URL = "share_url";
    public static final String SAIVIAN_ID = "saivian_id";


    public static enum CASH_BACK_STATE {
        NORMAL("Normal", 0),
        ITEM_SELECTED("ItemSelected", 1),
        HOURS_SELECTED("HoursSelected", 2),
        PHONE_SELECTED("PhoneSelected", 3),
        MAP_SELECTED("MapSelected", 4);

        private String stringValue;
        private int intValue;
        private CASH_BACK_STATE(String toString, int value) {
            stringValue = toString;
            intValue = value;
        }

        @Override
        public String toString() {
            return stringValue;
        }

        public int getIntValue() {
            return intValue;
        }
    }
    public static enum TOP10_STATE {
        NORMAL("Normal", 0),
        ITEM_EXPAND("ItemExpand", 1),
        ITEM_COLLAPSE("ItemCollapse", 2);
        private String stringValue;
        private int intValue;
        private TOP10_STATE(String toString, int value) {
            stringValue = toString;
            intValue = value;
        }

        @Override
        public String toString() {
            return stringValue;
        }

        public int getIntValue() {
            return intValue;
        }
    }
    public static final int GET_CALL_PERMISSION_RC = 255;
    public static final int GET_CAMERA_PERMISSION_RC = 254;
    public static final int GET_WRITE_PERMISSION_RC = 253;
    public static final int GET_READ_PERMISSION_RC = 251;
    public static final int REQUEST_GALLERY = 252;


    //Web Services
    public static final String HEADER_KEY = "X-API-KEY";
    public static final String HEADER_VALUE = "123456";

    public static final String POST = "POST";
    public static final String GET  = "GET";

    public static final String STATUS = "status";
    public static final String MESSAGE = "message";
    public static final int STATUS_SUCCESS = 1 ;

    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME  = "last_name";
    public static final String EMAIL = "email";
    public static final String REFERRAL_ID = "referral_id";
    public static final String PASSWORD = "password";
    public static final String DOB = "dob";
    public static final String CUSTOMER_DATA = "customer_data";

    public static final String ID = "id";
    public static final String PROFILE_IMAGE  = "profile_image";
    public static final String GENDER = "gender";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String ZIP = "zip";
    public static final String COUNTRY = "country";
    public static final String IS_VERIFIED = "is_verified";
    public static final String IS_ACTIVE = "is_active";
    public static final String CREATED_DATE = "created_date";

    public static final String SESSION_TOKEN = "session_token";
    public static final String IS_USER_LOGGED_IN = "isUserLoggedIn";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String PHONE = "phone_no";
    public static final String MONTH_CASH_BACK = "month_cashback";
    public static final String YEAR_CASH_BACK = "year_cashback";
    public static final String CASH_BACK_DATA = "cash_back_data";

    public static final String CATEGORY_NAME = "category_name";
    public static final String DESCRIPTION = "description";
    public static final String STORES_LIST = "stores_list";
    public static final String STORE_ID = "store_id";
    public static final String STORE_NAME = "store_name";
    public static final String WEB_URL = "web_url";
    public static final String FEATURED_IMAGE = "featured_image";
    public static final String OPENING_HOURS = "opening_hours";

    public static final String MONDAY = "monday";
    public static final String TUESDAY = "tuesday";
    public static final String WEDNESDAY = "wednesday";
    public static final String THURSDAY = "thursday";
    public static final String FRIDAY = "friday";
    public static final String SATURDAY = "saturday";
    public static final String SUNDAY = "sunday";

    public static final String LOCATION = "location";
    public static final String ADDRESS = "address";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String CATEGORIES = "categories";
    public static final String CASH_BACK_CATEGORY = "cash_back_category";
    public static final String STORE_ADDRESS = "store_address";
    public static final String STORE_DATA = "store_data";
    public static final String PAYMENT_INFO = "payment_info";
    public static final String CARD_HOLDER_NAME = "card_holder_name";
    public static final String CARD_TYPE = "card_type";
    public static final String CARD_NUMBER = "card_number";
    public static final String ANSWER = "answer";
    public static final String RECEIPTS = "receipts";
    public static final String RECEIPT_TYPE = "receipt_type";
    public static final String PURCHASED_DATE = "purchased_date";
    public static final String SUBTOTAL_AMOUNT = "subtotal_amount" ;
    public static final String RECEIPT_IMAGE = "receipt_image";
    public static final String UPLOADED_DATE = "uploaded_date";
    public static final String RECEIPT_ID = "receipt_id";
    public static final String SHOPPING = "SHOPPING";
    public static final String TRAVEL = "TRAVEL";
    public static final String THIRD_PARTY = "third_party";
    public static final String IS_FAVOURITE = "is_favourite";
    public static final String SHOULD_RUN_SPLASH = "should_run_splash";
    public static final String IMAGE = "image";
    public static final String FAVORITE_STORES = "favourite_stores";
    public static final String TOP_10_STORES_LIST = "top_ten_stores";
    public static final String CVC = "cvc";
    public static final String EXPIRY_MONTH = "expiry_month";
    public static final String EXPIRY_YEAR = "expiry_year";
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String RECEIPTS_LIST = "receipts_list";
    public static final String ALl_STORES_LIST = "all_stores_list";
    public static final String RECEIPT = "receipt";


    public static final String SEARCH_DISTANCE = "search_distance";
    public static final String OFFER_KEY = "offer_key";
    public static final String OFFER_GROUP_KEY = "offer_group_key";
    public static final String TITLE = "title";
    public static final String STARTED_ON = "started_on";
    public static final String EXPIRES_ON = "expires_on";
    public static final String TERMS_OF_USE = "terms_of_use";
    public static final String LOGO_URL = "logo_url";
    public static final String OFFER_PHOTO_URL = "offer_photo_url";
    public static final String NATIONAL_OFFER = "national_offer";
    public static final String USES_PER_PERIOD = "uses_per_period";
    public static final String OFFER_SET = "offer_set";
    public static final String SAVING_AMOUNT = "savings_amount";
    public static final String MINIMUM_PURCHASE = "minimum_purchase";
    public static final String MAXIMUM_AWARD = "maximum_award";
    public static final String OFFER_VALUE = "offer_value";
    public static final String DISCOUNT_TYPE = "discount_type";
    public static final String DISCOUNT_VALUE = "discount_value";
    public static final String PROMOTION_CODE = "promotion_code";
    public static final String REDEMPTION_METHODS = "redemption_methods";
    public static final String CATEGORY_KEY = "category_key";
    public static final String CATEGORY_PARENT_KEY = "category_parent_key";
    public static final String CATEGORY_PARENT_NAME = "category_parent_name";
    public static final String CATEGORY_TYPE = "category_type";

    public static final String OFFER_STORE = "offer_store";
    public static final String NAME = "name";
    public static final String STORE_KEY = "store_key";
    public static final String PHYSICAL_LOCATION = "physical_location";
    public static final String LOCATION_NAME = "location_name";
    public static final String WEB_ADDRESS = "web_address";
    public static final String ONLINE_EXCLUSIVE = "online_exclusive";
    public static final String LOCATION_KEY = "location_key";
    public static final String POSTAL_CODE = "postal_code";
    public static final String country = "country";
    public static final String STREET_ADDRESS = "street_address";
    public static final String EXTENDED_STREET_ADDRESS = "extended_street_address";
    public static final String CITY_LOCALITY = "city_locality";
    public static final String STATE_REGION = "state_region";
    public static final String GEOLOCATION_ARRAY = "geolocation_array";
    public static final String GEOLOCATION = "geolocation";
    public static final String LAT = "lat";
    public static final String LON = "lon";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String FACETS = "facets";
    public static final String LOCATION_KEYWORDS = "location_keywords";
    public static final String LINKS = "links";
    public static final String SHOW_OFFER = "show_offer";
    public static final String SHOW_STORE = "show_store";
    public static final String SHOW_LOCATION = "show_location";
    public static final String FIND_OFFERS_AT_THIS_STORE = "find_offers_at_this_store";
    public static final String FIND_OFFERS_AT_THIS_LOCATION = "find_offers_at_this_location";
    public static final String FIND_OFFERS_AT_MORE_LOCATIONS = "find_this_offer_at_more_locations";
    public static final String FIND_LOCATIONS_WITH_THIS_OFFER = "find_locations_with_this_offer";
    public static final String FIND_LOCATIONS_FOR_THIS_STORE = "find_locations_for_this_store";
    public static final String FIND_OFFER_FOR_THIS_STORE = "find_offers_for_this_store";

    public static final String CAN_EDIT = "can_edit";
}
