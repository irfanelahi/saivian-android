package saivian.txlabz.com.saivian;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONObject;

import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener{
    EditText email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        init();
    }

    private void init() {
        email = (EditText)findViewById(R.id.emailEditText);
        Button resetPassword = (Button)findViewById(R.id.resetPasswordButton);
        resetPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.resetPasswordButton:
                if(dataValid())
                sendResetPasswordRequest();
                break;
        }
    }

    private boolean dataValid() {
        boolean result = true;
        if(!GeneralUtil.isNetworkAvailable(this)){
            GeneralUtil.showAlert(this,getString(R.string.internet_not_connected));
            result = false;
        }else if(email.getText().toString().equals("")){
            GeneralUtil.showAlert(this,getString(R.string.error_email));
            result = false;
        }

        return result;
    }

    private void sendResetPasswordRequest(){
        GeneralUtil.showLoadingDialog(this);
        Ion.with(this)
                .load(AppConstants.RESET_PASSWORD)
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .setBodyParameter(AppConstants.EMAIL, email.getText().toString())
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            GeneralUtil.showAlert(ForgotPasswordActivity.this,Json1.getString(AppConstants.MESSAGE));
                        } else {
                            GeneralUtil.showAlert(ForgotPasswordActivity.this, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(ForgotPasswordActivity.this, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(ForgotPasswordActivity.this, e);
                }

            }
        });
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
