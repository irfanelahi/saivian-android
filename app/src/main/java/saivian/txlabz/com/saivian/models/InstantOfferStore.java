package saivian.txlabz.com.saivian.models;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 6/17/2016.
 */
public class InstantOfferStore {

    String name;
    String description;
    String storeKey;
    InstantPhysicalLocation location;

    public InstantOfferStore(String name, String description, String storeKey, InstantPhysicalLocation location) {
        this.name = name;
        this.description = description;
        this.storeKey = storeKey;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStoreKey() {
        return storeKey;
    }

    public void setStoreKey(String storeKey) {
        this.storeKey = storeKey;
    }

    public InstantPhysicalLocation getLocation() {
        return location;
    }

    public void setLocation(InstantPhysicalLocation location) {
        this.location = location;
    }

}
