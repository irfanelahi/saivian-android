package saivian.txlabz.com.saivian.models;

import java.util.ArrayList;

/**
 * Created by Fatima Siddique on 6/17/2016.
 */
public class InstantOffer {

    String searchDistance;
    String offerKey;
    String offerGroupKey;
    String title;
    String startedOn;
    String expiresOn;
    String termsOfUse;
    String logoUrl;
    String offerPhotoUrl;
    String nationalOffer;
    String usesPerPeriod;
    String offerSet;
    String savingAmount;
    String minimumPurchase;
    String maximumAward;
    String offerValue;
    String discountType;
    String discountValue;
    String promotionCode;
    ArrayList<String> redemptionMethods;
    ArrayList<String> facets;
    ArrayList<String> locationKeywords;
    InstantLinks links;
    InstantOfferStore offerStore;

    public InstantOffer(String searchDistance,String offerKey, String offerGroupKey, String title, String startedOn, String expiresOn, String termsOfUse, String logoUrl, String offerPhotoUrl, String nationalOffer, String usesPerPeriod, String offerSet, String savingAmount, String minimumPurchase, String maximumAward, String offerValue, String discountType, String discountValue, String promotionCode, ArrayList<String> redemptionMethods, ArrayList<String> facets, ArrayList<String> locationKeywords, InstantLinks links, InstantOfferStore store) {
        this.searchDistance = searchDistance;
        this.offerKey = offerKey;
        this.offerGroupKey = offerGroupKey;
        this.title = title;
        this.startedOn = startedOn;
        this.expiresOn = expiresOn;
        this.termsOfUse = termsOfUse;
        this.logoUrl = logoUrl;
        this.offerPhotoUrl = offerPhotoUrl;
        this.nationalOffer = nationalOffer;
        this.usesPerPeriod = usesPerPeriod;
        this.offerSet = offerSet;
        this.savingAmount = savingAmount;
        this.minimumPurchase = minimumPurchase;
        this.maximumAward = maximumAward;
        this.offerValue = offerValue;
        this.discountType = discountType;
        this.discountValue = discountValue;
        this.promotionCode = promotionCode;
        this.redemptionMethods = redemptionMethods;
        this.facets = facets;
        this.locationKeywords = locationKeywords;
        this.links = links;
        this.offerStore = store;
    }

    public String getSearchDistance() {
        return searchDistance;
    }

    public void setSearchDistance(String searchDistance) {
        this.searchDistance = searchDistance;
    }

    public String getOfferGroupKey() {
        return offerGroupKey;
    }

    public void setOfferGroupKey(String offerGroupKey) {
        this.offerGroupKey = offerGroupKey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(String startedOn) {
        this.startedOn = startedOn;
    }

    public String getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(String expiresOn) {
        this.expiresOn = expiresOn;
    }

    public String getTermsOfUse() {
        return termsOfUse;
    }

    public void setTermsOfUse(String termsOfUse) {
        this.termsOfUse = termsOfUse;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getOfferPhotoUrl() {
        return offerPhotoUrl;
    }

    public void setOfferPhotoUrl(String offerPhotoUrl) {
        this.offerPhotoUrl = offerPhotoUrl;
    }

    public String getNationalOffer() {
        return nationalOffer;
    }

    public void setNationalOffer(String nationalOffer) {
        this.nationalOffer = nationalOffer;
    }

    public String getUsesPerPeriod() {
        return usesPerPeriod;
    }

    public void setUsesPerPeriod(String usesPerPeriod) {
        this.usesPerPeriod = usesPerPeriod;
    }

    public String getOfferSet() {
        return offerSet;
    }

    public void setOfferSet(String offerSet) {
        this.offerSet = offerSet;
    }

    public String getSavingAmount() {
        return savingAmount;
    }

    public void setSavingAmount(String savingAmount) {
        this.savingAmount = savingAmount;
    }

    public String getMinimumPurchase() {
        return minimumPurchase;
    }

    public void setMinimumPurchase(String minimumPurchase) {
        this.minimumPurchase = minimumPurchase;
    }

    public String getMaximumAward() {
        return maximumAward;
    }

    public void setMaximumAward(String maximumAward) {
        this.maximumAward = maximumAward;
    }

    public String getOfferValue() {
        return offerValue;
    }

    public void setOfferValue(String offerValue) {
        this.offerValue = offerValue;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public ArrayList<String> getRedemptionMethods() {
        return redemptionMethods;
    }

    public void setRedemptionMethods(ArrayList<String> redemptionMethods) {
        this.redemptionMethods = redemptionMethods;
    }

    public ArrayList<String> getFacets() {
        return facets;
    }

    public void setFacets(ArrayList<String> facets) {
        this.facets = facets;
    }

    public ArrayList<String> getLocationKeywords() {
        return locationKeywords;
    }

    public void setLocationKeywords(ArrayList<String> locationKeywords) {
        this.locationKeywords = locationKeywords;
    }

    public InstantLinks getLinks() {
        return links;
    }

    public void setLinks(InstantLinks links) {
        this.links = links;
    }

    public String getOfferKey() {
        return offerKey;
    }

    public void setOfferKey(String offerKey) {
        this.offerKey = offerKey;
    }

    public InstantOfferStore getOfferStore() {
        return offerStore;
    }

    public void setOfferStore(InstantOfferStore offerStore) {
        this.offerStore = offerStore;
    }
}
