package saivian.txlabz.com.saivian.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import saivian.txlabz.com.saivian.MainActivity;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.adapters.ReceiptsListAdapter;
import saivian.txlabz.com.saivian.models.Receipt;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;

/**
 * Created by Fatima Siddique on 5/6/2016.
 */
public class ReceiptsFragment extends Fragment {
    Context context;
    ArrayList<Receipt> receiptArrayList;
    ArrayList<Receipt> filteredArrayList;
    RecyclerView recyclerView;
    Spinner month;
    EditText search;
    TextView daysOfMonth;
    HashMap<String,Calendar> calendarHashMap;
    View activityRootView;
    boolean isFragmentPaused = false;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        isFragmentPaused = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_receipts, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        month = (Spinner)view.findViewById(R.id.spinner_month);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        search = (EditText)view.findViewById(R.id.et_search_receipt);
        recyclerView.setLayoutManager(linearLayoutManager);
        daysOfMonth = (TextView)view.findViewById(R.id.txt_month_days);
        activityRootView = ((MainActivity)context).getRootView();
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(listener);
        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

                if(arg0.length() == 0) {
                    if(filteredArrayList != null) {
                        if(filteredArrayList.size() !=  0) {
                            ReceiptsListAdapter adapter = new ReceiptsListAdapter(context,filteredArrayList);
                            recyclerView.setAdapter(adapter);
                        }
                    }
                }
                ArrayList<Receipt> filteredContacts = checkFilter(arg0);
                if(filteredContacts != null) {
                    ReceiptsListAdapter adapter = new ReceiptsListAdapter(context,filteredContacts);
                    recyclerView.setAdapter(adapter);                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });
        setSpinners();
        MainStorageUtility utility = MainStorageUtility.getInstance(context);
        receiptArrayList = utility.getReceiptArrayList();
        setupMonth();
//        fetchReceiptsDataFromServer();
    }
    @Override
    public void onResume(){
        super.onResume();
        context = getActivity();
        isFragmentPaused = false;
    }
    ViewTreeObserver.OnGlobalLayoutListener listener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            if(context == null)
                return;
            if(isFragmentPaused)
                return;
            Rect r = new Rect();
            if(activityRootView != null)
                activityRootView.getWindowVisibleDisplayFrame(r);
            else{
                activityRootView = ((MainActivity)context).getRootView();
            }
            int screenHeight = activityRootView.getRootView().getHeight();

            int keypadHeight = screenHeight - r.bottom;

            Log.d(AppConstants.TAG, "keypadHeight = " + keypadHeight);
            if (keypadHeight > screenHeight * 0.15) {
                ((MainActivity)context).hideBottomBar();
            }
            else {
                ((MainActivity)context).showBottomBar();
            }
        }
    };
    private void fetchReceiptsDataFromServer(){
        if(receiptArrayList != null)
            receiptArrayList.clear();
        receiptArrayList = new ArrayList<>();
        GeneralUtil.showLoadingDialog(context);
        Ion.with(this)
                .load(AppConstants.GET,AppConstants.FETCH_RECEIPTS+"?customer_id="+ StorageUtility.getLoginModel(context).getId())
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();

                if (e == null) {
                    try {

                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            JSONArray receiptsArray = Json1.getJSONArray(AppConstants.RECEIPTS);
                            for(int i = 0; i<receiptsArray.length(); i++){
                                JSONObject receiptsObject = receiptsArray.getJSONObject(i);
                                String id = receiptsObject.getString(AppConstants.RECEIPT_ID);
                                String receiptType = receiptsObject.getString(AppConstants.RECEIPT_TYPE);
                                String storeName = receiptsObject.getString(AppConstants.STORE_NAME);
                                String purchasedDate = receiptsObject.getString(AppConstants.PURCHASED_DATE);
                                String subTotal = receiptsObject.getString(AppConstants.SUBTOTAL_AMOUNT);
                                String receiptImage = receiptsObject.getString(AppConstants.RECEIPT_IMAGE);
                                String uploadedDate = receiptsObject.getString(AppConstants.UPLOADED_DATE);
                                //2016-05-09 03:38:29
                                SimpleDateFormat inputFormat = null;
                                Date date = null;
                                try{
                                    inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    date = inputFormat.parse(purchasedDate);
                                }catch (Exception ex){
                                    inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    date = inputFormat.parse(purchasedDate);
                                }
                                SimpleDateFormat outputFormat = new SimpleDateFormat("MMMM");
                                String outputString = outputFormat.format(date) + " "+ String.valueOf(date.getYear()+1900);
                                receiptArrayList.add(new Receipt(id,receiptType,storeName,purchasedDate,subTotal,receiptImage,uploadedDate,outputString));
                            }
                            setupMonth();
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }

    private void setSpinners(){
        calendarHashMap = new HashMap<>();
        List<String> listMonths = new ArrayList<>();
        int index = 0;
        for(int i = 0; i < 12; i++){
            Calendar calendar2 = Calendar.getInstance();
            calendar2.add(Calendar.MONTH, index);
            String key = calendar2.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH ) +" "+ calendar2.get(Calendar.YEAR);
            listMonths.add(key);
            calendarHashMap.put(key,calendar2);
            index--;
        }
        ArrayAdapter countAdapter = new ArrayAdapter(context, R.layout.spinner_dropdown_item,listMonths);
        countAdapter.setDropDownViewResource(R.layout.custom_spinner_item);

        month.setAdapter(countAdapter);
        month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                setupMonth();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }
    private void setupMonth(){
        Calendar calendar1 = calendarHashMap.get(month.getSelectedItem().toString());
        String monthName = calendar1.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH );
        String lastDay = String.valueOf(calendar1.getActualMaximum(Calendar.DAY_OF_MONTH));
        String finalStr = monthName + " 1st - "+lastDay+"st " + monthName+" "+calendar1.get(Calendar.YEAR);
        daysOfMonth.setText(finalStr);
        getDataBasedOnMonth(month.getSelectedItem().toString());
    }
    private void getDataBasedOnMonth(String month){
        if(filteredArrayList != null)
            filteredArrayList.clear();
        filteredArrayList = new ArrayList<>();
        for(int i = 0;i<receiptArrayList.size();i++){
            if(receiptArrayList.get(i).getMonth().equals(month)){
                filteredArrayList.add(receiptArrayList.get(i));
            }
        }
        ReceiptsListAdapter adapter = new ReceiptsListAdapter(context,filteredArrayList);
        recyclerView.setAdapter(adapter);
    }
    private ArrayList<Receipt> checkFilter(CharSequence arg0) {
        ArrayList<Receipt> filteredContacts = new ArrayList<Receipt>();
        ArrayList<Receipt> list = filteredArrayList;

        for(int i = 0 ; i < list.size(); i++) {
            Receipt receipt = list.get(i);
            String storeName = receipt.getStoreName();
            if (storeName.toLowerCase().contains(arg0.toString().toLowerCase())) {
                filteredContacts.add(receipt);
            }
        }

        return filteredContacts;
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        isFragmentPaused = true;
        if(activityRootView != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                activityRootView.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
            } else {
                activityRootView.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
            }
            activityRootView = null;
        }
        activityRootView = null;
        context = null;
    }
    @Override
    public void onStop() {
        super.onStop();
        isFragmentPaused = true;
    }

}
