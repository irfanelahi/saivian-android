package saivian.txlabz.com.saivian.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.adapters.FavoriteStoreItemAdapter;
import saivian.txlabz.com.saivian.adapters.Top10StoreItemAdapter;
import saivian.txlabz.com.saivian.models.CashBackStore;
import saivian.txlabz.com.saivian.models.Store;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.MainStorageUtility;
import saivian.txlabz.com.saivian.utils.StorageUtility;

/**
 * Created by Fatima Siddique on 5/5/2016.
 */
public class FavoriteStoresFragment extends Fragment implements View.OnClickListener{
    Context context;
    ArrayList <Store> storeListItems;
    ArrayList<CashBackStore> favStoreArrayList;
    RecyclerView recyclerView;
    FavoriteStoreItemAdapter adapter;
    LinearLayoutManager linearLayoutManager;

    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_top_10_stores, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        View addStoreContainer = view.findViewById(R.id.btn_add_store_container);
        addStoreContainer.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(context);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);
        Button addStore = (Button)view.findViewById(R.id.btn_add_store);
        addStore.setOnClickListener(this);

        MainStorageUtility utility = MainStorageUtility.getInstance(context);
        storeListItems = utility.getFetchAllStoreArrayList();
        favStoreArrayList = utility.getFavoriteStoresArrayList();
        adapter = new FavoriteStoreItemAdapter(context,favStoreArrayList,storeListItems, FavoriteStoresFragment.this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        adapter = null;
        linearLayoutManager = null;
        recyclerView = null;
    }
    @Override
    public void onDetach(){
        super.onDetach();
        context = null;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
        }
    }
    public void addFavorite(final CashBackStore store, final int position){

        Ion.with(this)
                .load(AppConstants.POST,AppConstants.ADD_FAVORITE_STORE)
                .setHeader(AppConstants.HEADER_KEY,AppConstants.HEADER_VALUE)
                .setBodyParameter(AppConstants.STORE_ID,store.getStoreId())
                .setBodyParameter(AppConstants.CUSTOMER_ID,StorageUtility.getLoginModel(context).getId())
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.i(AppConstants.TAG,"result is : "+result);

                if (e == null) {
                    try {
                        JSONObject Json1 = new JSONObject(result);
                        int status = Json1.getInt(AppConstants.STATUS);
                        if (status == AppConstants.STATUS_SUCCESS) {
                            MainStorageUtility utility = MainStorageUtility.getInstance(context);
                            if(store.getIsFavorite().equals("1")) {
                                store.setIsFavorite("0");
                                utility.removeStoreFromFavorite(store);
                            }
                            else {
                                store.setIsFavorite("1");
                                utility.addStoreToFavorite(store);
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            GeneralUtil.showAlert(context, Json1.getString(AppConstants.MESSAGE));
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }
    public void makeCall(String num){
        if(num != null && !num.equals("") && GeneralUtil.isValidPhoneNumber(num)) {

            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + num));
            startActivity(intent);
//            ((Activity) context).overridePendingTransition(R.anim.flip_out, R.anim.flip_in);
        }
    }
    public void scrollToPosition(int position){
        linearLayoutManager.scrollToPositionWithOffset(position,0);
    }
}
