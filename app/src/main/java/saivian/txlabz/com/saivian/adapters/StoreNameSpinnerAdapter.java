package saivian.txlabz.com.saivian.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.List;

import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.models.Store;

public class StoreNameSpinnerAdapter extends BaseAdapter {

	private Context context;
	List<Store> stores;
	LayoutInflater inflater;

	public StoreNameSpinnerAdapter(Context context, List<Store> stores) {
		super();
		this.context = context;
		this.stores = stores;
		inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return stores.size();
	}

	@Override
	public Store getItem(int arg0) {
		return stores.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	/**
	 * Return row for each country
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View cellView = convertView;
		Cell cell;
		Store store = stores.get(position);

		if (convertView == null) {
			cell = new Cell();
			cellView = inflater.inflate(R.layout.spinner_dropdown_item, null);
			cell.textView = (TextView) cellView.findViewById(R.id.spinner_textView);
			cellView.setTag(cell);
		} else {
			cell = (Cell) cellView.getTag();
		}

		cell.textView.setText(store.getStoreName());

		return cellView;
	}

	/**
	 * Holder for the cell
	 * 
	 */
	static class Cell {
		public TextView textView;
	}

}