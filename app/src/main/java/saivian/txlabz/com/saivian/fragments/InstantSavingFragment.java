package saivian.txlabz.com.saivian.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.models.InstantCategory;
import saivian.txlabz.com.saivian.models.InstantGeoLocation;
import saivian.txlabz.com.saivian.models.InstantLinks;
import saivian.txlabz.com.saivian.models.InstantOffer;
import saivian.txlabz.com.saivian.models.InstantOfferStore;
import saivian.txlabz.com.saivian.models.InstantPhysicalLocation;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.StorageUtility;

/**
 * Created by Fatima Siddique on 5/4/2016.
 */
public class InstantSavingFragment extends Fragment implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    HashMap<String, InstantCategory> categoryMap;
    Context context;
    ArrayList<InstantCategory> categoryNamesList;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cash_back_local, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
//        setupViewPager();

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);


    }

    @Override
    public void onResume() {
        super.onResume();
        if(mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    protected void getPermission() {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    AppConstants.REQUEST_CODE);
        } else {
            statusCheck();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppConstants.REQUEST_CODE: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    statusCheck();
                } else {

                }
                return;
            }
        }
    }
    public void statusCheck()
    {
        final LocationManager manager = (LocationManager) context.getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }else
            connect();


    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,  final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();

    }

    private void connect() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            GeneralUtil.showAlert(context,"Unable to fetch location at the moment, Please make sure device location is on");
        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

            getCashBackDetailsFromServer();
//            Toast.makeText(context, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
        }
    }
    /**
     * If connected get lat and long
     *
     */
    @Override
    public void onConnected(Bundle bundle) {
        getPermission();
    }


    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
            /*
             * Google Play services can resolve some errors it detects.
             * If the error has a resolution, try sending an Intent to
             * start a Google Play services activity that can resolve
             * error.
             */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult((Activity) context, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                    /*
                     * Thrown if Google Play services canceled the original
                     * PendingIntent
                     */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
                /*
                 * If no resolution is available, display a dialog to the
                 * user with the error.
                 */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * If locationChanges change lat and long
     *
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

//        Toast.makeText(context, currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
    }

    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        if(categoryMap != null) {
            for (InstantCategory value : categoryMap.values()) {
                adapter.addFragment(InstantSavingItemFragment.getInstance(value.getOfferArrayList()), value.getCategoryName());
            }
        }
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    @Override
    public void onClick(View view) {

    }
    private void getCashBackDetailsFromServer(){
        GeneralUtil.showLoadingDialog(context);
        if(categoryMap != null) {
            categoryMap.clear();
            categoryNamesList.clear();
        }
        categoryMap = new HashMap<>();
        categoryNamesList = new ArrayList<>();
        Ion.with(this)
                .load(AppConstants.GET,AppConstants.GET_INSTANT_OFFERS+"?lat="+currentLatitude+"&lon="+currentLatitude+"&access_token=1842f8c32dbc7530cd6faca1b0025c8ee8ffa5338b4b5ce5b4855790dbed2e6b&member_key="+ StorageUtility.getLoginModel(context).getId())
                .asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                GeneralUtil.dismissLoadingDialog();
                Log.i(AppConstants.TAG,"result is : "+result);

                if (e == null) {
                    try {

                        JSONObject jsonObject = new JSONObject(result);
                        if(jsonObject.has(AppConstants.OFFERS)){
                            JSONArray offersArray = jsonObject.getJSONArray(AppConstants.OFFERS);
                            for(int i = 0; i < offersArray.length(); i++) {
                                JSONObject offersObject = offersArray.getJSONObject(i);
                                String searchDistance = offersObject.getString(AppConstants.SEARCH_DISTANCE);
                                String offerKey = offersObject.getString(AppConstants.OFFER_KEY);
                                String offerGroupKey = offersObject.getString(AppConstants.OFFER_GROUP_KEY);
                                String title = offersObject.getString(AppConstants.TITLE);
                                String startedOn = offersObject.getString(AppConstants.STARTED_ON);
                                String expiresOn = offersObject.getString(AppConstants.EXPIRES_ON);
                                String termsOfUse = offersObject.getString(AppConstants.TERMS_OF_USE);
                                String logoUrl = offersObject.getString(AppConstants.LOGO_URL);
                                String offerPhotoUrl = offersObject.getString(AppConstants.OFFER_PHOTO_URL);
                                boolean nationalOffer = offersObject.getBoolean(AppConstants.NATIONAL_OFFER);
                                String usesPerPeriod = offersObject.getString(AppConstants.USES_PER_PERIOD);
                                String offerSet = offersObject.getString(AppConstants.OFFER_SET);
                                String savingAmount = offersObject.getString(AppConstants.SAVING_AMOUNT);
                                String minimumPurchase = offersObject.getString(AppConstants.MINIMUM_PURCHASE);
                                String maximumAward = offersObject.getString(AppConstants.MAXIMUM_AWARD);
                                String offerValue = offersObject.getString(AppConstants.OFFER_VALUE);
                                String discountType = offersObject.getString(AppConstants.DISCOUNT_TYPE);
                                String discountValue = offersObject.getString(AppConstants.DISCOUNT_VALUE);
                                boolean promotionCode = offersObject.getBoolean(AppConstants.PROMOTION_CODE);
                                JSONArray redemptionMethods = offersObject.getJSONArray(AppConstants.REDEMPTION_METHODS);// pending
                                JSONArray categoriesJSONArray = offersObject.getJSONArray(AppConstants.CATEGORIES);

                                JSONObject offerStoreJSON = offersObject.getJSONObject(AppConstants.OFFER_STORE);
                                String name = offerStoreJSON.getString(AppConstants.NAME);
                                String description = offerStoreJSON.getString(AppConstants.DESCRIPTION);
                                String storeKey = offerStoreJSON.getString(AppConstants.STORE_KEY);

                                JSONObject physicalLocation = offerStoreJSON.getJSONObject(AppConstants.PHYSICAL_LOCATION);
                                String locationName = physicalLocation.getString(AppConstants.LOCATION_NAME);
                                String webAddress = physicalLocation.getString(AppConstants.WEB_ADDRESS);
                                String locDescription = physicalLocation.getString(AppConstants.DESCRIPTION);
                                boolean onlineExclusive = physicalLocation.getBoolean(AppConstants.ONLINE_EXCLUSIVE);
                                String locationKey = physicalLocation.getString(AppConstants.LOCATION_KEY);
                                String postalCode = physicalLocation.getString(AppConstants.POSTAL_CODE);
                                String country = physicalLocation.getString(AppConstants.COUNTRY);
                                String streetAddress = physicalLocation.getString(AppConstants.STREET_ADDRESS);
                                String extendedStreetAddress = physicalLocation.getString(AppConstants.EXTENDED_STREET_ADDRESS);
                                String cityLocality = physicalLocation.getString(AppConstants.CITY_LOCALITY);
                                String stateRegion = physicalLocation.getString(AppConstants.STATE_REGION);

                                JSONObject geoLocation = physicalLocation.getJSONObject(AppConstants.GEOLOCATION);
                                String lat = geoLocation.getString(AppConstants.LAT);
                                String lon = geoLocation.getString(AppConstants.LON);

                                InstantGeoLocation instantGeoLocation = new InstantGeoLocation(lat, lon);
                                String phoneNumber = physicalLocation.getString(AppConstants.PHONE_NUMBER);

                                InstantPhysicalLocation instantPhysicalLocation = new InstantPhysicalLocation(locationName, webAddress, locDescription,
                                        onlineExclusive, locationKey, postalCode, country, streetAddress, extendedStreetAddress, cityLocality, stateRegion, instantGeoLocation, phoneNumber);

                                InstantOfferStore instantOfferStore = new InstantOfferStore(name,description,storeKey,instantPhysicalLocation);

//                                JSONArray facets = offerStoreJSON.getJSONArray(AppConstants.FACETS);///pending
//                                JSONArray locationKeywords = offerStoreJSON.getJSONArray(AppConstants.LOCATION_KEYWORDS);///pending

                                JSONObject links = offersObject.getJSONObject(AppConstants.LINKS);
                                String showOffer = links.getString(AppConstants.SHOW_OFFER);
                                String showStore = links.getString(AppConstants.SHOW_STORE);
                                String showLocation = links.getString(AppConstants.SHOW_LOCATION);
                                String findOffersAtThisStore = links.getString(AppConstants.FIND_LOCATIONS_FOR_THIS_STORE);
                                String findOffersAtThisLocation = links.getString(AppConstants.FIND_OFFERS_AT_THIS_LOCATION);
                                String findOffersAtMoreLocation = links.getString(AppConstants.FIND_OFFERS_AT_MORE_LOCATIONS);
//                                String findThisOfferAtMoreLocations = links.getString(AppConstants.FIND_OFFERS_AT_MORE_LOCATIONS);
                                String findOffersWithThisOffer = links.getString(AppConstants.FIND_LOCATIONS_WITH_THIS_OFFER);
                                String findOfferForThisStore = links.getString(AppConstants.FIND_OFFERS_AT_THIS_STORE);

                                InstantLinks instantLinks = new InstantLinks(showOffer, showStore, showLocation, findOffersAtThisStore,
                                        findOffersAtThisLocation, findOffersAtMoreLocation, findOffersWithThisOffer, findOfferForThisStore);

                                InstantOffer instantOffer = new InstantOffer(searchDistance, offerKey, offerGroupKey, title, startedOn, expiresOn, termsOfUse, logoUrl, offerPhotoUrl, showOffer, usesPerPeriod, offerSet, savingAmount, minimumPurchase, maximumAward,
                                        offerValue, discountType, discountValue, postalCode, null, null, null, instantLinks,instantOfferStore);

                                ArrayList<InstantCategory> instantCategoryArrayList = new ArrayList<InstantCategory>();
                                for (int j = 0; j < categoriesJSONArray.length(); j++) {
                                    JSONObject catJSON = categoriesJSONArray.getJSONObject(j);
                                    String categoryName = catJSON.getString(AppConstants.CATEGORY_NAME);
                                    String categoryKey = catJSON.getString(AppConstants.CATEGORY_KEY);
                                    String categoryParentKey = catJSON.getString(AppConstants.CATEGORY_PARENT_KEY);
                                    String categoryParentName = catJSON.getString(AppConstants.CATEGORY_PARENT_NAME);
                                    String categoryType = catJSON.getString(AppConstants.CATEGORY_TYPE);


                                    InstantCategory instantCategoryArrayList1 = categoryMap.get(categoryKey);
                                    if (instantCategoryArrayList1 == null) {
                                        ArrayList<InstantOffer> instantOffers = new ArrayList<>();
                                        instantOffers.add(instantOffer);
                                        InstantCategory category = new InstantCategory(categoryName, categoryKey, categoryParentKey, categoryParentName, categoryType, instantOffers);
                                        instantCategoryArrayList1 = category;
                                        categoryMap.put(categoryKey, instantCategoryArrayList1);
                                    } else {
                                        InstantCategory prevCat = categoryMap.get(categoryKey);
                                        ArrayList<InstantOffer> offers = prevCat.getOfferArrayList();
                                        offers.add(instantOffer);
                                        categoryMap.put(categoryKey, prevCat);
                                    }
                                }
                            }
                        }else{
                            GeneralUtil.showAlert(context,jsonObject.getString(AppConstants.MESSAGE));
                        }
                        setupViewPager();

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        GeneralUtil.handleException(context, ex);
                    }

                } else {
                    Log.i(AppConstants.TAG, "error is : " +e.getMessage());
                    GeneralUtil.handleException(context, e);
                }

            }
        });
    }

    @Override
    public void onStop(){
        super.onStop();
        if (mGoogleApiClient!= null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }
    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mGoogleApiClient = null;
    }

}
