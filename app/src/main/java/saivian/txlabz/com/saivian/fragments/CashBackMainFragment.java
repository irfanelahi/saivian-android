package saivian.txlabz.com.saivian.fragments;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import saivian.txlabz.com.saivian.models.CashBackCategory;
import saivian.txlabz.com.saivian.models.CashBackStore;
import saivian.txlabz.com.saivian.models.Location;
import saivian.txlabz.com.saivian.models.OpeningHour;
import saivian.txlabz.com.saivian.utils.AppConstants;
import saivian.txlabz.com.saivian.R;
import saivian.txlabz.com.saivian.utils.GeneralUtil;
import saivian.txlabz.com.saivian.utils.StorageUtility;

/**
 * Created by Fatima Siddique on 5/4/2016.
 */
public class CashBackMainFragment extends Fragment implements View.OnClickListener {
    View cashBackLocalContainer, instantSavingContainer;
    Context context;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cashback_saving, container, false);
        init(view);

        return view;
    }

    private void init(View view) {
        cashBackLocalContainer = view.findViewById(R.id.cashbacklocal_container);
        instantSavingContainer = view.findViewById(R.id.instantsaving_container);
        cashBackLocalContainer.setOnClickListener(this);
        instantSavingContainer.setOnClickListener(this);

        Bundle bundle = getArguments();
        if(bundle != null){
            int index = bundle.getInt(AppConstants.INDEX);
            if(index == 1){
                initInstantSavingFragment();
            }else{
                initCashBackFragment();
            }
        }else{
            initCashBackFragment();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cashbacklocal_container:
                initCashBackFragment();
                break;

            case R.id.instantsaving_container:
                initInstantSavingFragment();
                break;
        }
    }

    private void initCashBackFragment() {
        cashBackLocalContainer.setSelected(true);
        instantSavingContainer.setSelected(false);
        CashBackLocalFragment myf = new CashBackLocalFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.tabs_container, myf);
        transaction.commit();
    }
    private void initInstantSavingFragment() {
        cashBackLocalContainer.setSelected(false);
        instantSavingContainer.setSelected(true);
        InstantSavingFragment myf = new InstantSavingFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.tabs_container, myf);
        transaction.commit();
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        context = null;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case AppConstants.REQUEST_CODE:
                List<Fragment> listOfFragments = getChildFragmentManager().getFragments();

                if (listOfFragments.size() >= 1) {
                    for (Fragment fragment : listOfFragments) {
                        if (fragment != null)
                            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    }
                }
                break;
        }
    }
}
