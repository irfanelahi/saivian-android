package saivian.txlabz.com.saivian.models;

/**
 * Created by Fatima Siddique on 5/6/2016.
 */
public class Receipt {
    String id;
    String receiptType;
    String storeName;
    String purchasedDate;
    String subTotalAmount;
    String receiptImage;
    String uploadedDate;
    String month;

    public Receipt(String id, String receiptType, String storeName, String purchasedDate, String subTotalAmount, String receiptImage, String uploadedDate, String month) {
        this.id = id;
        this.receiptType = receiptType;
        this.storeName = storeName;
        this.purchasedDate = purchasedDate;
        this.subTotalAmount = subTotalAmount;
        this.receiptImage = receiptImage;
        this.uploadedDate = uploadedDate;
        this.month = month;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public String getSubTotalAmount() {
        return subTotalAmount;
    }

    public void setSubTotalAmount(String subTotalAmount) {
        this.subTotalAmount = subTotalAmount;
    }

    public String getReceiptImage() {
        return receiptImage;
    }

    public void setReceiptImage(String receiptImage) {
        this.receiptImage = receiptImage;
    }

    public String getUploadedDate() {
        return uploadedDate;
    }

    public void setUploadedDate(String uploadedDate) {
        this.uploadedDate = uploadedDate;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

}
